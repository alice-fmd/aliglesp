/* 
 * Spherical Harmonics analysis of `maps'
 * Copyright (C) 2012 Christian Holm Christensen
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Concept derived from Gaus-LEgendre Sky Pixelization (GLESP). 
 * Home-page: http://www.glesp.nbi.dk/
 * Reference: Int.Jour.M.Phys.D14, 275 (astro-ph/0305537) 
 *    http://www.worldscinet.com/ijmpd/14/1402/S0218271805006183.html
 *
 */
/**
 * @file   EquidistantAxis.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu May  3 12:50:26 2012
 * 
 * @brief  Declaration of equi-distant axis base class 
 */
#ifndef EQUIDISTANTAXIS_H_
#define EQUIDISTANTAXIS_H_
#include <Axis.h>
#include <TAxis.h>

/** 
 * Base class for equidistant axis
 */
class EquidistantAxis : public Axis
{
public:
  /** 
   * Constructor (for I/O)
   */
  EquidistantAxis() : fAxis() {}
  /** 
   * Constructor 
   * 
   * @param title  Title of axis  
   * @param nx     Number of bins
   * @param x1     Least value 
   * @param x2     Largest value
   */
  EquidistantAxis(const char* title, UShort_t nx, Double_t x1, Double_t x2) 
    : fAxis(nx,x1,x2) 
  {
    fAxis.SetTitle(title);
  }
  /** 
   * Destructor
   */
  virtual ~EquidistantAxis() {}
    

  /**
   * Get the number of bins
   *
   * @return Number of bins
   */
  virtual UShort_t GetNBins(UShort_t=0) const { return fAxis.GetNbins(); }
  /**
   * Get the 'centre' value @f$ \theta@f$ corresponding to
   * this bin
   *
   * @param bin Bin number
   *
   * @return 'centre' value @f$ \theta@f$
   */
  virtual Double_t GetBinX(UShort_t bin,UShort_t=0) const 
  { 
    return fAxis.GetBinCenter(bin); 
  }
  /**
   * Get the weight of this bin.  Note, one should also scale by the
   * phi bin size.
   *
   * @param bin  Bin number
   *
   * @return Weight
   */
  virtual Double_t GetBinWeight(UShort_t bin,UShort_t=0) const 
  { 
    return (fAxis.GetXmax()-fAxis.GetXmin())/GetNBins(); 
  }
  /**
   * Find bin corresponding to @f$ x=\theta@f$
   *
   * @param x @f$ \theta@f$
   *
   * @return Bin number
   */
  virtual UShort_t FindBin(Double_t x,UShort_t=0) const 
  { 
    return const_cast<TAxis&>(fAxis).FindBin(x); 
  }
  /**
     * Get the ROOT axis corresponding to this axis
     *
     * @return Pointer to axis object
     */
  virtual TAxis* GetAxis() const { return const_cast<TAxis*>(&fAxis); }

  /** 
   * @{
   * @name ROOT utilities 
   */
  /** 
   * Whether this should be considered a folder
   * 
   * @return Always true 
   */
  Bool_t IsFolder() const { return true; }
  /* @} */

protected:
  TAxis fAxis;

  ClassDef(EquidistantAxis,1); // 
};

#endif /* EQUIDISTANTAXIS_H_ */
/*
 * Local Variables: 
 *  mode: C++
 * End:
 */
