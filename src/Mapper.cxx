/* 
 * Spherical Harmonics analysis of `maps'
 * Copyright (C) 2012 Christian Holm Christensen
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Concept derived from Gaus-LEgendre Sky Pixelization (GLESP). 
 * Home-page: http://www.glesp.nbi.dk/
 * Reference: Int.Jour.M.Phys.D14, 275 (astro-ph/0305537) 
 *    http://www.worldscinet.com/ijmpd/14/1402/S0218271805006183.html
 *
 */
/**
 * @file   Mapper.cxx
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu May  3 12:53:40 2012
 * 
 * @brief  Implementation of class to map data
 */
#include <TFile.h>
#include <TTree.h>
#include <TVector2.h>
#include <TClonesArray.h>

#include <fstream>
#include <sstream>
#include <iostream>

#include "Mapper.h"
#include "GausLegendreAxis.h"
#include "PhiAxis.h"
#include "EtaAxis.h"
#include "SpecialAxis.h"
#include "HistogramMap.h"

//____________________________________________________________________
Mapper::Mapper(UShort_t nx, 
	       UShort_t ny, 
	       UInt_t   xType, 
	       UInt_t   yType, 
	       UInt_t   option)
  : fAll(0), 
    fCurrent(0),
    fXAxis(0),
    fYAxis(0),
    fTree(0),
    fPerEvent(option & kMapPerEvent)
{
  if   (xType & kEta)     fXAxis = new EtaAxis(nx, -6, 6);
  else                    fXAxis = new GausLegendreAxis(nx);
  
  if (yType & kEqualSize) fYAxis = new SpecialAxis(*fXAxis, ny);
  else                    fYAxis = new PhiAxis(ny);
  
  fAll = new HistogramMap("all", fXAxis, fYAxis);
}


//____________________________________________________________________
Map*
Mapper::MakeMap(const char* name) const
{
  if (!fPerEvent) return 0;
  // Info("MakeMap", "Cloning map %s -> %s", fAll->GetName(), name);
  return fAll->CloneThis(name);
}

//____________________________________________________________________
void
Mapper::Fill(Map* map, Double_t eta, Double_t phi)
{
  if (map) map->FillEta(eta, phi);
  fAll->FillEta(eta, phi);
  // map->FillRadians(theta*TMath::DegToRad(), phi*TMath::DegToRad(), 1);
}

//____________________________________________________________________
void
Mapper::FinishMap(Map* map, Int_t nHits, Int_t nEvents, Bool_t writeAxis)
{
  if (!map) return;
  map->Normalize(nHits, nEvents);
  if (writeAxis) {
    // Info("FinishMap", "Writing map %s to disk", map->GetName());
    map->Write();
    delete map;
    return;
  }
  // Info("FinishMap", "Filling into tree %s", map->GetName());
  fTree->Fill();
}
//____________________________________________________________________
Bool_t
Mapper::Read(const char* input, const char* output)
{
  TFile* file = TFile::Open(output, "RECREATE");
  if (!file) { 
    Error("Read", "Failed to open output file %s", output);
    return false;
  }
  fCurrent = MakeMap("dummy");
  fTree = new TTree("T", "T");
  fTree->Branch("map", "HistogramMap", &fCurrent, 32000, 0);
  
  delete fCurrent;

  TString inp(input);
  Bool_t ret = true;
  if (inp.EndsWith(".root")) ret = ReadROOT(inp);
  else                       ret = ReadASCII(inp);

  if (!ret) { 
    Error("Read", "Failed to read input file %s", input);
    return ret;
  }

  fXAxis->Write("xaxis");
  fYAxis->Write("yaxis");
  
  file->Write();
  file->Close();
  
  return ret;
}


//____________________________________________________________________
Bool_t
Mapper::ReadASCII(const TString& input)
{
  std::ifstream file(input.Data());
  if (!file) { 
    Error("ReadASCII", "Failed to open input file %s", input.Data());
    return false;
  }

  bool in_event = false;
  int  n_part   = 0;
  int  n_total  = 0;
  int  ev       = 0;
  while (!file.eof()) { 
    std::string l;
    std::getline(file, l);
    if (l[0] == '#') continue;

    if (l.find("BEGIN") != std::string::npos) { 
      Info("ReadASCII", "New event");
      in_event = true;
      n_part   = 0;
      ev++;
      fCurrent = MakeMap(Form("map%08d", ev));
      continue;
    }
    if (l.find("END") != std::string::npos) {
      FinishMap(fCurrent, n_part, 1, false);
      fCurrent =  0;
      in_event =  false;
      n_total  += n_part;
      continue;
    }
    
    if (!in_event) continue;

    std::stringstream s(l);
	    
    Double_t eta;
    Double_t theta; 
    Double_t phi;
      
    s >> eta >> theta >> phi;
    n_part++;
      
    if ((n_part % 1000) == 0) std::cout << "." << std::flush;

    Fill(fCurrent, eta, phi*TMath::DegToRad());
  }
  std::cout << " " << n_total << " particles" << std::endl; 

  FinishMap(fAll, n_total, ev, true);
  fAll = 0;

  file.close();

  return true;
}

//____________________________________________________________________
Bool_t
Mapper::ReadROOT(const TString& input)
{
  struct Event 
  {
    Int_t    evNo;
    Int_t    nPart;
    Double_t phiR;
    Double_t v[4];
  } event;

  TDirectory* sav  = gDirectory;
  TFile*      file = TFile::Open(input, "READ");
  if (!file) { 
    Error("ReadROOT", "Failed to open input file %s", input.Data());
    return false;
  }

  TTree* tree = static_cast<TTree*>(file->Get("data"));
  if (!tree) { 
    Error("ReadROOT", "Failed to find tree 'data' in %s", input.Data());
    return false;
  }
  sav->cd();

  TClonesArray* hits = new TClonesArray("TVector2");
  tree->SetBranchAddress("event", &event);
  tree->SetBranchAddress("hits", &hits);
  fTree->Branch("event", &event, tree->GetBranch("event")->GetTitle());

  Int_t nEvents = tree->GetEntries();
  Int_t nTotal;
  for (Int_t ev = 0; ev < nEvents; ev++) {
    tree->GetEntry(ev);
    fCurrent = MakeMap(Form("map%08d", ev+1));
    
    Int_t nHits =  hits->GetEntriesFast();
    nTotal      += nHits;
    // Info("ReadROOT", "Got a total of %6d hits", nHits);
    for (Int_t ih = 0; ih < nHits; ih++) { 
      TVector2* v = static_cast<TVector2*>(hits->At(ih));
      Fill(fCurrent, v->X(), v->Y());
    }

    FinishMap(fCurrent, nHits, 1, false);
    fCurrent = 0;
  }
  
  FinishMap(fAll, nTotal, nEvents, true);
  fAll = 0;

  return true;
}


//
// EOF
//

