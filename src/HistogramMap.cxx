/* 
 * Spherical Harmonics analysis of `maps'
 * Copyright (C) 2012 Christian Holm Christensen
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Concept derived from Gaus-LEgendre Sky Pixelization (GLESP). 
 * Home-page: http://www.glesp.nbi.dk/
 * Reference: Int.Jour.M.Phys.D14, 275 (astro-ph/0305537) 
 *    http://www.worldscinet.com/ijmpd/14/1402/S0218271805006183.html
 *
 */
/**
 * @file   HistogramMap.cxx
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu May  3 12:52:45 2012
 * 
 * @brief  Implementation of map backed by a histogram
 */
#include "HistogramMap.h"
#include <TH2D.h>
#include <TBrowser.h>
#include <TMath.h>
#include <TAxis.h>
#include "Axis.h"
#include "EtaAxis.h"
#include <iostream>

//____________________________________________________________________
HistogramMap::HistogramMap()
  : Map(), 
    fMap(0), 
    fWeightedMap(0)
{}

//____________________________________________________________________
HistogramMap::HistogramMap(const char* name, 
			   const Axis* xaxis, 
			   const Axis* yaxis)
  : Map(name, xaxis, yaxis),
    fMap(0),
    fWeightedMap(0)
{
  if (!fXAxis || !fYAxis) { 
    Fatal("HistogramMap", "Either X (%p) or Y (%p) axis or both not set", 
	  fXAxis, fYAxis);
    return;
  }
  TAxis* xa = fXAxis->GetAxis();
  TAxis* ya = fYAxis->GetAxis();
  if (!xa) { 
    Fatal("HistogramMap", "Couldn't get TAxis objects for X-axis");
    return;
  }
  if (!ya) { 
    Fatal("HistogramMap", "Couldn't get TAxis objects for Y-axis");
    return;
  }
  if (fXAxis->IsDependent()) { 
    Fatal("HistogramMap", "This class does not handle a dependent X axis");
    return;
  }

  Int_t          nx = xa->GetNbins();
  Int_t          ny = ya->GetNbins();
  Double_t*      xb = xa->GetXbins()->fArray;
  Double_t*      yb = ya->GetXbins()->fArray;
  Double_t       x1 = xa->GetXmin();
  Double_t       x2 = xa->GetXmax();
  Double_t       y1 = ya->GetXmin();
  Double_t       y2 = ya->GetXmax();

  if (xb) { 
    if (yb) fMap = new TH2D("map", "Map",  nx, xb, ny, yb);
    else    fMap = new TH2D("map", "Map",  nx, xb, ny, y1, y2);
  } 
  else { 
    if (yb) fMap = new TH2D("map", "Map", nx, x1, x2, ny, yb);
    else    fMap = new TH2D("map", "Map", nx, x1, x2, ny, y1, y2);
  }    
  fMap->SetXTitle(xa->GetTitle());
  fMap->SetYTitle(ya->GetTitle());
  fMap->SetZTitle("'Temperature'");
  // fMap->SetStats(0);
  fMap->SetDirectory(0);

  fWeightedMap = static_cast<TH2D*>(fMap->Clone("weightedMap"));
  fWeightedMap->SetTitle("Weighted map");
  // fWeightedMap->SetStats(0);
  fWeightedMap->SetDirectory(0);
}

//____________________________________________________________________
HistogramMap::HistogramMap(const HistogramMap& o)
  : Map(o), 
    fMap(0), 
    fWeightedMap(0)
{
  // Info("Copy-ctor", "Copying from %s", o.GetName());
  if (o.fMap)         
    fMap = static_cast<TH2D*>(o.fMap->Clone());
  if (o.fWeightedMap) 
    fWeightedMap = static_cast<TH2D*>(o.fWeightedMap->Clone());
  if (fMap)         fMap->Reset();
  if (fWeightedMap) fWeightedMap->Reset();
}

    
//____________________________________________________________________
HistogramMap::~HistogramMap()
{
  if (fMap)         delete fMap;
  if (fWeightedMap) delete fWeightedMap;
}

//____________________________________________________________________
Map*
HistogramMap::CloneThis(const char* name) const
{
  HistogramMap* ret = new HistogramMap(*this);
  ret->SetName(name);
  return ret;
}

//____________________________________________________________________
UShort_t
HistogramMap::FindThetaBin(Double_t theta)  const
{
  UShort_t bin = fXAxis->FindBin(theta);
  return bin;
}
//____________________________________________________________________
UShort_t
HistogramMap::FindEtaBin(Double_t eta) const
{
  return FindThetaBin(EtaAxis::Eta2Theta(eta));
}
//____________________________________________________________________
UShort_t
HistogramMap::FindPhiBin(Double_t phi, UShort_t thetaBin) const
{
  return fYAxis->FindBin(phi, thetaBin);
}   
//____________________________________________________________________
void
HistogramMap::SetBin(Double_t theta, Double_t phi, Double_t c)
{
  Int_t xbin = FindThetaBin(theta);
  Int_t ybin = FindPhiBin(phi);
  fMap->SetBinContent(xbin, ybin, c);
}
//____________________________________________________________________
void
HistogramMap::SetBinEta(Double_t eta, Double_t phi, Double_t c) 
{
  Double_t theta = EtaAxis::Eta2Theta(eta);
  SetBin(theta, phi, c);
}
//____________________________________________________________________
void
HistogramMap::Fill(Double_t theta, Double_t phi, Double_t w)
{
  Int_t thetaBin = FindThetaBin(theta);
  Int_t phiBin   = FindPhiBin(phi, thetaBin);
  Int_t bin      = fMap->GetBin(thetaBin, phiBin); 
  // Info("Fill", "Filling at theta=%10f, phi=%10f w=%10f", theta, phi, w);
  fMap->AddBinContent(bin, w);
  fMap->SetEntries(fMap->GetEntries()+w);
}
//____________________________________________________________________
void
HistogramMap::FillEta(Double_t eta, Double_t phi, Double_t w)
{
  Fill(EtaAxis::Eta2Theta(eta), phi, w);
}
//____________________________________________________________________
UShort_t 
HistogramMap::GetLmax() const 
{
  UShort_t lxmax = (fXAxis->GetMaxNBins() - 1) / 2;
  UShort_t lymax = (fYAxis->GetMaxNBins() - 1) / 4;
  return TMath::Min(lxmax, lymax);
}
//____________________________________________________________________
UShort_t
HistogramMap::GetNX() const
{
  if (!fMap) { 
    Warning("GetNX", "No map defined yet");
    return 0;
  }
  return fMap->GetNbinsX();
}

//____________________________________________________________________
UShort_t
HistogramMap::GetNY(UShort_t bin) const
{
  if (!fYAxis) { 
    Warning("GetNY", "No Y axis defined yet");
    return 0;
  }
  return fYAxis->GetNBins(bin);
}
//____________________________________________________________________
UShort_t
HistogramMap::GetMaxNY() const
{
  if (fYAxis) 
    return fYAxis->GetMaxNBins();
  else if (!fMap) {
    Warning("GetMaxNY", "No Y axis defined yet");
    return 0;
  }
  return fMap->GetNbinsY();  
}

//____________________________________________________________________
Double_t
HistogramMap::GetValue(UShort_t xbin, UShort_t ybin, bool weighted) const
{
  TH2D* m = (weighted ? GetWeightedMap() : GetMap());
  return m->GetBinContent(xbin, ybin);
}

//____________________________________________________________________
Double_t
HistogramMap::Normalize(ULong_t nHit, ULong_t nEvent)
{
  fWeightedMap->Reset();
  Double_t monopole = 0;

  for (Int_t i = 1; i <= fXAxis->GetNBins(); i++) {
    Double_t wx = fXAxis->GetBinWeight(i);
    for (Int_t j = 1; j <= fYAxis->GetNBins(i); j++) {
      Double_t wy = fYAxis->GetBinWeight(j, i);
      Double_t c  = fMap->GetBinContent(i, j);
      Double_t v  = wx * wy * c;
      fWeightedMap->SetBinContent(i, j, v);
      monopole += v;
    }
  }
  return monopole / TMath::Sqrt(4 * TMath::Pi());
}

//____________________________________________________________________
Double_t
HistogramMap::GetBinPhi(UShort_t /*xbin*/, UShort_t ybin) const
{
  return fYAxis->GetBinX(ybin);
}

//____________________________________________________________________
Double_t
HistogramMap::GetBinTheta(UShort_t xbin, UShort_t /*ybin*/) const
{
  return fXAxis->GetBinX(xbin);
}
//____________________________________________________________________
void
HistogramMap::Browse(TBrowser* b) 
{
  if (fMap)         b->Add(fMap);
  if (fWeightedMap) b->Add(fWeightedMap);
  if (fXAxis)       b->Add(const_cast<Axis*>(fXAxis));
  if (fYAxis)       b->Add(const_cast<Axis*>(fYAxis));
}
//____________________________________________________________________
void
HistogramMap::Draw(Option_t* option)
{
  TString opt(option);
  TH1*  h = fMap;
  if (opt.Contains("raw", TString::kIgnoreCase)) { 
    opt.ReplaceAll("raw", "");
    h = fWeightedMap;
  }
  if (!h) { 
    Warning("Draw", "Nothing to draw: %p", h);
    return;
  }
  h->Draw(opt.Data());
}

//____________________________________________________________________
void
HistogramMap::Print(Option_t* option) const
{
  std::cout << "HistogramMap: " << GetName() << "\n"
	    << "\tMap:          " << fMap << "\n"
	    << "\tWeighted map: " << fWeightedMap << "\n"
	    << "\tNX:           " << GetNX() << "\n"
	    << "\tmax(NY):      " << GetMaxNY()  << std::endl;
}

//
// EOF
//
