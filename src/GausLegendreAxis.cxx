/* 
 * Spherical Harmonics analysis of `maps'
 * Copyright (C) 2012 Christian Holm Christensen
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Concept derived from Gaus-LEgendre Sky Pixelization (GLESP). 
 * Home-page: http://www.glesp.nbi.dk/
 * Reference: Int.Jour.M.Phys.D14, 275 (astro-ph/0305537) 
 *    http://www.worldscinet.com/ijmpd/14/1402/S0218271805006183.html
 *
 */
/**
 * @file   GausLegendreAxis.cxx
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu May  3 12:51:57 2012
 * 
 * @brief  Implementation of Gaus-Legendre Axis 
 */
#include "GausLegendreAxis.h"
#include <TAxis.h>
#include <TMap.h>
#include <TMath.h>
#include <iostream>
#include <iomanip>

//____________________________________________________________________
GausLegendreAxis::GausLegendreAxis()
  : Axis(), 
    fWeights(), 
    fAbscissa()
{
}

//____________________________________________________________________
GausLegendreAxis::GausLegendreAxis(UShort_t nx, Double_t xmin, Double_t xmax)
  : Axis(), 
    fWeights("weights", "Gauss-Legendre curvature weight", 1, 0, 1), 
    fAbscissa()
{
  TArrayD weights;
  GaussLegendreIntegral(fAbscissa, weights, nx, xmin, xmax);

  TArrayD bins(nx+1);
  bins.SetAt(xmin, 0);
  bins.SetAt(xmax, nx);

  UShort_t m  = fAbscissa.GetSize();
  for (UShort_t i = 1; i < m; i++) {
    Double_t x = fAbscissa.At(i-1) + (fAbscissa.At(i) - fAbscissa.At(i-1))/2;
    bins.SetAt(+x, i);
    bins.SetAt(-x, nx-i);
  }

  fWeights.SetBins(nx, bins.fArray);
  fWeights.SetXTitle("cos(#theta)");
  fWeights.SetYTitle("Weight");
  fWeights.SetDirectory(0);
  fWeights.SetStats(0);

  for (UShort_t i = 0; i < m; i++) {
    fWeights.SetBinContent(i+1,  weights[i]);
    fWeights.SetBinContent(nx-i, weights[i]);
  }
}

//____________________________________________________________________
GausLegendreAxis::GausLegendreAxis(const GausLegendreAxis& o)
  : Axis(o), 
    fWeights(o.fWeights), 
    fAbscissa(o.fAbscissa)
{
}

//____________________________________________________________________
GausLegendreAxis&
GausLegendreAxis::operator=(const GausLegendreAxis& o)
{
  Axis::operator=(o);
  fWeights  = o.fWeights;
  fAbscissa = o.fAbscissa;
  return *this;
}
//____________________________________________________________________
Double_t
GausLegendreAxis::GetBinX(UShort_t bin,UShort_t) const
{
  Int_t i = bin - 1;
  Int_t j = GetNBins() - bin;
  return TMath::ACos((i >= j ? -1 : 1) * fAbscissa.At(i >= j ? j : i));
}
//____________________________________________________________________
UShort_t
GausLegendreAxis::FindBin(Double_t x,UShort_t) const
{
  Int_t ret = GetAxis()->FindBin(TMath::Cos(x));
  if (ret > GetNBins()) ret = GetNBins();
  return ret;
}

//____________________________________________________________________
Double_t
GausLegendreAxis::GetBinWeight(UShort_t bin,UShort_t) const
{
  return fWeights.GetBinContent(bin);
}
//____________________________________________________________________
void
GausLegendreAxis::Print(Option_t* option) const
{
  const TAxis* ax = fWeights.GetXaxis();
  std::cout << "Axis " << ax->GetTitle() << ":" << std::endl;
  Int_t n = ax->GetNbins();
  Int_t m = 5;
  for (Int_t i = 0; i < n; i++) { 
    if ((i % 5) == 0) {
      if (i != 0) std::cout << '\n';
      std::cout << std::setw(3) << i+1 << "-" 
		<< std::setw(3) << i+m << ": ";
    }
    std::cout << std::setw(12) << ax->GetBinCenter(i+1);
  }
  std::cout << std::endl;
}

//____________________________________________________________________
void
GausLegendreAxis::GaussLegendreIntegral(TArrayD& abs, 
					TArrayD& weights,
					UShort_t n, 
					Double_t x1, 
					Double_t x2, 
					Double_t eps) 
{
  UShort_t m  = (n+1) / 2;
  Double_t xm = .5 * (x2 + x1);
  Double_t xl = .5 * (x2 - x1);

  abs.Set(m);
  weights.Set(m);

  for (UShort_t i = 0; i < m; i++) { 
    Double_t z = TMath::Cos(TMath::Pi() * ((i + 1) - .25) / (n + .5));
    Double_t w = GaussLegendreKernel(z, n, eps);
      
    abs.SetAt(xm - xl * z, i);
    weights.SetAt(2 * xl / ((1 - z * z) * w * w), i);
  }
}
//____________________________________________________________________
Double_t
GausLegendreAxis::GaussLegendreKernel(Double_t& z, UShort_t order, 
				      Double_t eps)
{
  // This searches for the ith root of the Legendre polynomial
  Double_t z1 = 0; 
  Double_t pp = 0;
  do {
    Double_t p1 = 1.0;
    Double_t p2 = 0.0;
    Double_t p3 = 0;				
    for(UShort_t i = 0; i < order; i++) {
      p3 = p2;
      p2 = p1;
      p1 = ((2 * i + 1) * z * p2 - i * p3) / (i + 1);
    }
    pp = order * (z * p1 - p2)/(z * z - 1.0);
    z1 = z;
    z  = z1 - p1 / pp;
  } while (TMath::Abs(z-z1) > eps);
  return pp;
}
//____________________________________________________________________
//
// EOF
//
