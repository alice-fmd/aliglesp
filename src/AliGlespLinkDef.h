/* 
 * Spherical Harmonics analysis of `maps'
 * Copyright (C) 2012 Christian Holm Christensen
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Concept derived from Gaus-LEgendre Sky Pixelization (GLESP). 
 * Home-page: http://www.glesp.nbi.dk/
 * Reference: Int.Jour.M.Phys.D14, 275 (astro-ph/0305537) 
 *    http://www.worldscinet.com/ijmpd/14/1402/S0218271805006183.html
 *
 */
/**
 * @file   AliGlespLinkDef.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu May  3 12:48:19 2012
 * 
 * @brief  ROOT dictionary linkage specs.
 */

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class Axis+;
#pragma link C++ class GausLegendreAxis+;
#pragma link C++ class EquidistantAxis+;
#pragma link C++ class EtaAxis+;
#pragma link C++ class PhiAxis+;
#pragma link C++ class SpecialAxis+;
#pragma link C++ class Map+;
#pragma link C++ class HistogramMap+;
#pragma link C++ class SphericalHarmonics+;
#pragma link C++ class Mapper+;
#pragma link C++ class Desolver+;
#pragma link C++ class Flower+;

#endif
/*
 * Local Variables: 
 *  mode: C++ 
 * End:
 */

