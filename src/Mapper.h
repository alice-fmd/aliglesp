/* 
 * Spherical Harmonics analysis of `maps'
 * Copyright (C) 2012 Christian Holm Christensen
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Concept derived from Gaus-LEgendre Sky Pixelization (GLESP). 
 * Home-page: http://www.glesp.nbi.dk/
 * Reference: Int.Jour.M.Phys.D14, 275 (astro-ph/0305537) 
 *    http://www.worldscinet.com/ijmpd/14/1402/S0218271805006183.html
 *
 */
/**
 * @file   Mapper.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu May  3 12:54:01 2012
 * 
 * @brief  Declaration of class to map data
 */
#ifndef ALIGLESP_MAPPER
#define ALIGLESP_MAPPER
#include <TObject.h>
#include <Map.h>
class TTree;
/**
 * @file   Mapper.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu May  3 12:47:45 2012
 * 
 * @brief  Mapper class 
 */

/**
 * @class Mapper
 * @brief Class to map data into maps 
 *
 * @example MapIt.C 
 * 
 */
class Mapper : public TObject
{
public:
  /** 
   * Axis types 
   */
  enum { 
    /** A GausLegendreAxis */
    kGausLegendre = 0x1, 
    /** A EtaAxis */
    kEta          = 0x2,
    /** A SpecialAxis */
    kEqualSize    = 0x4,
    /** A PhiAxis */
    kPhi          = 0x8
  };
  /** 
   * Other options 
   */
  enum { 
    kMapPerEvent = 0x4 
  };
  /** 
   * Constructor 
   * 
   * @param nx       Number of X bins to use 
   * @param ny       Number of Y bins to use
   * @param xType    Type of X axis
   * @param yType    Type of Y axis 
   * @param options  Other options
   */
  Mapper(UShort_t nx, UShort_t ny, 
	 UInt_t xType=kGausLegendre,
	 UInt_t yType=kEqualSize, 
	 UInt_t options=kMapPerEvent);
  /** 
   * Read the input and store maps on the output 
   * 
   * @param input   Input file name 
   * @param output  Output file name 
   * 
   * @return true on success
   */  
  Bool_t Read(const char* input, const char* output);
private:
  Mapper(const Mapper&) 
  : fAll(0), 
    fCurrent(0), 
    fXAxis(0), 
    fYAxis(0), 
    fTree(0), 
    fPerEvent(true) 
  {}
  Mapper& operator=(const Mapper&) { return *this; }

  /** 
   * Read ASCII data.  The data is structured like 
   *
   * @verbatim 
   * BEGIN EVENT n 
   *   eta phi theta flag dEta dTheta 
   *   ...
   * END EVENT n 
   * BEGIN EVENT n 
   *   ...
   * @endverbatim
   *
   * where eta, phi, and theta are the @f$\eta@f$, @f$\phi@f$, and
   * @f$\theta@f$ of the particles, flag is 1 for primaries and 0 for
   * secondaries, dEta and dPhi are the resolutions in @f$\eta@f$ and
   * @f$\phi@f$ respectively.  All angles are given in degrees.
   * 
   * @param input  Input file
   * 
   * @return true on success 
   */
  Bool_t ReadASCII(const TString& input);
  /** 
   * Read data from a ROOT tree
   * 
   * The hits are assumed to be stored on a branch named "hits" which
   * contains a TClonesArray of TVector2.  The first coordinate of the
   * vector is @f$\eta@f$ and the second is @f$\phi@f$ (in radians).
   * 
   * @param input  Input file. 
   * 
   * @return True on success 
   */
  Bool_t ReadROOT(const TString& input);
  /** 
   * Make a map object from our template 
   * 
   * @param name Name of map. 
   * 
   * @return Newly allocated map 
   */
  Map*   MakeMap(const char* name) const; 
  /** 
   * Fill a hit into the map 
   * 
   * @param map  Map to fill into 
   * @param eta  The pseudo-rapidity 
   * @param phi  Azimuthal angle 
   */
  void   Fill(Map* map, Double_t eta, Double_t phi);
  /** 
   * Finish off the map.  
   * 
   * @param map       Map to finish. 
   * @param nHits     Number of hits in this map. 
   * @param nEvents   Number of events in this map. 
   * @param writeAxis Whether to write the axis objects too. 
   */
  void   FinishMap(Map* map, Int_t nHits, Int_t nEvents,
		   Bool_t writeAxis=false);

  Map*   fAll;
  Map*   fCurrent;
  Axis*  fXAxis;
  Axis*  fYAxis;
  TTree* fTree;
  Bool_t fPerEvent;


  ClassDef(Mapper,1);
};


#endif
/*
 * Local Variables: 
 *  mode: C++ 
 * End: 
 */
