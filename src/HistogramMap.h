/* 
 * Spherical Harmonics analysis of `maps'
 * Copyright (C) 2012 Christian Holm Christensen
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Concept derived from Gaus-LEgendre Sky Pixelization (GLESP). 
 * Home-page: http://www.glesp.nbi.dk/
 * Reference: Int.Jour.M.Phys.D14, 275 (astro-ph/0305537) 
 *    http://www.worldscinet.com/ijmpd/14/1402/S0218271805006183.html
 *
 */
/**
 * @file   HistogramMap.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu May  3 12:52:45 2012
 * 
 * @brief  Declaration of map backed by a histogram
 */
#ifndef ALIGLESP_HISTOGRAMMAP
#define ALIGLESP_HISTOGRAMMAP 
#include <Map.h>
class TH2D;
class TBrowser;


/**
 * A map backed by a histogram 
 */
class HistogramMap : public Map
{
public:
  /** 
   * Default constructor 
   */
  HistogramMap(); 
  /** 
   * Named user constructor 
   * 
   * @param name  Name of object 
   * @param xaxis X axis to use
   * @param yaxis Y axis to use 
   */
  HistogramMap(const char* name, const Axis* xaxis, const Axis* yaxis);
  /**
   * Destructor 
   */
  virtual ~HistogramMap();

  /** 
   * Clone this map
   * 
   * @param name New name 
   * 
   * @return Newly allocated empty map 
   */
  virtual Map* CloneThis(const char* name) const;
  /** 
   * @{
   * @name Methods for filling map 
   */
  /** 
   * Fill into map at @f$ (\theta,\phi)@f$ with weight @f$ w@f$. 
   * 
   * @param theta Polar angle in radians 
   * @param phi   Azimuthal angle in radians 
   * @param w     Weight 
   */
  virtual void Fill(Double_t theta, Double_t phi, Double_t w=1);
  /** 
   * Fill into map at @f$ (\theta,\phi)@f$ with weight @f$ w@f$, where 
   * @f[ 
   *  \theta = 2 \tan^{-1}\left(e^{-\eta}\right)
   * @f]
   * 
   * @param eta   Pseudo-rapidity
   * @param phi   Azimuthal angle in radians
   * @param w     Weight 
   */
  virtual void FillEta(Double_t eta, Double_t phi, Double_t w=1);
  /* @} */

  /** 
   * @{ 
   * @name Calculations 
   */
  /** 
   * Get the maximum number of moments 
   * 
   * @return Maximum possible number of components 
   */  
  UShort_t GetLmax() const;
  /** 
   * Normalize (or weight) the map 
   * 
   * @return The monopole value 
   */
  virtual Double_t Normalize(ULong_t nHits, ULong_t nEvents);
  /* @} */

  /** 
   * @{ 
   * @name Getters and setters 
   */
  /** 
   * Get the number of bins along X 
   * 
   * @return Number of bins along X 
   */
  UShort_t GetNX() const;
  /** 
   * Get the number of bins along Y 
   * 
   * @return Number of bins along Y 
   */
  UShort_t GetNY(UShort_t xbin) const;
  /** 
   * Get the maximum number of bins along Y
   * 
   * @return Maximum number of bins along Y
   */
  virtual UShort_t GetMaxNY() const ;
  /** 
   * Get the value of the map a given point 
   * 
   * @param xbin       X bin
   * @param ybin       Y bin 
   * @param weighted   If true, the normalized value 
   * 
   * @return Value 
   */
  Double_t GetValue(UShort_t xbin, UShort_t ybin, bool weighted=true) const;
  /** 
   * Get the @f$\theta@f$ value associated with a bin 
   * 
   * @param xbin Bin number (1 is the first bin) - if Y dependent on X 
   * @param ybin Bin number (1 is the first bin)
   * 
   * @return @f$\theta@f$ value
   */
  virtual Double_t GetBinTheta(UShort_t xbin, UShort_t ybin) const;
  /** 
   * Get the @f$\phi@f$ value associated with a bin 
   * 
   * @param xbin Bin number (1 is the first bin) - if Y dependent on X 
   * @param ybin Bin number (1 is the first bin)
   * 
   * @return @f$\phi@f$ value
   */
  virtual Double_t GetBinPhi(UShort_t xbin, UShort_t ybin) const;
  /* @} */

  /** 
   * @{ 
   * @name Protected getters 
   */
  /** 
   * Get the map 
   * 
   * @return The map 
   */
  TH2D* GetMap() const { return fMap; }
  /** 
   * Get the weighted (or normalized) map
   * 
   * @return Weighted map
   */
  virtual TH2D* GetWeightedMap() const { return fWeightedMap; }
  /* @} */

  /** 
   * @{
   * @name ROOT utilities 
   */
  /** 
   * Whether this should be considered a folder
   * 
   * @return Always true 
   */
  Bool_t IsFolder() const { return true; }
  /** 
   * Browse this object 
   * 
   * @param b Browser to use 
   */
  virtual void Browse(TBrowser* b);
  /** 
   * Draw this map 
   * 
   * @param option Drawing options. 
   */
  void Draw(Option_t* option); //*MENU*
  void Print(Option_t* option) const; //*MENU*
  /* @} */

protected:
  /** 
   * @{ 
   * @name Bin indexing 
   */
  /** 
   * Find the bin corresponding to @f$\theta@f$ 
   * 
   * @param theta Polar angle @f$\theta@f$ in radians 
   * 
   * @return Bin number 
   */
  virtual UShort_t FindThetaBin(Double_t theta) const;
  /** 
   * Find the bin corresponding to @f$\theta=2\tan^{-1}e^{-\eta}@f$ 
   * 
   * @param eta Pseudo-rapidity
   * 
   * @return Bin number 
   */
  virtual UShort_t FindEtaBin(Double_t eta) const;
  /** 
   * Find the bin corresponding to @f$\phi@f$ 
   * 
   * @param phi      Azimuthal angle @f$\phi@f$ in radians 
   * @param thetaBin @f$\theta@f$ bin in case the Y axis depends on the X axis 
   * 
   * @return Bin number 
   */
  virtual UShort_t FindPhiBin(Double_t phi, UShort_t thetaBin=0) const;
  /* @} */
  
  /** 
   * @{ 
   * @name Set bins 
   */
  /** 
   * Set bin content corresponding to @f$(\theta,\phi)@f$ to @a c
   * 
   * @param theta Polar angle @f$\theta@f$ in radians
   * @param phi   Azimuthal angle @f$\phi@f$ in radians 
   * @param c     Content of bin
   */
  virtual void SetBin(Double_t theta, Double_t phi, Double_t c);
  /** 
   * Set bin content corresponding to @f$(\eta,\phi)@f$ to @a c
   * 
   * @param eta   Pseudo-rapidity @f$\eta@f$
   * @param phi   Azimuthal angle @f$\phi@f$ in radians 
   * @param c     Content of bin
   */
  virtual void SetBinEta(Double_t eta, Double_t phi, Double_t c);
  /* @} */


protected:

private:
  /** 
   * Copy constructor - dummy
   * 
   * @param o Object to copy from
   */
  HistogramMap(const HistogramMap& o);
  /** 
   * Assignment operator - dummy
   * 
   * @return Reference to this object
   */
  HistogramMap& operator=(const HistogramMap&) { return *this; }
  
  /** Our map */
  TH2D* fMap;
  /** Our normalized map */
  TH2D* fWeightedMap;

  ClassDef(HistogramMap,1);
};


#endif
/*
 * Local Variables: 
 *  mode:  C++ 
 * End:
 */
