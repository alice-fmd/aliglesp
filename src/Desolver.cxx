/* 
 * Spherical Harmonics analysis of `maps'
 * Copyright (C) 2012 Christian Holm Christensen
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Concept derived from Gaus-LEgendre Sky Pixelization (GLESP). 
 * Home-page: http://www.glesp.nbi.dk/
 * Reference: Int.Jour.M.Phys.D14, 275 (astro-ph/0305537) 
 *    http://www.worldscinet.com/ijmpd/14/1402/S0218271805006183.html
 *
 */
/**
 * @file   Desolver.cxx
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu May  3 12:49:15 2012
 * 
 * @brief  Desolver implementation
 */

#include "Desolver.h"
#include "Map.h"
#include "SphericalHarmonics.h"
#include "Axis.h"

#include <TFile.h>
#include <TList.h>
#include <TKey.h>
#include <TClass.h>
#include <TTree.h>

//____________________________________________________________________
Desolver::Desolver()
  : TObject()
{
}

//____________________________________________________________________
Bool_t
Desolver::Process(const char* input, const char* output)
{
  const Int_t kMaxV = 4;
  struct Event 
  {
    Int_t    evNo;
    Int_t    nPart;
    Double_t phiR;
    Double_t v[kMaxV];
  } event;

  TFile* in = TFile::Open(input, "READ");
  if (!in) { 
    Error("Process", "Failed to open input file %s", input);
    return false;
  }
  TTree* tree = static_cast<TTree*>(in->Get("T"));
  if (!tree) { 
    Error("Process", "Failed to get tree from input %s", input);
    return false;
  }
  
  Axis*  xAxis = static_cast<Axis*>(in->Get("xaxis"));
  if (!xAxis) { 
    Error("Process", "Failed to read X axis from %s", input);
    return false;
  }
  Axis*  yAxis = static_cast<Axis*>(in->Get("yaxis"));
  if (!yAxis) { 
    Error("Process", "Failed to read Y axis from %s", input);
    return false;
  }

  Map* map = 0; 
  tree->SetBranchAddress("map", &map);
  if (tree->GetBranch("event")) { 
    tree->SetBranchAddress("event", &event);
  }
    

  // xAxis->Print();
  // yAxis->Print();

  TFile* out = TFile::Open(output, "RECREATE");
  if (!out) { 
    Error("Process", "Failed to open output file %s", output);
    return false;
  }
  SphericalHarmonics* sph = new SphericalHarmonics;
  TTree* oTree = new TTree("T", "T");
  oTree->Branch("sph", "SphericalHarmonics", &sph, 32000, 1);
  if (tree->GetBranch("event")) { 
    oTree->Branch("event", &event, tree->GetBranch("event")->GetTitle());
  }
  delete sph;


  Int_t n = tree->GetEntries();
  for (Int_t i = 0; i < n; i++) { 
    tree->GetEntry(i);
    
    sph = ProcessMap(map, xAxis, yAxis);
    if (!sph) continue;

    oTree->Fill();

    delete sph;
  }

  map = static_cast<Map*>(in->Get("all"));
  if (!sph) 
    Warning("Process", "Didn't get the map 'all' from input");
  else {
    sph = ProcessMap(map, xAxis, yAxis);
    if (sph) { 
      out->cd();
      sph->Write("all");
      delete sph;
    }
  }
  out->cd();
  xAxis->Write("xaxis");
  yAxis->Write("yaxis");
    
  out->Write();
  out->Close();
  in->Close();

  return true;
}

//____________________________________________________________________
SphericalHarmonics*
Desolver::ProcessMap(Map* m, const Axis* xAxis, const Axis* yAxis) 
{
  if (!m) return 0;

  TString mN(m->GetName());
  mN.ReplaceAll("map", "sph");
  SphericalHarmonics* sph = new SphericalHarmonics(mN.Data());
  m->SetXAxis(xAxis);
  m->SetYAxis(yAxis);
  sph->ProcessMap(*m);
  
  return sph;
}


//____________________________________________________________________
//
// EOF
//

