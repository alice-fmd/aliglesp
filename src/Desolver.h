/* 
 * Spherical Harmonics analysis of `maps'
 * Copyright (C) 2012 Christian Holm Christensen
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Concept derived from Gaus-LEgendre Sky Pixelization (GLESP). 
 * Home-page: http://www.glesp.nbi.dk/
 * Reference: Int.Jour.M.Phys.D14, 275 (astro-ph/0305537) 
 *    http://www.worldscinet.com/ijmpd/14/1402/S0218271805006183.html
 *
 */
/**
 * @file   Desolver.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu May  3 12:49:31 2012
 * 
 * @brief  Desolver declaration 
 */
#ifndef ALIGLESP_DESOLVER
#define ALIGLESP_DESOLVER
#include <TObject.h>
class Map;
class Axis;
class SphericalHarmonics;

/** 
 * @class Desolver 
 *
 * Class to desolve the input maps into spherical harmonis
 *
 * @example DesolveIt.C
 */
class Desolver : public TObject
{
public:
  /** 
   * Constructor
   */
  Desolver();
  /** 
   * Process maps in the input file 
   * 
   * @param input  Input file name 
   * @param output Output file name 
   * 
   * @return true on success. 
   */  
  Bool_t Process(const char* input, const char* output);
private:
  /** 
   * Process a single map and return the spherical harmonics 
   * 
   * @param map    Map to process
   * @param xAxis  X axis used
   * @param yAxis  Y axis usedE
   * 
   * @return Pointer to newly allocated spherical harmonics
   */
  SphericalHarmonics* ProcessMap(Map*        map, 
				 const Axis* xAxis,
				 const Axis* yAxis);

  ClassDef(Desolver,1);
};


#endif
/*
 * Local Variables: 
 *  mode: C++ 
 * End: 
 */
