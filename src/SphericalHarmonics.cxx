/* 
 * Spherical Harmonics analysis of `maps'
 * Copyright (C) 2012 Christian Holm Christensen
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Concept derived from Gaus-LEgendre Sky Pixelization (GLESP). 
 * Home-page: http://www.glesp.nbi.dk/
 * Reference: Int.Jour.M.Phys.D14, 275 (astro-ph/0305537) 
 *    http://www.worldscinet.com/ijmpd/14/1402/S0218271805006183.html
 *
 */
/**
 * @file   SphericalHarmonics.cxx
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu May  3 12:55:00 2012
 * 
 * @brief  Implementation of class to do spherical harmonics analysis
 */
#include "SphericalHarmonics.h"
#include "Axis.h"
#include <TH2D.h>
#include <TVirtualFFT.h>
#include <TMath.h>
#include <Math/SpecFuncMathMore.h>


//____________________________________________________________________
SphericalHarmonics::SphericalHarmonics() 
  : TNamed(),
    fLmax(0),
    fReC(0),
    fImC(0), 
    fReA(0), 
    fImA(0),
    fCl(0),
    fDl(0)
{}

//____________________________________________________________________
SphericalHarmonics::SphericalHarmonics(const char* name) 
  : TNamed(name, ""), 
    fLmax(0),
    fReC(0),
    fImC(0), 
    fReA(0), 
    fImA(0),
    fCl(0),
    fDl(0)
{}

//____________________________________________________________________
SphericalHarmonics::~SphericalHarmonics() 
{
  Clean();
}

//____________________________________________________________________
void
SphericalHarmonics::Clean() 
{
  if (fReC) delete fReC; fReC = 0;
  if (fImC) delete fImC; fImC = 0;
  if (fReA) delete fReA; fReA = 0;
  if (fImA) delete fImA; fImA = 0;
  if (fCl)  delete fCl;  fCl  = 0;
  if (fDl)  delete fDl;  fDl  = 0;
}

//____________________________________________________________________
void
SphericalHarmonics::ProcessMap(const Map& mMap, Short_t ulmax) 
{
  Clean();
  
  Initialize(mMap);
  
  if (ulmax > 0 && ulmax < fLmax) fLmax = ulmax;
  
  fReA->SetBinContent(1, 1, CalculateMonopole(mMap));
  
  Transform(mMap);
  CalculateAllPoles(mMap);
  CalculateCl();
}

//____________________________________________________________________
void
SphericalHarmonics::Initialize(const Map& map) 
{
  TAxis* xaxis = map.GetXAxis().GetAxis();
  if (!xaxis) { 
    Fatal("Initialize", "Failed to get Axis object");
    return;
  }

  Int_t nEta = xaxis->GetNbins();
  Int_t ny   = map.GetMaxNY(); // (nEta+1)/2;

  Double_t* bins = xaxis->GetXbins()->fArray;
  if (bins) {
    // Variable sized bins 
    fReC = new TH2D("reC", "#Rgothic(#bf{c})", nEta, bins, ny+1, -.5, ny+.5);
  }
  else {
    // Fixed bin size 
    fReC = new TH2D("reC", "#Rgothic(#bf{c})", 
		    nEta, xaxis->GetXmin(), xaxis->GetXmax(), 
		    ny+1, -.5, ny+.5);
  }
  fReC->SetXTitle(xaxis->GetTitle());
  fReC->SetYTitle("m");
  fReC->SetZTitle("#Rgothic(#bf{c})");
  fReC->SetStats(0);
  fReC->SetDirectory(0);
    
  fImC = static_cast<TH2D*>(fReC->Clone("imC"));
  fImC->SetTitle("#Jgothic(#bf{c})");
  fImC->SetZTitle("#Jgothic(#bf{c})");
  fImC->SetDirectory(0);

  fLmax = map.GetLmax();
  fReA = new TH2D("reA", "#Rgothic(#bf{a})", 
		  fLmax+1, -.5, fLmax+.5, fLmax+1, -.5, fLmax+.5);
  fReA->SetXTitle("l");
  fReA->SetYTitle("m");
  fReA->SetZTitle("#Rgothic(#bf{a})");
  fReA->SetStats(0);
  fReA->SetDirectory(0);

  fImA = static_cast<TH2D*>(fReA->Clone("imA"));
  fImA->SetTitle("#Jgothic(#bf{a})");
  fImA->SetZTitle("#Jgothic(#bf{a})");
  fImA->SetDirectory(0);

  fCl = new TH1D("cl", "Power spectrum", fLmax+1, .5, fLmax+.5);
  fCl->SetXTitle("l");
  fCl->SetYTitle("C(l)");
  fCl->SetStats(0);
  fCl->SetLineColor(kRed+2);
  fCl->SetFillColor(kRed+2);
  fCl->SetFillStyle(3001);
  fCl->SetDirectory(0);
    
  fDl = static_cast<TH1D*>(fCl->Clone("dl"));
  fDl->SetTitle("Reduced power spectrum");
  fDl->SetYTitle("D(l)");
  fDl->SetLineColor(kBlue+2);
  fDl->SetFillColor(kBlue+2);
  fDl->SetFillStyle(3001);
  fDl->SetDirectory(0);  
}

//____________________________________________________________________
Double_t
SphericalHarmonics::CalculateMonopole(const Map& map) const
{
  Double_t monopole = 0;

  for (Int_t i = 1; i <= map.GetNX(); i++) {
    Int_t ny = map.GetNY(i);
    for (Int_t j = 1; j <= ny; j++) {
      monopole += map.GetValue(i, j, true);
    }
  }
  return monopole / TMath::Sqrt(4 * TMath::Pi());
}

//____________________________________________________________________
void
SphericalHarmonics::Transform(const Map& map)
{
  // Info("Transform", "Doing Fourier transform up to l=%d", fLmax);

  fReC->Reset();
  fImC->Reset();

  UShort_t     nx  = map.GetNX();
  // UShort_t     oy = fReC->GetNbinsY(); 
  for (UShort_t i = 0; i < nx; i++) { 
    Int_t ny = map.GetNY(i);
    if (ny == 0) continue;

    TVirtualFFT* fft = TVirtualFFT::FFT(1, &ny, "R2HC ES K");
    TArrayD  points(fft->GetN()[0]); // fMap->GetNbinsY());

    // Copy one cos(theta) slice to temporary array 
    for (UShort_t j = 0; j < ny; j++) {
      Double_t c = map.GetValue(i+1, j+1); 
      points.SetAt(c, j);
    }
    fft->SetPoints(points.fArray);
    fft->Transform();

    for (UShort_t j = 0; j < ny; j++) {
      Double_t re = 0;
      Double_t im = 0;
      fft->GetPointComplex(j, re, im);
      fReC->SetBinContent(i+1, j+1, -re);
      fImC->SetBinContent(i+1, j+1, im);
    }
    delete fft;
  }
}

//____________________________________________________________________
void
SphericalHarmonics::CalculateAllPoles(const Map& map, 
				      Double_t   eps)
{
  // Info("CalculateAllPoles", "Calculating multipoles up to l=%d", fLmax);
  UShort_t       nx        = map.GetNX();
  
  Double_t monopole = fReA->GetBinContent(1,1);
  fImA->SetBinContent(1,1,0);
  for (UShort_t l = 0; l <= fLmax; l++) { 
    // Loop up to current l 
    for (UShort_t m = 0; m <= l; m++) { 
      Int_t bin = fReA->FindBin(l, m);

      // Loop over x-axis and calculate Legendre 
      for (UShort_t i = 0; i < nx; i++) { 
	// Middle of bin 
	Double_t theta = map.GetBinTheta(i+1,1);
	Double_t api   = fReC->GetBinContent(i+1, m+1);
	Double_t bpi   = fImC->GetBinContent(i+1, m+1);

	// Calculate the sperical harmonic for l,m at xc
	Double_t gl = ROOT::Math::sph_legendre(l, m, theta);
	if (TMath::Abs(gl) <= eps) {
	  Warning("CalculateAllPoles", 
		  "Contribution from l=%d m=%d at theta=%f %g < %g", 
		  l, m, theta, gl, eps);
	  continue;
	}

	fReA->AddBinContent(bin, -gl * api);
	fImA->AddBinContent(bin,  gl * bpi);
      }
    }
  }
  fReA->SetBinContent(1,1, monopole);
  // Normalize to phi integral 
}
 
//____________________________________________________________________
void
SphericalHarmonics::CalculateCl()
{
  // Info("CalculateCl", "Calculating power spectrum up to l=%d", fLmax);
  fCl->Reset();
  fDl->Reset();
  const Double_t kTcmb = TMath::Power(2.726, 2);

  for (UShort_t l = 0; l <= fLmax; l++) { 
    Double_t sAlm = TMath::Power(fReA->GetBinContent(l+1,1),2);
    for (UShort_t m = 1; m <= l; m++) {
      sAlm += 2 * (TMath::Power(fReA->GetBinContent(l+1,m+1),2) + 
		   TMath::Power(fImA->GetBinContent(l+1,m+1),2));
    }
    Double_t cl = sAlm / (2*l+1);
    fCl->SetBinContent(l+1, cl);
    fDl->SetBinContent(l+1, cl * (l*(l+1)) / TMath::TwoPi() / kTcmb);
  }
}

//____________________________________________________________________
void
SphericalHarmonics::Draw(Option_t* option)
{
  TString opt(option);
  TH1* h = 0;
  const char*  hists[] = { "reC", "imc", "rea", "ima", "cl", "dl" };
  for (Int_t id = 0; id < 6; id++) { 
    Int_t idx = opt.Index(hists[id], 0, TString::kIgnoreCase);
    if (idx == kNPOS) continue;
    
    opt.Remove(idx, (id < 4 ? 3 : 2));
    switch (id) { 
    case 0:   h = GetReC(); break;
    case 1:   h = GetImC(); break;
    case 2:   h = GetReA(); break;
    case 3:   h = GetImA(); break;
    case 4:   h = GetCl();  break;
    case 5:   h = GetDl();  break;
    }
    break;
  }
  if (!h) { 
    Warning("Draw", "No histogram specified in %s - drawing CL", option);
    h = fCl;
  }

  h->Draw(opt);
}

//____________________________________________________________________
Double_t 
SphericalHarmonics::GetReAlm(UShort_t l, UShort_t m) const 
{ 
  return fReA->GetBinContent(fReA->FindBin(l,m)); 
}

//____________________________________________________________________
Double_t 
SphericalHarmonics::GetImAlm(UShort_t l, UShort_t m) const 
{ 
  return fImA->GetBinContent(fImA->FindBin(l,m)); 
}

//____________________________________________________________________
Double_t 
SphericalHarmonics::GetAlmMag2(UShort_t l, UShort_t m) const
{
  Double_t re = GetReAlm(l,m);
  Double_t im = GetImAlm(l,m);
  return re*re + im*im;
}

//____________________________________________________________________
Double_t 
SphericalHarmonics::GetAlmMag(UShort_t l, UShort_t m) const
{
  return TMath::Sqrt(GetAlmMag2(l,m));
}

//____________________________________________________________________
Double_t 
SphericalHarmonics::GetAlmPhase(UShort_t l, UShort_t m) const
{
  Double_t re = GetReAlm(l,m);
  Double_t im = GetImAlm(l,m);
  return (re != 0 || im != 0 ? TMath::ATan2(im, re) : 0);
}

//____________________________________________________________________
Double_t 
SphericalHarmonics::GetReCxm(UShort_t ix, UShort_t m) const 
{ 
  Int_t ybin = fReC->GetYaxis()->FindBin(m);
  Int_t bin  = fReC->GetBin(ix, ybin);
  return fReC->GetBinContent(bin); 
}

//____________________________________________________________________
Double_t 
SphericalHarmonics::GetImCxm(UShort_t ix, UShort_t m) const 
{ 
  Int_t ybin = fImC->GetYaxis()->FindBin(m);
  Int_t bin  = fImC->GetBin(ix, ybin);
  return fImC->GetBinContent(bin); 
}

//____________________________________________________________________
Double_t 
SphericalHarmonics::GetCxmMag2(UShort_t ix, UShort_t m) const
{
  Double_t re = GetReCxm(ix,m);
  Double_t im = GetImCxm(ix,m);
  return re*re + im*im;
}

//____________________________________________________________________
Double_t 
SphericalHarmonics::GetCxmMag(UShort_t ix, UShort_t m) const
{
  return TMath::Sqrt(GetCxmMag2(ix,m));
}

//____________________________________________________________________
Double_t 
SphericalHarmonics::GetCxmPhase(UShort_t ix, UShort_t m) const
{
  Double_t re = GetReCxm(ix,m);
  Double_t im = GetImCxm(ix,m);
  return (re != 0 || im != 0 ? TMath::ATan2(im, re) : 0);
}
//
// EOF
//
