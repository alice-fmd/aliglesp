/* 
 * Spherical Harmonics analysis of `maps'
 * Copyright (C) 2012 Christian Holm Christensen
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Concept derived from Gaus-LEgendre Sky Pixelization (GLESP). 
 * Home-page: http://www.glesp.nbi.dk/
 * Reference: Int.Jour.M.Phys.D14, 275 (astro-ph/0305537) 
 *    http://www.worldscinet.com/ijmpd/14/1402/S0218271805006183.html
 *
 */
/**
 * @file   GausLegendreAxis.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu May  3 12:52:30 2012
 * 
 * @brief  Declaration of Gaus-Legendre Axis 
 */
#ifndef ALIGLESP_GAUSLEGENDREAXIS
#define ALIGLESP_GAUSLEGENDREAXIS
#include <Axis.h>
#include <TArrayD.h>
#include <TH1D.h>
class TAxis;

/** 
 * An axis based on Gaus-Legendre curvatures 
 */
class GausLegendreAxis : public Axis
{
public:
  /** 
   * Constructor
   */
  GausLegendreAxis();
  /** 
   * Constructor 
   * 
   * @param nx   Number of bins
   * @param min  Minimum 
   * @param max  Maximum
   */
  GausLegendreAxis(UShort_t nx, Double_t min=-1, Double_t max=1);
  /** 
   * Copy constructor 
   * 
   * @param o Object to copy from 
   */
  GausLegendreAxis(const GausLegendreAxis& o);
  /** 
   * Assignment operator
   * 
   * @param o Object to assign from 
   * 
   * @return Reference this this object
   */
  GausLegendreAxis& operator=(const GausLegendreAxis& o);

  /** 
   * Get the number of bins 
   * 
   * @return Number of bins 
   */
  UShort_t GetNBins(UShort_t=0) const { return 2*fAbscissa.GetSize(); }
  /** 
   * Get the 'centre' value @f$ \theta@f$ corresponding to
   * this bin
   * 
   * @param bin Bin number 
   * 
   * @return 'centre' value @f$ \theta@f$ 
   */
  Double_t GetBinX(UShort_t bin,UShort_t=0) const;
  /** 
   * Get the weight of this bin.  Note, one should also scale by the
   * phi bin size.
   * 
   * @param bin  Bin number
   * 
   * @return Weight
   */
  Double_t GetBinWeight(UShort_t bin,UShort_t=0) const;
  /** 
   * Find bin corresponding to @f$ \theta@f$ 
   * 
   * @param x @f$ \theta@f$
   * 
   * @return Bin number 
   */
  UShort_t FindBin(Double_t x,UShort_t=0) const;
  /** 
   * Get the ROOT axis corresponding to this axis 
   * 
   * @return Pointer to axis object
   */
  TAxis* GetAxis() const { return fWeights.GetXaxis(); }

  /** 
   * @{
   * @name ROOT utilities 
   */
  /** 
   * Draw this 
   * 
   * @param opt 
   */
  void Draw(Option_t* opt="") //*MENU*
  { 
    fWeights.Draw(opt); 
  } 
  /** 
   * Print this axis 
   * 
   * @param option Not used 
   */
  void Print(Option_t* option="") const;
  /** 
   * Whether this should be considered a folder
   * 
   * @return Always true 
   */
  Bool_t IsFolder() const { return true; }
  /* @} */
private:
  /** Weights */
  TH1D fWeights;
  /** Abscissa in Gauss-Legendre curvature */
  TArrayD fAbscissa;

  /** 
   * @{
   * @name Gauss-Legendre Curvature 
   */
  /**
   * Abscissas and weights for the Gauss-Legendre quadrature. 
   * 
   * Algorithm from "Numerical Recipies" - routine @c gauleg
   *
   * @param abs     On return, half the abcissa mid-points 
   * @param weights On return, half the weights 
   * @param n       Order of polynomials 
   * @param x1      Lower bound 
   * @param x2      Upper bound 
   * @param eps     Precision for evaluation 
   *
   * @note An alternative to this would be the GSL function 
   * @c gsl_integration_glfixed_table_alloc.  However, there's no
   * interface to that in ROOT so we stick with this one. 
   */
  static void GaussLegendreIntegral(TArrayD& abs, 
				    TArrayD& weights,
				    UShort_t n, 
				    Double_t x1=-1, 
				    Double_t x2=1, 
				    Double_t eps=1e-13);
  /**
   * Abscissas and weights for the Gauss-Legendre quadrature
   * 
   * @param z     where (in -1 to 1) to evaluate the Gauss-Legendre
   *              quadrature. On return, this contains the evaluation 
   * @param order Up to which order 
   * @param eps   Wanted precision 
   *
   * @return The weight.
   */
  static Double_t GaussLegendreKernel(Double_t& z, UShort_t order, 
				      Double_t eps=1e-13);
  /* @} */

  ClassDef(GausLegendreAxis,1); 
};

#endif
/*
 * Local Variables: 
 *  mode: C++ 
 * End:
 */
