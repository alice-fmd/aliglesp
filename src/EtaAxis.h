/* 
 * Spherical Harmonics analysis of `maps'
 * Copyright (C) 2012 Christian Holm Christensen
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Concept derived from Gaus-LEgendre Sky Pixelization (GLESP). 
 * Home-page: http://www.glesp.nbi.dk/
 * Reference: Int.Jour.M.Phys.D14, 275 (astro-ph/0305537) 
 *    http://www.worldscinet.com/ijmpd/14/1402/S0218271805006183.html
 *
 */
/**
 * @file   EtaAxis.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu May  3 12:50:48 2012
 * 
 * @brief  Declaration of equal bin size @f$\eta@f$ axis 
 */
#ifndef ALIGLESP_ETAXIS
#define ALIGLESP_ETAXIS
#include <EquidistantAxis.h>
#include <TMath.h>

/** 
 * An @f$\eta@f$ axis divided into equal parts 
 */
class EtaAxis : public EquidistantAxis
{
public:
  /**
   * Constructor for I/O
   */
  EtaAxis() : EquidistantAxis() {}
  /** 
   * Constructor 
   * 
   * @param nx   Number of bins 
   * @param low  Least pseudo rapidity
   * @param high Largest pseudo rapidity
   */
  EtaAxis(UShort_t nx, Double_t low, Double_t high) 
    : EquidistantAxis("#eta", nx, low, high)
  {}
  
  /**
   * Get the 'centre' value @f$ \theta@f$ corresponding to
   * this bin
   *
   * @param bin Bin number
   *
   * @return 'centre' value @f$ \theta@f$
   */
  virtual Double_t GetBinX(UShort_t bin,UShort_t=0) const 
  { 
    return Eta2Theta(EquidistantAxis::GetBinX(bin));
  }
  /**
   * Find bin corresponding to @f$ x=\theta@f$
   *
   * @param x @f$ \theta@f$
   *
   * @return Bin number
   */
  virtual UShort_t FindBin(Double_t x,UShort_t=0) const 
  { 
    return EquidistantAxis::FindBin(Theta2Eta(x)); 
  }
  /** 
   * Calculate @f$\theta@f$ from @f$\eta@f$:
   *
   * @f{eqnarray*}{
   *                              \eta = -\log\left(\tan(\theta/2)\right)\\
   *                             -\eta = \log\left(\tan(\theta/2)\right)\\
   *                         e^{-\eta} = \tan(\theta/2)\\
   *   \tan^{-1}\left(e^{-\eta}\right) = \theta / 2\\
   * 2 \tan^{-1}\left(e^{-\eta}\right) = \theta / 2
   * @f} 
   *
   * @param eta @f$\eta@f$
   * 
   * @return @f$\theta@f$ (in radians)
   */
  static Double_t Eta2Theta(Double_t eta) 
  {
    return 2 * TMath::ATan(TMath::Exp(-eta));
  }
  /** 
   * Calculate @f$\eta@f$ from @f$\theta@f$:
   * @f[
   *   \eta = - \log\left(\tan(\theta/2)\right)
   * @f]
   *
   * @param theta Polar angle (in radians)
   * 
   * @return Pseudo-rapidity @f$\eta@f$ 
   */
  static Double_t Theta2Eta(Double_t theta)
  {
    return - TMath::Log(TMath::Tan(theta/2));
  }

  ClassDef(EtaAxis, 1);
};

#endif
/* 
 * Local Variables: 
 *  mode: C++ 
 * End:
 */
