#include "Flower.h"
#include <TMath.h>
#include <TTree.h>
#include <TFile.h>
#include <TH2.h>
#include "SphericalHarmonics.h"

//____________________________________________________________________
Flower::Flower()
  : TObject()
{

}

//____________________________________________________________________
Flower::~Flower()
{

}

//____________________________________________________________________
Bool_t
Flower::Process(const char* input)
{
  const Int_t kMaxV = 4;
  struct Event 
  {
    Int_t    evNo;
    Int_t    nPart;
    Double_t phiR;
    Double_t v[kMaxV];
  } event;

  TFile* in = TFile::Open(input, "READ");
  if (!in) { 
    Error("Process", "Failed to open input file %s", input);
    return false;
  }
  TTree* tree = static_cast<TTree*>(in->Get("T"));
  if (!tree) { 
    Error("Process", "Failed to get tree from input %s", input);
    return false;
  }

  SphericalHarmonics* sph = 0; 
  tree->SetBranchAddress("sph", &sph);
  if (tree->GetBranch("event")) { 
    tree->SetBranchAddress("event", &event);
  }
  
  Int_t n = tree->GetEntries();
  for (Int_t i = 0; i < n; i++) { 
    tree->GetEntry(i);
    Info("Process", "Event # %3d, %s", i, sph->GetName());

    for (Int_t j = 0; j < kMaxV; j++) { 
      Int_t    nV = j+1;
      Double_t iV = event.v[j];
      Double_t sV = GetVN(*sph, nV);
      
      Info("", " Input v%d=%10f, Analysed v%d=%10f", nV, iV, nV, sV);
    }
  }

  in->Close();

  return true;
}

//____________________________________________________________________
Double_t 
Flower::GetVN(const SphericalHarmonics& sph, Int_t n) const
{
  // TH2* reA = sph.GetReA();
  // TH2* imA = sph.GetImA();
  // Int_t    bin  = n+1;
  // Double_t rAn0 = reA->GetBinContent(bin,1);
  // Double_t iAn0 = imA->GetBinContent(bin,1);
  // Double_t rAnn = reA->GetBinContent(bin,bin);
  // Double_t iAnn = imA->GetBinContent(bin,bin);
  // Double_t nAn0 = TMath::Sqrt(rAn0 * rAn0 + iAn0 * iAn0);
  // Double_t nAnn = TMath::Sqrt(rAnn * rAnn + iAnn * iAnn);
  Double_t nAn0 = sph.GetAlmMag(n, 0); 
  Double_t nAnn = sph.GetAlmMag(n, n);

  return nAn0 ? nAnn / nAn0 : 0;

}

//____________________________________________________________________
//
// EOF
//
