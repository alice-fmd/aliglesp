/* 
 * Spherical Harmonics analysis of `maps'
 * Copyright (C) 2012 Christian Holm Christensen
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Concept derived from Gaus-LEgendre Sky Pixelization (GLESP). 
 * Home-page: http://www.glesp.nbi.dk/
 * Reference: Int.Jour.M.Phys.D14, 275 (astro-ph/0305537) 
 *    http://www.worldscinet.com/ijmpd/14/1402/S0218271805006183.html
 *
 */
/**
 * @file   SpecialAxis.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu May  3 12:54:48 2012
 * 
 * @brief  Declaration of quasi-equal area @f$\phi@f$ axis 
 */
#ifndef ALIGLESP_SPECIALAXIS
#define ALIGLESP_SPECIALAXIS
#include <Axis.h>
#include <TMath.h>
#include <TH1S.h>


/**
 * An axis that modifies the bin width according to the other axis,
 * such that the area of each bin is quasi constant.
 * 
 */
class SpecialAxis : public Axis
{
public: 
  /**
   * Constructor
   */
  SpecialAxis() : Axis(), fNBins(), fAxis() {}
  /** 
   * Constructor 
   * 
   * @param other Other axis for reference  
   * @param maxN  Maximum number of bins 
   * @param min   Least value 
   * @param max   Largest value 
   */
  SpecialAxis(const Axis& other, UShort_t maxN, 
	      Double_t min=0, Double_t max=TMath::TwoPi());
  /**
   * Get the number of bins
   *
   * @param bin Bin number on other axis - if needed
   *
   * @return Number of bins
   */
  virtual UShort_t GetNBins(UShort_t bin) const 
  {
    return fNBins.GetBinContent(bin);
  }
  /** 
   * Get the maximum number of bins 
   * 
   * @return Maximum number of bins 
   */
  virtual UShort_t GetMaxNBins() const { return fAxis.GetNbins(); }
  /**
   * Get the 'centre' value @f$\theta@f$ corresponding to
   * this bin
   *
   * @param bin Bin number
   * @param other Bin number on other axis, if needed
   *
   * @return 'centre' value @f$ \theta@f$
   */
  virtual Double_t GetBinX(UShort_t bin, UShort_t other=0) const;
  /**
   * Get the weight of this bin.  Note, one should also scale by the
   * phi bin size.
   *
   * @param bin  Bin number
   * @param other Bin number on other axis, if needed
   *
   * @return Weight
   */
  virtual Double_t GetBinWeight(UShort_t bin, UShort_t other=0) const;
  /**
   * Find bin corresponding to @f$ \theta@f$
   *
   * @param theta @f$\theta@f$
   * @param other Bin number on other axis, if needed
   *
   * @return Bin number
   */
  virtual UShort_t FindBin(Double_t theta, UShort_t other=0) const;
  /**
     * Get the ROOT axis corresponding to this axis
     *
     * @return Pointer to axis object
     */
  virtual TAxis* GetAxis() const { return const_cast<TAxis*>(&fAxis); }
  /** 
   * Whether this axis depends on the bins of the another axis. 
   * 
   * @return Normally false
   */
  virtual Bool_t IsDependent() const { return true; }

  /** 
   * @{
   * @name ROOT utilities 
   */
  /** 
   * Draw this axis 
   * 
   * @param option Passed to THistPainter::Paint
   */
  void Draw(Option_t* option="") //*MENU*
  {
    fNBins.Draw(option);
  }
  /** 
   * Print this axis 
   * 
   * @param option Not used 
   */
  void Print(Option_t* option="") const;
  /** 
   * Whether this should be considered a folder
   * 
   * @return Always true 
   */
  Bool_t IsFolder() const { return true; }
  /* @} */
    
    
protected:
  TH1S     fNBins;
  TAxis    fAxis;

  ClassDef(SpecialAxis,1);
};

#endif
/*
 * Local Variables:
 *  mode: C++
 * End:
 */
