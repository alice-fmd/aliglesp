/* 
 * Spherical Harmonics analysis of `maps'
 * Copyright (C) 2012 Christian Holm Christensen
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Concept derived from Gaus-LEgendre Sky Pixelization (GLESP). 
 * Home-page: http://www.glesp.nbi.dk/
 * Reference: Int.Jour.M.Phys.D14, 275 (astro-ph/0305537) 
 *    http://www.worldscinet.com/ijmpd/14/1402/S0218271805006183.html
 *
 */
/**
 * @file   PhiAxis.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu May  3 12:51:23 2012
 * 
 * @brief  Declaration of equal bin size @f$\phi@f$ axis 
 */
#ifndef ALIGLESP_PHIXIS
#define ALIGLESP_PHIXIS
#include <EquidistantAxis.h>
#include <TMath.h>

/**
 * A straight up equidistant phi axis 
 * 
 */
class PhiAxis : public EquidistantAxis
{
public:
  PhiAxis() : EquidistantAxis() {}
  PhiAxis(UShort_t n, Double_t low=0, Double_t high=TMath::TwoPi()) 
    : EquidistantAxis("#phi", n, TMath::Max(low,0.), 
		      TMath::Min(high,TMath::TwoPi()))
  {}
  
  /**
   * Get the 'centre' value @f$ \phi@f$ corresponding to
   * this bin
   *
   * @param bin Bin number
   *
   * @return 'centre' value @f$ \phi@f$
   */
  virtual Double_t GetBinX(UShort_t bin,UShort_t=0) const 
  {
    return EquidistantAxis::GetBinX(bin);
    // return fAxis.GetBinLowEdge(bin);
  }
  /**
   * Find bin corresponding to @f$ \phi@f$
   *
   * @param x @f$ \phi@f$
   *
   * @return Bin number
   */
  virtual UShort_t FindBin(Double_t x,UShort_t=0) const 
  { 
    // This is how the phi bin is calculated in GLESP - a little funny,
    // in that it goes to the next bin if close to the upper bin
    // boundary
    Int_t ny  = fAxis.GetNbins();
    Int_t bin = Int_t(x / TMath::TwoPi() * ny + .5) % ny; 
    return bin + 1;
  }

  ClassDef(PhiAxis,1);
};

#endif
/* 
 * Local Variables: 
 *  mode: C++ 
 * End:
 */
