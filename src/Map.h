/** 
 * @page copyright License 
 *
 * Spherical Harmonics analysis of `maps'
 * Copyright (C) 2012 Christian Holm Christensen
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Concept derived from Gaus-LEgendre Sky Pixelization (GLESP). 
 * - Home-page: http://www.glesp.nbi.dk/
 * - Reference: Int.Jour.M.Phys.D14, 275 (astro-ph/0305537) 
 *    http://www.worldscinet.com/ijmpd/14/1402/S0218271805006183.html
 *
 */
/**
 * @file   Map.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu May  3 12:53:18 2012
 * 
 * @brief  Base class for maps 
 */
#ifndef ALIGLESP_MAP
#define ALIGLESP_MAP 
#include <TNamed.h>
// #include <TMath.h>
class Axis;

/**
 * @class Map 
 *
 * Base class for maps.  This class defines an interface - nothing
 * else.  Some static helper functions are also implementedx 
 *
 * @example Draw.C 
 */
class Map : public TNamed 
{ 
public:
  /**
   * Destructor 
   */
  virtual ~Map() {}
  /** 
   * Clone this map
   * 
   * @param name New name 
   * 
   * @return Newly allocated empty map 
   */
  virtual Map* CloneThis(const char* name) const = 0;

  virtual const Axis& GetXAxis() const { return *fXAxis; }
  virtual const Axis& GetYAxis() const { return *fYAxis; }
  
  /** 
   * @{
   * @name Methods for filling map 
   */
  /** 
   * Fill into map at @f$ (\theta,\phi)@f$ with weight @f$ w@f$. 
   * 
   * @param theta Polar angle in radians 
   * @param phi   Azimuthal angle in radians 
   * @param w     Weight 
   */
  virtual void Fill(Double_t theta, Double_t phi, Double_t w=1) = 0;
  /** 
   * Fill into map at @f$ (\theta,\phi)@f$ with weight @f$ w@f$, where 
   * @f[ 
   *  \theta = 2 \tan^{-1}\left(e^{-\eta}\right)
   * @f]
   * 
   * @param eta   Pseudo-rapidity
   * @param phi   Azimuthal angle in radians
   * @param w     Weight 
   */
  virtual void FillEta(Double_t eta, Double_t phi, Double_t w=1) = 0;
  /* @} */


  /** 
   * @{ 
   * @name Calculations 
   */
  /** 
   * Get the maximum number of moments 
   * 
   * @return Maximum possible number of components 
   */  
  virtual UShort_t GetLmax() const = 0;
  /** 
   * Normalize (or weight) the map 
   * 
   * @return The monopole value 
   */
  virtual Double_t Normalize(ULong_t hNits, ULong_t nEvents) = 0;
  /* @} */

  /** 
   * @{ 
   * @name Getters and setters 
   */
  /** 
   * Get the number of bins along X 
   * 
   * @return Number of bins along X 
   */
  virtual UShort_t GetNX() const = 0;
  /** 
   * Get the number of bins along Y 
   * 
   * @return Number of bins along Y 
   */
  virtual UShort_t GetNY(UShort_t xbin) const = 0;
  /** 
   * Get the maximum number of bins along Y 
   * 
   * @return Maximum number of bins along Y
   */
  virtual UShort_t GetMaxNY() const = 0;
  /** 
   * Get the value of the map a given point 
   * 
   * @param xbin       X bin
   * @param ybin       Y bin 
   * @param weighted   If true, the normalized value 
   * 
   * @return Value 
   */
  virtual Double_t GetValue(UShort_t xbin, 
			    UShort_t ybin, 
			    bool weighted=true) const = 0;
  /** 
   * Get the @f$\theta@f$ value associated with a bin 
   * 
   * @param xbin Bin number (1 is the first bin) - if Y dependent on X
   * @param ybin Bin number (1 is the first bin)
   * 
   * @return @f$\theta@f$ value
   */
  virtual Double_t GetBinTheta(UShort_t xbin, UShort_t ybin) const = 0;
  /** 
   * Get the @f$\phi@f$ value associated with a bin 
   * 
   * @param xbin Bin number (1 is the first bin) - if Y dependent on X
   * @param ybin Bin number (1 is the first bin)
   * 
   * @return @f$\phi@f$ value
   */
  virtual Double_t GetBinPhi(UShort_t xbin, UShort_t ybin) const = 0;
  /* @} */

  /** 
   * @{ 
   * @name Setters used when processing this map 
   */
  /** 
   * Set the X axis
   * 
   * @param axis X axis to use 
   */
  void SetXAxis(const Axis* axis) { fXAxis = axis; }
  /** 
   * Set the Y axis
   * 
   * @param axis Y axis to use 
   */
  void SetYAxis(const Axis* axis) { fYAxis = axis; }
  /* @} */
  /** 
   * @{
   * @name ROOT utilities 
   */
  /* @} */
protected:
  /** 
   * Default constructor 
   */
  Map() : TNamed(), fXAxis(0), fYAxis(0) {}
  /** 
   * Named user constructor 
   * 
   * @param name  Name of object 
   * @param xaxis X axis to use 
   * @param yaxis Y axis to use 
   */
  Map(const char* name, const Axis* xaxis, const Axis* yaxis) 
    : TNamed(name, "Map"), fXAxis(xaxis), fYAxis(yaxis) 
  {}
  /** 
   * Copy constructor - dummy
   * 
   * @param o Object to copy from
   */
  Map(const Map& o) : TNamed(o), fXAxis(o.fXAxis), fYAxis(o.fYAxis) {}
  /** 
   * Assignment operator - dummy
   * 
   * @return Reference to this object
   */
  Map& operator=(const Map& o) 
  { 
    fXAxis = o.fXAxis;
    fYAxis = o.fYAxis;
    TNamed::operator=(o); 
    return *this; 
  }

  // --- Utilities ---------------------------------------------------
  /** 
   * @{
   * @name Static utilities 
   */
  /** 
   * Make sure the angle is in @f$ [0,2\pi[@f$ 
   * 
   * @param ang Input angle (in radians)
   * 
   * @return Normalized angle (in radians)
   */
  static Double_t NormalizeAngle(Double_t ang) 
  {
    // if (ang < 0)              ang += 2*TMath::Pi();
    // if (ang >= 2*TMath::Pi()) ang -= 2*TMath::Pi();
    return ang;
  }
  /* @} */ 

  const Axis* fXAxis; //! Not owned
  const Axis* fYAxis; //! Now owned
private:
  

  ClassDef(Map,1);
};


#endif
/**
 * @mainpage 
 *
 * The package contains code to do analysis of maps in terms of
 * spherical harmonics.
 *
 * Concept derived from Gaus-LEgendre Sky Pixelization (GLESP). 
 * - Home-page: http://www.glesp.nbi.dk/
 * - Reference: Int.Jour.M.Phys.D14, 275 (astro-ph/0305537) 
 *    http://www.worldscinet.com/ijmpd/14/1402/S0218271805006183.html
 *
 * @section Structure 
 * @subsection Maps 
 * 
 * Maps are stored in objects of the class Map.  This class is in it
 * self a base class with a few (currently 1) concrete implementation. 
 * 
 *  - HistogramMap.   A map backed by ROOT histograms. 
 *
 * @subsection Axis
 *
 * Each map must have two axis defined.  One for the polar angle
 * @f$\theta@f$ and one for the azimuthal angle @f$\phi@f$.  The base
 * class Axis encodes the axis behaviour.  Concrete axis classes
 * include
 * 
 * - EtaAxis.  An axis defined in terms of equal-size bins in
 *   @f$\eta=-\log\left(\tan(\theta/2)\right)@f$. 
 * - PhiAxis.  An axis defined in terms of equal-size bins in the
 *   azimuthal angle @f$\phi@f$.
 * - GausLegendreAxis.  An axis defined by Gauss-Legendre curvatures 
 *   in @f$\cos(\theta)@f$ 
 * - SpecialAxis.  An axis defined to make quasi-equal sized bins 
 *   in @f$\phi@f$ given a bin size along @f$\theta@f$ 
 *
 * @subsection Spherical Harmonics 
 *
 * The class SphericalHarmonics calculates the spherical harmonic
 * coefficients @f$ a_{lm}@f$ from a given input map (object of the
 * class Map or it's decendants).  The data is stored in ROOT
 * histograms internally.
 * 
 * @subsection Workers 
 * 
 * The class Mapper maps data in various formats to (a set of) Map
 * objects, and stores these in a ROOT file.
 * 
 * The class Desolver takes the output of the Mapper class and
 * calculates the spherical harmonics for each map found in the input
 * ROOT file.  The results are stored in another ROOT file.
 *
 * @section Examples 
 * 
 * The script MapIt.C and DesolveIt.C are examples of running the
 * Mapper and Desolver class.  The script Draw.C draws the results of
 * one or more of these runs.
 */

/*
 * Local Variables: 
 *  mode:  C++ 
 * End:
 */
