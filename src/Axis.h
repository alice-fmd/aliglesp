/* 
 * Spherical Harmonics analysis of `maps'
 * Copyright (C) 2012 Christian Holm Christensen
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Concept derived from Gaus-LEgendre Sky Pixelization (GLESP). 
 * Home-page: http://www.glesp.nbi.dk/
 * Reference: Int.Jour.M.Phys.D14, 275 (astro-ph/0305537) 
 *    http://www.worldscinet.com/ijmpd/14/1402/S0218271805006183.html
 *
 */
/**
 * @file   Axis.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu May  3 12:48:53 2012
 * 
 * @brief  Axis base class
 */
#ifndef ALIGLESP_AXIS_H
#define ALIGLESP_AXIS_H
#include <TObject.h>
class TAxis;

/**
 * Base class for axis definitions.  The 'x' is assumed to be an angle
 * (phi or theta)
 * 
 */
class Axis : public TObject
{
public:
  Axis() {}
  virtual ~Axis() {}

  /**
   * Get the number of bins
   *
   * @param bin Bin number on other axis - if needed
   *
   * @return Number of bins
   */
  virtual UShort_t GetNBins(UShort_t bin=0) const = 0;
  /** 
   * Get the maximum number of bins possible 
   * 
   * @return Maximum number of bins 
   */
  virtual UShort_t GetMaxNBins() const { return GetNBins(); }
  /**
   * Get the 'centre' value @f$\theta@f$ corresponding to
   * this bin
   *
   * @param bin Bin number
   * @param other Bin number on other axis, if needed
   *
   * @return 'centre' value @f$ \theta@f$
   */
  virtual Double_t GetBinX(UShort_t bin, UShort_t other=0) const = 0;
  /**
   * Get the weight of this bin.  Note, one should also scale by the
   * phi bin size.
   *
   * @param bin  Bin number
   * @param other Bin number on other axis, if needed
   *
   * @return Weight
   */
  virtual Double_t GetBinWeight(UShort_t bin, UShort_t other=0) const = 0;
  /**
   * Find bin corresponding to @f$ \theta@f$
   *
   * @param theta @f$ \theta@f$
   * @param other Bin number on other axis, if needed
   *
   * @return Bin number
   */
  virtual UShort_t FindBin(Double_t theta, UShort_t other=0) const = 0;
  /**
     * Get the ROOT axis corresponding to this axis
     *
     * @return Pointer to axis object
     */
  virtual TAxis* GetAxis() const { return 0; }
  /** 
   * Whether this axis depends on the bins of the another axis. 
   * 
   * @return Normally false
   */
  virtual Bool_t IsDependent() const { return false; }
protected:
  Axis(const Axis& o) : TObject(o) {}

  ClassDef(Axis,1);
};

#endif /* AXIS_H_ */
/*
 * Local Variables: 
 *  mode: C++ 
 * End:
 */

