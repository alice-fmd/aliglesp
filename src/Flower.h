#ifndef ALIGLESP_FLOWER
#define ALIGLESP_FLOWER
#include <TObject.h>
class SphericalHarmonics;

/**
 * @class Flower 
 * Class to calculate the flow harmonics from the spherical harmonics 
 *
 * @example FlowIt.C 
 */
class Flower : public TObject
{
public:
  /** 
   * Constructor
   */
  Flower();
  /** 
   * Destructor
   */
  virtual ~Flower();
  /** 
   * Process the input
   * 
   * @param input Input file 
   * 
   * @return true on success
   */
  Bool_t Process(const char* input);

protected:
  /** 
   * Calculate @f$v_{n}@f$ for a given set of spherical harmonics
   * 
   * @param sph set of spherical harmonics
   * @param n   order @f$n@f$ of @f$v_{n}@f$ 
   * 
   * @return @f$v_{n}@f$ 
   */
  Double_t GetVN(const SphericalHarmonics& sph, Int_t n) const;

  ClassDef(Flower,1);
};

#endif
// 
// Local Variables:
//  mode: C++
// End:
//
