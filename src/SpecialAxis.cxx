/* 
 * Spherical Harmonics analysis of `maps'
 * Copyright (C) 2012 Christian Holm Christensen
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Concept derived from Gaus-LEgendre Sky Pixelization (GLESP). 
 * Home-page: http://www.glesp.nbi.dk/
 * Reference: Int.Jour.M.Phys.D14, 275 (astro-ph/0305537) 
 *    http://www.worldscinet.com/ijmpd/14/1402/S0218271805006183.html
 *
 */
/**
 * @file   SpecialAxis.cxx
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu May  3 12:54:15 2012
 * 
 * @brief  Implementation of quasi-equal area @f$\phi@f$ axis 
 */
#include "SpecialAxis.h"
#include <iostream>
#include <iomanip>

//____________________________________________________________________
SpecialAxis::SpecialAxis(const Axis& other, UShort_t maxN, 
			 Double_t min, Double_t max)
  : Axis(), 
    fNBins("bins", "# of bins", 1, 0, 1),
    fAxis(maxN, min, max)
{
  fAxis.SetTitle("#phi [radians]");
  fNBins.SetXTitle("Bin #");
  fNBins.SetYTitle("# Bins");
  fNBins.SetDirectory(0);
  fNBins.SetFillColor(kRed+1);
  fNBins.SetFillStyle(3001);
  fNBins.SetStats(0);
  
  TAxis*    ax = other.GetAxis();
  Int_t     nx = other.GetNBins();
  Double_t  xl = (ax ? ax->GetXmin()          : .5);
  Double_t  xh = (ax ? ax->GetXmax()          : nx+.5);
  Double_t* xb = (ax ? ax->GetXbins()->fArray : 0);
  if (xb)  fNBins.SetBins(nx, xb);
  else     fNBins.SetBins(nx, xl, xh);
  
  TArrayS bins(nx);
  
  Int_t    i  = (nx + 1) / 2 + 1;// Center
  Int_t    np = maxN; 
  Double_t s0 = 1. / np * TMath::Abs(other.GetBinX(i+1)-other.GetBinX(i-1))/2;
  Double_t x0  = TMath::Cos(other.GetBinX(1));
  // Info("", "s0=%8f x0=%8f i=%6d np=%6d", s0, x0, i, np);
  
  for (Int_t i = 1; i <= nx; i++) { 
    Double_t dth = 0;
    Double_t fac = 1;
    Double_t x   = other.GetBinX(i);
    Double_t xp  = (i == 1  ? 0 : other.GetBinX(i-1));
    Double_t xn  = (i == nx ? 0 : other.GetBinX(i+1));

    if (i == 1) { 
      dth = ((x0 > 0 ? 0 : TMath::Pi()) + (x0 > 0 ? 1 : -1) * (xn + x) / 2);
      fac = .5;
    }
    else if (i == nx) { 
      dth = ((x0 < 0 ? 0 : TMath::Pi()) + (x0 > 0 ? 1 : -1) * (xp + x) / 2);
      fac = .5;
    }
    else 
      dth = (xn - xp) / 2;
    Double_t cx = TMath::Cos(x);
    fNBins.SetBinContent(i, Short_t(fac * TMath::Sqrt(1 - cx * cx) / 
				    s0 * TMath::Abs(dth) + .5));
    //Info("", "%4d: fac=%8f dth=%8f x=%8f n=%d",i,fac,dth,x,fNBins[i-1]);
  }
}

//____________________________________________________________________
Double_t 
SpecialAxis::GetBinX(UShort_t bin, UShort_t other) const
{
  UShort_t n = GetNBins(other);
  Double_t d = (fAxis.GetXmax() - fAxis.GetXmin()) / n;
  return (bin-1+.5) * d;
}

//____________________________________________________________________
Double_t 
SpecialAxis::GetBinWeight(UShort_t bin, UShort_t other) const 
{
  UShort_t n = GetNBins(other);
  Double_t d = (fAxis.GetXmax() - fAxis.GetXmin()) / n;
  return 1. / d;
}

//____________________________________________________________________
UShort_t
SpecialAxis::FindBin(Double_t theta, UShort_t other) const 
{
  UShort_t n = GetNBins(other);
  Double_t d = (fAxis.GetXmax() - fAxis.GetXmin()) / n;
  UShort_t m = Int_t(theta / d + .5);
  return m;
}

//____________________________________________________________________
void
SpecialAxis::Print(Option_t* option) const 
{
  std::cout << "Axis " << fNBins.GetXaxis()->GetTitle() << ":" 
	    << std::endl;
  Int_t n = fNBins.GetXaxis()->GetNbins();
  Int_t m = 10;
  for (Int_t i = 0; i < n; i++) { 
    if ((i % m) == 0) {
      if (i != 0) std::cout << '\n';
      std::cout << std::setw(3) << i+1 << "-" 
		<< std::setw(3) << i+m << ": ";
    }
    std::cout << std::setw(6) << fNBins.GetBinContent(i+1);
  }
  std::cout << std::endl;
}
//
// EOF
//
