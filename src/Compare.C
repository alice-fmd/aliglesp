class HistogramMap;
class SphericalHarmonics;

//____________________________________________________________________
TH2*
CompareHists(const TH2* h1, const TH2* h2)
{
  TH2* d = static_cast<TH2D*>(h2->Clone(Form("diff%s", h2->GetName())));
  d->SetTitle(Form("Differences for %s", h2->GetTitle()));
  d->Reset();
  
  for (Int_t i = 1; i <= d->GetNbinsX(); i++) { 
    for (Int_t j = 1; j <= d->GetNbinsY(); j++) { 
      Double_t denom = h1->GetBinContent(i, j);
      Double_t numer = h2->GetBinContent(i, j);
      if (TMath::Abs(denom) < 1e-12) continue;

      Double_t diff  = (numer - denom) / denom;
      
      if (TMath::Abs(diff) > 1e-6)
	Warning("", "%s(%3d,%3d)=%9.4f vs %s=%9.4f differ by %f",
		h1->GetName(), i, j, denom, h2->GetName(), numer, diff);

      d->SetBinContent(i, j, diff);
    }
  }
  return d;
}

//____________________________________________________________________
void 
CompareMaps(TDirectory* d1, HistogramMap* mm)
{
  TH2* m1 = static_cast<TH2*>(d1->Get("map"));
  TH2* w1 = static_cast<TH2*>(d1->Get("weightedMap"));
  if (!m1 || !w1)  {
    Error("", "Failed to map %p or weighted map %p from %s", m1, w1, 
	  d1->GetName());
    return;
  }
  Info("", "Got maps %p, %p, from %s", m1, w1, d1->GetName());

  TH2*          m2 = mm->GetMap();
  TH2*          w2 = mm->GetWeightedMap();
  if (!m2 || !w2)  {
    Error("", "Failed to map %p or weighted map %p from %s", m2, w2, 
	  mm->GetName());
    return;
  }
  Info("", "Got map histograms %p, %p, from %s", m2, w2, mm->GetName());

  TH2* dm = CompareHists(m1, m2);
  TH2* dw = CompareHists(w1, w2);
  Info("", "Made copies for histogram maps form %s", mm->GetName());

  TCanvas* c = new TCanvas("cMap", "C", 1200, 900);
  c->Divide(2,3);

  c->cd(1);
  m1->Draw("lego2z");
  c->cd(2);
  w1->Draw("colz");


  c->cd(3);
  m2->Draw("lego2z");
  c->cd(4);
  w2->Draw("colz");


  c->cd(5);
  dm->Draw("lego2z");
  c->cd(6);
  dw->Draw("colz");
}

//____________________________________________________________________
void
CompareFFT(TDirectory* d1, SphericalHarmonics* sh)
{
  TH2* r1 = static_cast<TH2*>(d1->Get("reC"));
  TH2* i1 = static_cast<TH2*>(d1->Get("imC"));
  if (!r1 || !i1)  {
    Error("", "Failed to map %p or weighted map %p from %s", r1, i1, 
	  d1->GetName());
    return;
  }
  Info("", "Got maps %p, %p, from %s", r1, i1, d1->GetName());

  TH2* r2 = sh->GetReC();
  TH2* i2 = sh->GetImC();
  if (!r2 || !i2)  {
    Error("", "Failed to map %p or weighted map %p from %s", r2, i2, 
	  sh->GetName());
    return;
  }
  Info("", "Got harmonics FFT %p, %p, from %s", r2, i2, sh->GetName());

  TH2* dr = CompareHists(r1, r2);
  TH2* di = CompareHists(i1, i2);
  Info("", "subtracted maps from %s", d1->GetName());

  TCanvas* c = new TCanvas("cFFT", "C", 1200, 900);
  c->Divide(2,3);

  c->cd(1);
  r1->Draw("colz");
  c->cd(2);
  i1->Draw("colz");


  c->cd(3);
  r2->Draw("colz");
  c->cd(4);
  i2->Draw("colz");

  for (Int_t i = 0; i < dr->GetNbinsX(); i++) { 
    for (Int_t j = 0; j < dr->GetNbinsY(); j++) { 
      if (r1->GetBinContent(i+1, j+1) == 0) { 
	dr->SetBinContent(i+1, j+1, 0);
	di->SetBinContent(i+1, j+1, 0);
      }
    }
  }
  
  c->cd(5);
  dr->Draw("colz");
  c->cd(6);
  di->Draw("colz");
}

//____________________________________________________________________
void
CompareALM(TDirectory* d1, SphericalHarmonics* sh)
{
  TH2* r1 = static_cast<TH2*>(d1->Get("reA"));
  TH2* i1 = static_cast<TH2*>(d1->Get("imA"));
  if (!r1 || !i1)  {
    Error("", "Failed to map %p or weighted map %p from %s", r1, i1, 
	  d1->GetName());
    return;
  }
  Info("", "Got maps %p, %p, from %s", r1, i1, d1->GetName());

  TH2* r2 = sh->GetReA();
  TH2* i2 = sh->GetImA();
  if (!r2 || !i2)  {
    Error("", "Failed to map %p or weighted map %p from %s", r2, i2, 
	  sh->GetName());
    return;
  }
  Info("", "Got harmonics ALM %p, %p, from %s", r2, i2, sh->GetName());

  TH2* dr = CompareHists(r1, r2);
  TH2* di = CompareHists(i1, i2);
  Info("", "subtracted maps from %s", d1->GetName());

  TCanvas* c = new TCanvas("cALM", "C", 1200, 900);
  c->Divide(2,3);

  c->cd(1);
  r1->Draw("colz");
  c->cd(2);
  i1->Draw("colz");


  c->cd(3);
  r2->Draw("colz");
  c->cd(4);
  i2->Draw("colz");

  for (Int_t i = 0; i < dr->GetNbinsX(); i++) { 
    for (Int_t j = 0; j < dr->GetNbinsY(); j++) { 
      if (r1->GetBinContent(i+1, j+1) == 0) { 
	dr->SetBinContent(i+1, j+1, 0);
	di->SetBinContent(i+1, j+1, 0);
      }
    }
  }
  
  c->cd(5);
  dr->Draw("colz");
  c->cd(6);
  di->Draw("colz");
}



//____________________________________________________________________
void
Compare(const char* s1="../data/sim-glesp.root", 
	const char* s2="sim-map.root",
	const char* s3="sim-sph.root")
{
  gSystem->Load("libAliGlesp.so");
  
  TFile* f1 = TFile::Open(s1, "READ");
  TFile* f2 = TFile::Open(s2, "READ");
  TFile* f3 = TFile::Open(s3, "READ");
  if (!f1 || !f2 || !f3) { 
    Error("", "Failed to open %s (%p) or %s (%p) %s (%p)", 
	  s1, f1, s2, f2, s3, f3);
    return;
  }
  Info("", "Got files %s, %s, and %s", s1, s2, s3);


  Int_t ev = 1;
  HistogramMap* mm = static_cast<HistogramMap*>(f2->Get(Form("map%08d", ev)));
  if (!mm) { 
    Error("", "Failed to get map%08d from %s", ev, s2);
    f2->ls();
    return;
  }
  Info("", "Got map %p from %s", mm, s2);

  SphericalHarmonics* sh = 
    static_cast<SphericalHarmonics*>(f3->Get(Form("sph%08d", ev)));
  if (!sh) { 
    Error("", "Failed to get sph%08d from %s", ev, s3);
    f3->ls();
    return;
  }
  Info("", "Got harmonics %p from %s", sh, s3);

  CompareMaps(f1, mm);
  CompareFFT(f1, sh);
  CompareALM(f1, sh);
}
//
// EOF
//

  
