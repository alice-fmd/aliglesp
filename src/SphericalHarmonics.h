/* 
 * Spherical Harmonics analysis of `maps'
 * Copyright (C) 2012 Christian Holm Christensen
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Concept derived from Gaus-LEgendre Sky Pixelization (GLESP). 
 * Home-page: http://www.glesp.nbi.dk/
 * Reference: Int.Jour.M.Phys.D14, 275 (astro-ph/0305537) 
 *    http://www.worldscinet.com/ijmpd/14/1402/S0218271805006183.html
 *
 */
/**
 * @file   SphericalHarmonics.h
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu May  3 12:55:29 2012
 * 
 * @brief  Declaration of class to do spherical harmonics analysis
 */
#ifndef ALIGLESP_SPHERICALHARMONICS
#define ALIGLESP_SPHERICALHARMONICS
#include <TNamed.h>
#include <Map.h>
#include <TObjArray.h>
class TH1D;
class TH2D;

/**
 * @class SphericalHarmonics 
 *
 * Class that calculates and holds the spherical harmonics of a map.
 *
 * @example Draw.C 
 */
class SphericalHarmonics : public TNamed
{
public:
  /** 
   * Constructor - for ROOT I/O only
   */
  SphericalHarmonics();
  /** 
   * User constructor 
   * 
   * @param name Name of this object 
   */
  SphericalHarmonics(const char* name);
  /** 
   * Destructor 
   */
  virtual ~SphericalHarmonics();

  /** 
   * @{ 
   * @name Calculations 
   */
  /** 
   * Process a map.  
   * 
   * @param map   Map to process
   * @param ulmax If larger than 0, the maximum @f$ l@f$ value used if
   * not larger than the maximum @f$ l@f$ allowed by the map.  If 0 or
   * less, the use the maximum @f$ l@f$ allowed by the map. 
   */
  void ProcessMap(const Map& map, Short_t ulmax=-1);
  /* @} */

  /** 
   * @{ 
   * @name Histogram access 
   */
  /** 
   * Get the real part of the Fourier coefficients @f$ c_{ij}@f$ of the map 
   * 
   * @return @f$ R(\mathbf{C})@f$ 
   */  
  TH2D* GetReC() const { return fReC; }
  /** 
   * Get the imaginary part of the Fourier coefficients @f$ c_{ij}@f$
   * of the map
   * 
   * @return @f$ I(\mathbf{C})@f$ 
   */  
  TH2D* GetImC() const { return fImC; }
  /** 
   * Get the real part of the spherical harmonic coefficents @f$
   * a_{lm}@f$
   * 
   * @return @f$ R(\mathbf{A}@f$
   */  
  TH2D* GetReA() const { return fReA; }
  /** 
   * Get the imaginary part of the spherical harmonic coefficents @f$
   * a_{lm}@f$
   * 
   * @return @f$ I(\mathbf{A}@f$
   */  
  TH2D* GetImA() const { return fImA; }
  /** 
   * Get the power spectrum 
   * 
   * @return The power spectrum @f$ C(l)@f$ 
   */
  TH1D* GetCl() const { return fCl; }
  /** 
   * Get the reduced power spectrum 
   * 
   * @return The reduced power spectrum @f$ D(l)@f$ 
   */
  TH1D* GetDl() const { return fDl; }
  /* @} */
  
  /** 
   * @{ 
   * @name a_lm access
   */
  /** 
   * Get @f$ l_{max}@f$ 
   * 
   * @return @f$ l_{max}@f$ 
   */
  UShort_t GetLmax() const { return fLmax; }
  /** 
   * Get @f$\Re(a_{lm})@f$
   * 
   * @param l @f$ l\in[0,l_{max}]@f$
   * @param m @f$ m\in[0,l]@f$ 
   * 
   * @return @f$\Re(a_{lm})@f$
   */
  Double_t GetReAlm(UShort_t l, UShort_t m) const;
  /** 
   * Get @f$\Im(a_{lm})@f$
   * 
   * @param l @f$ l\in[0,l_{max}]@f$
   * @param m @f$ m\in[0,l]@f$ 
   * 
   * @return @f$\Im(a_{lm})@f$
   */
  Double_t GetImAlm(UShort_t l, UShort_t m) const;
  /** 
   * Get @f$|a_{lm}|@f$
   * 
   * @param l @f$ l\in[0,l_{max}]@f$
   * @param m @f$ m\in[0,l]@f$ 
   * 
   * @return @f$|a_{lm}|=\sqrt{\Re(a_{lm})^2+\Im(a_{lm})^2}@f$
   */
  Double_t GetAlmMag(UShort_t l, UShort_t m) const;
  /** 
   * Get @f$a_{lm}^2@f$
   * 
   * @param l @f$ l\in[0,l_{max}]@f$
   * @param m @f$ m\in[0,l]@f$ 
   * 
   * @return @f$|a_{lm}|=\Re(a_{lm})^2+\Im(a_{lm})^2@f$
   */
  Double_t GetAlmMag2(UShort_t l, UShort_t m) const;
  /** 
   * Get @f$\mbox{Arg}(a_{lm})@f$
   * 
   * @param l @f$ l\in[0,l_{max}]@f$
   * @param m @f$ m\in[0,l]@f$ 
   * 
   * @return @f$\mbox{Arg}(a_{lm})=\tan^{-1}(\Im(a_{lm})/\Re(a_{lm}))@f$
   */
  Double_t GetAlmPhase(UShort_t l, UShort_t m) const;
  /** @} */

  /** 
   * @{ 
   * @name c_xm access
   */
  /** 
   * Get @f$\Re(c_{m}(x))@f$
   * 
   * @param ix X axis bin number @f$[1,n_x]@f$
   * @param m @f$ m\in[0,l_{max}]@f$ 
   * 
   * @return @f$\Re(c_{m}(x))@f$
   */
  Double_t GetReCxm(UShort_t ix, UShort_t m) const;
  /** 
   * Get @f$\Im(c_{m}(x))@f$
   * 
   * @param ix X axis bin number @f$[1,n_x]@f$
   * @param m @f$ m\in[0,l_{max}]@f$ 
   * 
   * @return @f$\Im(c_{m}(x))@f$
   */
  Double_t GetImCxm(UShort_t ix, UShort_t m) const;
  /** 
   * Get @f$|c_{m}(x)|@f$
   * 
   * @param ix X axis bin number @f$[1,n_x]@f$
   * @param m @f$ m\in[0,l_{max}]@f$ 
   * 
   * @return @f$|c_{m}(x)|=\sqrt{\Re(c_{m}(x))^2+\Im(c_{m}(x))^2}@f$
   */
  Double_t GetCxmMag(UShort_t ix, UShort_t m) const;
  /** 
   * Get @f$c_{m}(x)^2@f$
   * 
   * @param ix X axis bin number @f$[1,n_x]@f$
   * @param m @f$ m\in[0,l_{max}]@f$ 
   * 
   * @return @f$|c_{m}(x)|=\Re(c_{m}(x))^2+\Im(c_{m}(x))^2@f$
   */
  Double_t GetCxmMag2(UShort_t ix, UShort_t m) const;
  /** 
   * Get @f$\mbox{Arg}(c_{m}(x))@f$
   * 
   * @param ix X axis bin number @f$[1,n_x]@f$
   * @param m @f$ m\in[0,l_{max}]@f$ 
   * 
   * @return @f$\mbox{Arg}(c_{m}(x))=\tan^{-1}(\Im(c_{m}(x))/\Re(c_{m}(x)))@f$
   */
  Double_t GetCxmPhase(UShort_t ix, UShort_t m) const;
  /** @} */

  /** 
   * @{
   * @name ROOT utilities 
   */
  Bool_t IsFolder() const { return true; }
  /** 
   * Draw this
   * 
   * @param option Drawing options. 
   */
  void Draw(Option_t* option); //*MENU*
  /* @} */
private:
  SphericalHarmonics(const SphericalHarmonics&) 
  : fLmax(0),
    fReC(0), 
    fImC(0), 
    fReA(0), 
    fImA(0), 
    fCl(0), 
    fDl(0)
  {}
  SphericalHarmonics& operator=(const SphericalHarmonics&) { return *this; }
  /** 
   * Initialize histograms and get the maximum @f$ l@f$ value 
   * 
   * @param map Map to initialise from 
   */
  void Initialize(const Map& map);
  /** 
   * Clean up histograms 
   */
  void Clean();
  /** 
   * Calculate the monopole 
   * 
   * @param map Map to get the monopole from 
   * 
   * @return Monopole value 
   */
  Double_t CalculateMonopole(const Map& map) const;
  /** 
   * Do the fourier transform.  That is, for each @f$\theta@f$ slice,
   * determine the complex coefficients @f$ c_m@f$ such that 
   *
   * @f[
   *   f(\phi) = \sum_{m=0}^{N} |c_m| e^{-\frac{i2\pi\phi}{N}} 
   * @f] 
   *
   * where @f$N=\max(n_\phi,l_{max})@f$ and @f$n_\phi@f$ is the
   * number of segments (bins) along @f$\phi@f$ for the @f$\theta@f$
   * bin in question.
   * 
   * @param map  The map to transform
   */
  void Transform(const Map& map);
  /** 
   * Calculate all poles in one go. 
   * 
   * This loops over l from 0 to lmax.  For each l, it loops over m
   * from 0 up to l.  For each m, it loops over the x axis and
   * calculates the (l,m) Spherical Harmonic at that point and add
   * that contribution to the pole at (l,m)
   *
   * That is, 
   *
   * @f[ 
   *  a_{lm} = \sum_{i=1}^{n_x} \sum_{m=0}^{l} c_m \bar{Y}_{m}^{l}(\theta)
   * @f] 
   * 
   * @param map           Map to process
   * @param eps           Precision parameter @f$\epsilon@f$ 
   */
  void CalculateAllPoles(const Map& map, 
			 Double_t   eps=1e-30);
  /** 
   * Calculate power spectrum 
   */
  void CalculateCl();

  UShort_t fLmax;      // The absolute largest L to calculate 
  TH2D*    fReC;       // Histogram of the Fourier coefficents
  TH2D*    fImC;       // Histogram of the Fourier coefficents
  TH2D*    fReA;       // Histogram of the spherical harmonics
  TH2D*    fImA;       // Histogram of the spherical harmonics
  TH1D*    fCl;        // Power spectrum 
  TH1D*    fDl;        // Reduced power spectrum 

  ClassDef(SphericalHarmonics,1);
};


#endif
/*
 * Local Variables: 
 *  mode: C++ 
 * End:
 */


  
