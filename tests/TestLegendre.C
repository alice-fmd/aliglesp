void
TestLegendre(Int_t ndiv=100)
{
  gSystem->Load("libHist.so");
  gSystem->Load("/usr/lib/libfftw3.so");
  gSystem->Load("libFFTW.so");
  gSystem->Load("libminiglesp.so");

  UShort_t  lmax = 50;
  TArrayD   gc(2*lmax+2);
  TArrayD   ac(2*lmax+2);
  Double_t  gcmm = 0;
  Double_t  acmm = 0;
  Double_t  tol  = 1e-10;
  TArrayD   gglm(lmax+2);
  TArrayD   aglm(lmax+2);

  for (UShort_t m=2; m <= lmax; m++) {
    Info("", "m=%3d", m);
    gc.Reset(0);
    ac.Reset(0);
    start_clm(lmax, m, &gcmm, gc.fArray);
    acmm = AliGlesp::PrepareLegendre(lmax, m, ac.fArray);

    for (UShort_t i = 0; i < ndiv; i++) { 
      Double_t xc = -1 * i * 1. / ndiv;
      gglm.Reset(0);
      aglm.Reset(0);

      lgndr_m(lmax, m, gcmm, gc.fArray, xc, gglm.fArray);
      AliGlesp::EvaluateLegendre(lmax, m, acmm, ac.fArray, xc, aglm.fArray);

      Info("", " xc=%8f gglm[3]=%12g aglm[3]=%12g", 
	   xc, gglm[3], aglm[3]);
      for (UShort_t j = 0; j < lmax+1; j++) { 
	Double_t diff = gglm[j] - aglm[j];
	if (TMath::Abs(diff) > tol) 
	  Info("", "gglm[%3d]=%8g aglm[%3d]=%8g -> |%12g| > %g", 
	       l, gglm[l], l, aglm[l], diff, tol);
      }
    }
  }
}

