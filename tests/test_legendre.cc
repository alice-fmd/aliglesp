#include "miniglesp.h"
#include "AliGlesp.h"
#include <Math/SpecFuncMathMore.h>
#include <TMath.h>
#include <TMultiGraph.h>
#include <TGraph.h>
#include <TObjArray.h>
#include <TApplication.h>
#include <TLine.h>
#include <TCanvas.h>

#define LM2INDEX(L, M)  ((L)*(lmax+1)+(M))

int main()
{
  TApplication app("app", 0, 0);

  UShort_t lmax = 20;
  
  TArrayD glm(lmax+2);
  TArrayD rlm(lmax+1);
  TArrayD c(2*lmax+1);

  TMultiGraph* mg  = new TMultiGraph;
  TMultiGraph* frg = new TMultiGraph;
  TObjArray*   rgs = new TObjArray((lmax+1)*(lmax+1));
  TObjArray*   ggs = new TObjArray((lmax+1)*(lmax+1));
  TObjArray*   fgs = new TObjArray((lmax+1)*(lmax+1));
  
  for (UShort_t m = 0; m <= lmax; m++) {
    
    Double_t cmm = AliGlesp::PrepareLegendre(lmax, m, c.fArray);
    printf("cmm=%12g\n", cmm);

    for (UShort_t l = 0; l <= lmax; l++) { 
      if (m > l) continue;

      TGraph* rg = new TGraph;
      rg->SetLineStyle(1);
      rg->SetLineWidth(1);
      rg->SetLineColor(l+2);
      rg->SetFillStyle(0);
      rg->SetFillColor(0);
      rg->SetName(Form("r_%02d_%02d", l, m));
      rg->SetTitle(Form("ROOT l=%02d, m=%02d", l, m));
      rgs->AddAtAndExpand(rg, LM2INDEX(l,m));
      mg->Add(rg);

      if (l < 1) continue;

      TGraph* gg = new TGraph;
      gg->SetLineStyle(2);
      gg->SetLineWidth(2);
      gg->SetLineColor(l+2);
      gg->SetFillStyle(0);
      gg->SetFillColor(0);
      gg->SetName(Form("g_%02d_%02d", l, m));
      gg->SetTitle(Form("Glesp l=%02d, m=%02d", l, m));
      ggs->AddAtAndExpand(gg, LM2INDEX(l,m));
      mg->Add(gg);

      TGraph* fg = new TGraph;
      fg->SetLineStyle(1);
      fg->SetLineWidth(2);
      fg->SetLineColor(l+2);
      fg->SetFillStyle(0);
      fg->SetFillColor(0);
      fg->SetName(Form("f_%02d_%02d", l, m));
      fg->SetTitle(Form("l=%02d, m=%02d", l, m));
      frg->Add(fg);
      fgs->AddAtAndExpand(fg, LM2INDEX(l,m));

    }
    
    Int_t i = 0;
    for (Double_t x=-1; x <= 1; x += .01, i++) {
      AliGlesp::EvaluateLegendre(lmax, m, cmm, c.fArray, x, glm.fArray);

      for (UShort_t l = 0; l <= lmax; l++) {
	if (m > l) continue;

	TGraph* rg = static_cast<TGraph*>(rgs->At(LM2INDEX(l,m)));
	
	
	rlm[l] = ROOT::Math::sph_legendre(l, m, TMath::ACos(x));

	rg->SetPoint(i, x, rlm[l] + 2*l+1);

	if (l < 1) continue;
	TGraph* gg = static_cast<TGraph*>(ggs->At(LM2INDEX(l,m)));
	gg->SetPoint(i, x, glm[l+1] + 2*l+1);


	TGraph* fg = static_cast<TGraph*>(fgs->At(LM2INDEX(l,m)));
	Double_t f = 0;
	if      (TMath::Abs(rlm[l] - glm[l+1]) < 1e-8) f = 1;
	else if (TMath::Abs(rlm[l]) < 1e-8)            f = 1;
	else                                           f = glm[l+1] / rlm[l];

	fg->SetPoint(i, x, f + 2*(l-1));
	printf("f==%12g\n", f);
#if 0
	Double_t diff = TMath::Abs(glm[l] / rlm[l]);
	printf("l=%3d m=%3d  glesp=%12f root=%12f  diff=%12f %c\n", 
	       l, m, glm[l+1], rlm[l], diff, 
	       TMath::Abs(diff-1) > 10e-6 ? '*' : ' ');
#endif	

      }
    }
  }

  TCanvas* c1 = new TCanvas("C1", "C1");
  c1->cd();
  mg->Draw("al");

  for (UShort_t l = 0; l <= lmax; l++) { 
    TLine* ll = new TLine(-1.1, 2*l+1, 1.1, 2*l+1);
    ll->SetLineStyle(3);
    ll->Draw();
  }
  c1->cd();

  TCanvas* c2 = new TCanvas("C2", "C2");
  c2->cd();
  frg->Draw("al");
  for (UShort_t l = 0; l <= lmax; l++) { 
    TLine* ll = new TLine(-1.1, 2*l+1, 1.1, 2*l+1);
    ll->SetLineStyle(3);
    ll->Draw();
  }

  app.Run();
  

  return 0;
}

      
				       
	
    
