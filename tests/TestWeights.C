void
TestWeights()
{
  gSystem->Load("libHist.so");
  gSystem->Load("/usr/lib/libfftw3.so");
  gSystem->Load("libFFTW.so");
  gSystem->Load("libminiglesp.so");

  Int_t     n = 200;
  Double_t* x = new Double_t[n];
  Double_t* w = new Double_t[n];
  Int_t*    m = new Int_t[n];
  for (UShort_t i = 0; i < n; i++) m[i] = n;
  weights(-1., 1., x, w,  n, m);

  TArrayD abs;
  TArrayD wei;
  
  AliGlesp::GaussLegendreIntegral(abs, wei, n);

  Double_t dPhi = 2 * TMath::Pi() / n;
  for (UShort_t i = 0; i < abs.GetSize(); i++) { 
    Double_t ww = wei.At(i); 
    wei.SetAt(ww*dPhi, i);
    Printf("%3d  %12lf %12lf %c   %12lf %12lf %c", 
	   i, 
	   x[i], abs.At(i), (TMath::Abs(x[i]-abs.At(i)) > 1e-15 ? '*' : ' '),
	   w[i], wei.At(i), (TMath::Abs(w[i]-wei.At(i)) > 1e-15 ? '*' : ' '));
  }
}

