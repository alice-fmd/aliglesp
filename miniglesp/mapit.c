#include "miniglesp.h"
#include <stdlib.h>

/** 
 * Maps data in inp_file to a GLESP map stored in out_map_file 
 * 
 * @param inp_file     Input file
 * @param out_map_file Output file 
 * @param nx           Number of eta bins
 * @param np           Number of phi bins
 * @param event        Event number (-1 for all)
 * @param type         Particle types (-1 for all, 1 for primaries)
 * @param resolution   Whether to take full resolution only 
 * 
 * @return 0 on success, 1 otherwise 
 */
int mapit(const char* inp_file, const char* out_map_file, 
	  unsigned int nx, unsigned int np, int event, int type, 
	  int resolution)
{
  FILE*   fp_out = 0;
  double* f      = 0;
  double* x      = 0;
  int*    nph    = 0;
  double* pw     = 0;

  /* find memory for arrays */
  if ((x = (double*)calloc(nx, sizeof(double))) == 0) {
    fprintf(stderr, "Failed to allocate array x\n");
    return 1;
  }
  if ((nph = (int*)calloc(nx, sizeof(int))) == 0) {
    fprintf(stderr, "Failed to allocate array nph\n");
    return 1;
  }
  if ((f=(double*)calloc((nx*np), sizeof(double))) == 0) {
    fprintf(stderr, "Failed to allocate array f\n");
    return 1;
  }
  if ((pw = (double*)calloc(nx, sizeof(double))) == 0) {
    fprintf(stderr, "Failed to allocate array pw\n");
    return 1;
  }
  if ((fp_out = fopen(out_map_file, "w")) == 0) {
    fprintf(stderr, "Failed to open output file %s", out_map_file);
    return 1;
  }

  /* calculation of map pattern */
  coord(-1, 1, x, pw, nx, nph, np);
  if (map_alisa(f, x, nx, np, nph, inp_file, event, type, resolution))
    return 1;
    

  /* map output in FITS-format */
  outp_glesp_map(f, x, nx, np, nph, fp_out);
    
  /* close files */
  if (fp_out != 0 && fp_out != stdout) fclose(fp_out);

  /* free memory */
  if (f)   free(f);
  if (x)   free(x);
  if (nph) free(nph);
  if (pw)  free(pw);
  
  return 0;
}
// 
// EOF
//

