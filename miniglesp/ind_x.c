#include "miniglesp.h"

/** 
 * Look-up x_in in the x array of size n 
 * 
 * @param n 
 * @param x 
 * @param x_in 
 * 
 * @return 
 */
int ind_x(int n, double* x, double x_in)
{
  int i = x_locate(n, x, x_in);
  if (i == -1)  i = 0; // Put in lowest bin
  /* thanks to Martin Reinecke for finding bug in 'i=n-1' */
  else  if (i == n || i==n-1)  i = n-1; // put in top bin 
  else  {
    // Closet edge 
    i = (fabs(x_in-x[i]) < fabs(x[i+1]-x_in) ? i : i+1);
  }
  return i;
}
//
// EOF
//

