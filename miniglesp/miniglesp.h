#ifndef LIB_LIB_H
#define LIB_LIB_H

#include <math.h>
#include <stdio.h>
#include <float.h>

#ifndef __CINT__
#define ABS(x)  ((x) < 0 ? -(x) : (x))
#define EPS     (3e-15)
#define TWOPI   (2*M_PI)
#define RAD2DEG (180. / M_PI)
#define BLOCK_SIZE      2880
#define LMIN_GL     ((int)(log10(DBL_MAX))-8)
#define ENORM_GL    (pow(10., LMIN_GL))
#define VNYQ        .5 // Nyquist-factor for max m in respect with Nph(ix)
#endif

#ifdef __cplusplus
extern "C" {
#endif
  // --- Coordinates ---------------------------------------------------
  /** 
   * Fill arrays with coordinates 
   * 
   * @param x1    Lower bound on X
   * @param x2    Upper bound on X 
   * @param x     X array
   * @param pw    Array of weights 
   * @param Nx    Number of X bins 
   * @param Nph   Phi bins 
   * @param Nphi  Number of phi bins 
   */
  extern void coord(double  x1, 
		    double  x2, 
		    double* x, 
		    double* pw, 
		    int     Nx, 
		    int*    Nph, 
		    int     Nphi);

  /** 
   * Calculate special grid for Legendre Polynomials 
   * 
   * @param x1  Lower bound
   * @param x2  Upper bound 
   * @param x   Array to be filled 
   * @param w   Array of weights (optional)
   * @param n   Number of bins 
   */
  extern void gauleg(double x1, double x2, double* x, double* w, int n);

  /**
   * Given an array x[0,...,n-1], returns a value i such that x_in is
   * between x[i] and x[i+1]. x must be monotonic, either increasing or
   * decreasing. i=-1 or i=n-1 is returned to indicate that x is out of
   * range (Press et al., 2002)
   *
   * @param n    Size of X
   * @param x    Array to search 
   * @param x_in X value to find 
   *
   * @return Index i such that x[i] <= x < x[i+1] 
   */
  extern int x_locate(int n, double* x, double x_in);

  /** 
   * Look-up x_in in the x array of size n 
   * 
   * @param n 
   * @param x 
   * @param x_in 
   * 
   * @return 
   */
  extern int ind_x(int n, double* x, double x_in);

  /** 
   * Calculate pixel (i_x,i_phi) from (theta,phi) for 2D array. 
   * 
   * @param x 
   * @param nx 
   * @param nph 
   * @param i_x 
   * @param j_phi 
   * @param theta 
   * @param phi 
   *
   * @note This is redundant if using a histogram 
   */
  extern void angle2pixel(double* x, int nx, int* nph, int* i_x, int* j_phi, 
			  double theta, double phi);

  /** 
   * Calculate the index corresponding to (theta,phi) into the 2D array
   * of size (nx x nphi). 
   * 
   * @param theta Theta value (radians) 
   * @param phi   Phi value   (radians)
   * @param x     X axis 
   * @param nx    Number of bins along X 
   * @param nphi  Number of bins along phi 
   * @param nph   Phi axis
   * 
   * @return Index or negative value on error 
   *
   * @note This would be replaced by the TH2::FindBin(cos(theta), phi)
   */
  extern int ang2n(double theta, double phi, double* x, int nx, int nphi, 
		   int* nph);

  // --- ALICE ---------------------------------------------------------
  /** 
   * Read in data from a file 
   * 
   * @param f             Array to fill
   * @param x             X axis 
   * @param nx            Number of X bins 
   * @param np            Number of phi bins
   * @param nph           Phi bins
   * @param inp_file      Input file 
   * @param event_num     Which event to read 
   * @param event_type    0: all, 1 primary 
   * @param event_inf     Assume infinite resolution 
   */
  int map_alisa(double*     f, 
		double*     x, 
		int         nx, 
		int         np, 
		int*        nph, 
		const char* inp_file, 
		int         event_num, 
		int         event_type, 
		int         resolution);

  /** 
   * Write out map to a FITS formatted file 
   * 
   * @param f           Map to write
   * @param x           X axis
   * @param nx          N bins along X
   * @param np          Number of phi bins
   * @param nph         Phi bins
   * @param fp_out      Output file pointer
   */
  void outp_glesp_map(double* f, double* x, int nx, int np, int* nph, 
		      FILE* fp_out);

  /** 
   * Maps data in inp_file to a GLESP map stored in out_map_file 
   * 
   * @param inp_file     Input file
   * @param out_map_file Output file 
   * @param nx           Number of eta bins
   * @param np           Number of phi bins
   * @param event        Event number (-1 for all)
   * @param type         Particle types (-1 for all, 1 for primaries)
   * @param resolution   Whether to take full resolution only 
   * 
   * @return 0 on success, 1 otherwise 
   */
  int mapit(const char*  inp_file, 
	    const char*  out_map_file, 
	    unsigned int nx, 
	    unsigned int np, 
	    int          event, 
	    int          type, 
	    int          resolution);
  

  /** 
   * Calculate weights of Gauss-Legendre curvature.  The weights are
   * weighted by the bin size in phi
   * 
   * @param x1 
   * @param x2 
   * @param x 
   * @param pw 
   * @param Nx 
   * @param Nph 
   */
  void weights(double x1, double x2, double* x, double* pw, int Nx, int* Nph);

  /** 
   * 
   * 
   * @param f        2-dim map                          
   * @param x 	   argument, x = cos theta            
   * @param nx 	   number of elements in x-array      
   * @param np 	   max number of elements by phi-axis 
   * @param nph 	   number of elements in phi-array    
   * @param filename File to read 
   */
  int in_glesp_map(double** f, double** x, int* nx, int* np, 
		   int** nph, const char* filename);

  void 
  out_glesp_alm(double* alm, int lmax, int nbuf, int lmax_print, FILE* fp_out);


  void print_cl_table (FILE* fp_out, double* cl, double* clp, 
		       int lmin, int lmax);


  /** 
   * calculate C_l by a_lm 
   * 
   * @param cl   output C_l array                          
   * @param lmax input a_lm array - complex (nalm/2, nalm) 
   * @param alm  maximum value of l                        
   * @param nalm dimension of a_lm array (Nmx, nalm=2*Nmx) 
   */
  void alm2cl(double* cl, int lmax, double* alm, int nalm);

  /** 
   * calculate D_l by a_lm 
   * 
   * @param dl   output C_l array                          
   * @param lmax input a_lm array - complex (nalm/2, nalm) 
   * @param alm  maximum value of l                        
   * @param nalm dimension of a_lm array (Nmx, nalm=2*Nmx) 
   */
  void alm2dl(double* dl, int lmax, double* alm, int nalm);

  /** 
   * Return 
   * @f[ 
   *   \log_{10}(X)-8
   * @f]
   *
   * where @f$ X@f$ is the maximum value of a double 
   * 
   * @return 
   */
  int max_double();

  /**
   *   loop over l - SUM_l[a_{lm}P_l^m(x_p)]
   *   input: x(Nx),Nph(Jmx),apm(0:Jmx,Nmx),bpm(0:Jmx,Nmx)
   *   ouput: f(Nmx,Nmp)
   */
  void alm_dipole(int Nmx, int Nmp, int Lmx, int Nx,
		  double* x, double* apm, double* bpm, double* f);

  /**
   *    dimension c(2*Lmx+1)
   *    preparation of coefficients for recurrent
   *    relation for P_l^m
   */
  void start_clm(int Lmx, int m, double* cmm, double* c);

  /** 
   * Legandre functions F^m_k(x), k=m,m+1,...,Kmx
   * 
   * @param Lmx 
   * @param m 
   * @param cmm 
   * @param c 
   * @param x 
   * @param glm 
   */
  void lgndr_m(int Lmx, int m, double cmm, double* c, double x, double* glm);


  /**
   *   loop over l - SUM_l[a_{lm}P_l^m(x_p)]
   *   input: x(Nx),Nph(Jmx),apm(0:Jmx,Nmx),bpm(0:Jmx,Nmx)
   *   ouput: f(Nmx,Nmp)
   *
   *     DIMENSION f(Nmx*Nmp),x(Nx),Nph(Nmx)
   *     DIMENSION apm((Jmx+1)*Nmx),bpm((Jmx+1)*Nmx)
   *     DIMENSION c(2*Lmx+2),glm(Lmx+1)
   *
   */
  int alm_lgndr_m(int Nmx, int Nmp, int* Nph, 
		  int Lmn, int Lmx, int Mmn, int Mmx, 
		  double eps,
		  int Nx, 
		  double* x, double* apm, double* bpm, double* glm,
		  double* c, double* f);

  /** 
   * Calculate fourier coefficents 
   * 
   * @param Lmn 
   * @param Lmx 
   * @param Mmn 
   * @param Mmx 
   * @param Nx 
   * @param Vnyq 
   * @param eps 
   * @param Nmx 
   * @param Nmp 
   * @param x 
   * @param pw 
   * @param f 
   * @param Nph 
   * @param wsave 
   * @param cf 
   * @param apm 
   * @param bpm 
   */
  void a_lm(int     Lmn,
	    int     Lmx,
	    int     Mmn,
	    int     Mmx,
	    int     Nx,
	    double  eps,
	    int     Nmx,
	    int     Nmp, 
	    double* x,
	    double* pw,
	    double* f,
	    int*    Nph,
	    double* wsave,
	    double* cf,
	    double* apm,
	    double* bpm);

  void fftw_b(int n, double* f_in, double* f_out);

  void alm_fast3(int Nmx, int Nmp, int Nx, int Lmx,
		 int* Nph, double* f, double* c, double* apm, double* bpm,
		 double* wsave);

  int spectra(const char* input, const char* alm_out_file, 
	      const char* outp_file, int l_max, double eps);

  void in_glesp_alm(double* alm, int* lmax, int nbuf, char* filename);
  int  in_my_alm(double** ralm, double** ialm, int* lmax, char* filename);
#ifdef __cplusplus
}
#endif
#endif
// 
// EOF
//

