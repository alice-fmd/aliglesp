/*
 * cl2map - map generation and spectral calculation for CMB,
 *          basic procedure for TAC4CMB package
 *
 * uses Andrej G. Doroshkevich's FORTAN subroutines signal.f and alm.f
 *     form alm->map and map->alm conversions
 *
 *
 * Oleg V. Verkhodanov, (TAC; SAO) Version 0.0 (stopped on 3-Dec-2002)
 *      Version 0.1 (started on 3-Dec-2002)
 *         Variable Nph[i] are set
 *      Version 0.2 (started on 17-Feb-2003) - pixelization selected.
 *         Parallel OMP-procedure started to be included (with V.Stolyarov)
 *      Version 1.0 (started on 3-Sep-2004)
 *      Changes on 21-Sep-2004 at NBI, 19-Oct-2004, SAO
 *                 04/2009 (NBI),  06/2009 (SAO)
 */
#include "miniglesp.h"
#include <stdlib.h>
#include <string.h>

void usage(const char* prog)
{
  printf("Usage: %s [OPTIONS]\n\n"
	 "Options:\n"
	 "\t-h         This help\n"
	 "\t-i FILE    Input file\n"
	 "\t-o FILE    Output file for ALM\n"
	 "\t-c FILE    Output file for CL\n"
	 "\t-e NUMBER  Number of eta bins\n"
	 "\t-l NUMBER  Largest L value\n", prog);
}

#define NBUF 128

int main (int argc, char** argv)
{
  int  i     = 0; 
  int  l_max = (200 - 1) / 2;
  char input[NBUF];
  char alm_out[NBUF];
  char cl_out[NBUF];
  
  strncpy(input, "map.dat", NBUF);
  strncpy(alm_out, "alm.dat", NBUF);
  strncpy(cl_out, "cl.dat", NBUF);
  
  for (i = 1; i < argc; i++) { 
    if (argv[i][0] == '-') { 
      switch (argv[i][1]) { 
      case 'h': usage(argv[0]); return 0; 
      case 'e': l_max = (atoi(argv[++i])-1)/2; break;
      case 'l': l_max = atoi(argv[++i]); break;
      case 'i': strncpy(input, argv[++i], NBUF); break;
      case 'o': strncpy(alm_out, argv[++i], NBUF); break;
      case 'c': strncpy(cl_out, argv[++i], NBUF); break;
      default: 
	fprintf(stderr, "Unknown option %s", argv[i]); return 1;
      }
    }
  }

  return spectra(input, alm_out, cl_out, l_max, 1e-20);
}
//
// EOF
//
