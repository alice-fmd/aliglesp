#include "miniglesp.h"

/** 
 * Fill arrays with coordinates 
 * 
 * @param x1    Lower bound on X
 * @param x2    Upper bound on X 
 * @param x     X array
 * @param pw    Array of weights 
 * @param Nx    Number of X bins 
 * @param Nph   Phi bins 
 * @param Nphi  Number of phi bins 
 */
void coord(double  x1, 
	   double  x2, 
	   double* x, 
	   double* pw, 
	   int     Nx, 
	   int*    Nph, 
	   int     Nphi)
{
  register int i;

  gauleg(x1, x2, x, pw, Nx);
  if(x[Nx-1] > 1.0)   x[Nx-1] = 1.0;

  /*
   *   arbitrary grid for sphere and fast-fourier
   */
  for (i = 0; i < Nx; i++) Nph[i] = Nphi;

  return;
}
//
// EOF
//
