#include "miniglesp.h"
#include <stdlib.h>

int spectra(const char* map_file, const char* alm_out_file, 
	    const char* outp_file, int l_max, double eps)
{
  int   l_min   = 0;           /* Lmin                                   */
  int   m_min   = 0;           /* Mmin                                   */
  int   m_max   = l_max;       /* Mmax                                   */

  double* x  = 0; /* array of arguments x=cos theta, [-1;1] */
  double* pw = 0;

  /*---------------------------------------------------------------*/
  /* in FORTRAN f(NMX,2*NMX): f(l,m), l=1,lmax, m=0,l, + real,imag */
  /* or map: f(x,phi), x=cos theta, x=-1,1, phi=0,2pi,             */
  /*---------------------------------------------------------------*/
  double* f      = 0; /* main array */
  double* wsave  = 0; /* buffer     */
  int*    Nph    = 0; /* array of phi dimensions */
  int     Nmx    = 0; 
  int     Nmphi  = 0; /* Nmp - max number of cells */
  int     Jmx    = 0; /* Jmx - max value of Lmx    */
  int     Nphi   = 0;
  FILE*   fp_out = 0;
  int     Nx;

  register int i,j, ii;
  double* cl    = 0;
  double* cpl   = 0; /* array for Cp_l  */
  double* cf    = 0;
  double* apm   = 0;
  double* bpm   = 0;
  double  x1    = -1.0;
  double  x2    = 1.0;
  double  alm00 = 0.0;

  double* clp   = 0;
  FILE*   fp    = 0;

  /*------------------------------*/
  /* analysis of input parameters */
  /*------------------------------*/
  if ((fp_out = fopen(outp_file, "w")) == (FILE *)0) {
    fprintf(stderr, "can not create file '%s'\n", outp_file);
    return 1;
  }

  /***********************/
  /* read map: f(x, phi) */
  /***********************/
  in_glesp_map(&f, &x, &Nx, &Nphi, &Nph, map_file);
    
  /* catch memory for working arrays */
  if ((pw = (double *)calloc((unsigned)Nx, sizeof *pw)) ==
      (double*)0) { 
    fprintf(stderr, "Failed to allocate memory for pw\n");
    return 0;
  }
    
  /* calculate weights 'pw' */
  weights(x1, x2, x, pw, Nx, Nph);
  Nmx   = Nx;
  Nmphi = Nphi;

  /* control parameters */
  if (Nmx < 2*l_max+1) {
    fprintf(stderr, "your map resolution parameters are (%d,%d).\n"
	    "Thus, the maximum L should be Lmax <= (%d-1)/2=%d.\n"
	    "But you use Lmax=%d... Please, set the proper Lmax.\n", 
	    Nx, Nphi, Nx, (Nx-1)/2, l_max);
    return 2;
  }

  if (Nmphi < 4*l_max+1) {
    fprintf(stderr, "your map resolution parameters are (%d,%d).\n"
	    "Thus, the maximum L should be Lmax <= (%d-1)/2=%d,\n"
	    "and L<=(%d-1)/4=%d by phi-direction.\n"
	    "You use Lmax=%d... "
	    "Please, use Lmax corresponding to rules.\n",
	    Nx, Nphi, Nx, (Nx-1)/2, Nphi,(Nphi-1)/4, l_max);
    return 2;
  }

  // Apply weights 
  for (ii=i=0; i<Nx; i++, ii+=Nmphi) {
    register int n = Nph[i];
    for(j=0; j<n; j++) {
      if (pw[i] > 0) {
	f[ii+j] *= pw[i];
      }
    }
  }
  /* take memory for temporal arrays */
  if ((wsave=(double*)calloc((unsigned)(2*Nmphi+15),sizeof *wsave))
      == (double*)0){
    fprintf(stderr, "Failed to allocate memory for wsave\n");
    return 0;
  }

  if ((cf = (double *)calloc((unsigned)Nmphi, sizeof *cf)) ==
      (double *)0){
    fprintf(stderr, "Failed to allocate memory for cf\n");
    return 0;
  }
  Jmx=(Nmx+1)/2;  /* Jmx - max value of l_max    */
  if ((apm = (double *)calloc((unsigned)((Jmx+1)*Nmx), sizeof *apm))
      == (double *)0){
    fprintf(stderr, "Failed to allocate memory for apm\n");
    return 0;
  }
  if ((bpm = (double *)calloc((unsigned)((Jmx+1)*Nmx), sizeof *bpm))
      == (double *)0){
    fprintf(stderr, "Failed to allocate memory for bpm\n");
    return 0;
  }


  /* calculate a_{lm} and write to f(l, m) */
  // l_max = -l_max;

  /* calc monopole */
  for (i=0; i<Nx; i++) {
    register int ii = Nmphi * i;
    for(j=0; j<Nph[i]; j++)  alm00 += f[ii+j];
  }
  alm00 /= sqrt(4. * M_PI);


  a_lm(l_min,
       l_max,
       m_min,
       m_max,
       Nx,
       eps,
       Nmx,
       Nmphi, 
       x,
       pw,
       f,
       Nph,
       wsave,
       cf,
       apm,
       bpm);
  // l_max = -l_max;
  l_min = 2;
  f[0] = alm00;


  /* array for C_l   */
  if (cl != (double *)0) free((void *)cl);
  if ((cl = (double *)calloc((unsigned)l_max+2, sizeof *cl)) ==
      (double *)0){
    fprintf(stderr, "Failed to allocate memory for \n");
    return 0;
  }

  /************************/
  /*  output loop for alm */
  /************************/
  fp = fopen(alm_out_file, "w");
  if (fp == (FILE *)0) {
    fprintf(stderr,"can not create this file...\n");
  } else {
    out_glesp_alm(f, l_max, Nmphi, l_max, fp);
    fclose (fp);
  }
    
  /* calculate Cl/Dl */
  alm2cl (cl, l_max, f, Nmphi);
  print_cl_table (fp_out, cl, clp, 0, l_max);
  


  if(fp_out != (FILE *)0 && fp_out != stdout) fclose(fp_out);

  if (cl    != (double *)0)  free((void *)cl);
  if (cpl   != (double *)0)  free((void *)cpl);

  return 0;
}
