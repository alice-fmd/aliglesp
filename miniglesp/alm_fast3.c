#include "miniglesp.h"

/*
 *  fast loop over m - SUM_m[a_{lm}e^{i m phi}]
 *      IMPLICIT REAL*8(a-h,o-y)
 *      DIMENSION f(Nmx,Nmp),Nph(Nmx)
 *      DIMENSION c(Nmp),wsave(2*Nmp+15)
 *      DIMENSION apm(0:Jmx,Nmx),bpm(0:Jmx,Nmx)
 *
 *
 *  input: map f(i,j), Nph - number of cells vs. x,
 *          Lmx - upper limit for m
 *  service: c, wsave
 *
 */
void alm_fast3(int Nmx, int Nmp, int Nx, int Lmx, 
	       int* Nph, double* f, double* c, double* apm, double* bpm,
	       double* wsave)
{
  register int ix, iz, ip;
  int Kx = (Nx + 1) / 2;

  for(ix=0; ix < Kx; ix++) {     /* loop over x */
    register int iix  = ix * Nmp;
    register int iiz  = 0;
    register int ix_s = Nx - 1 - ix;
    int nphi_work     = Nph[ix];
    int Mx            = VNYQ * nphi_work + .5;

    for(ip = 0; ip < nphi_work; ip++)  c[ip] = f[iix+ip];

    // First call 
    fftw_b(nphi_work, c, wsave);
    apm[ix] = -c[0];

    if(Mx         > Lmx)        Mx = Lmx;
    if(2 * Mx + 1 > nphi_work)  Mx = (nphi_work - 1) / 2;

    // iz  runs from 1 to Mx and loops over coefficients 
    // iiz Loops from Nmx+ix in Mx steps, with a step size of Nmx. 
    // iiz loops over the row of the output array 
    // iz2 is the pointer into the Half-complex array. 
    for(iz=1, iiz = Nmx + ix; iz <= Mx; iz++, iiz += Nmx) {
      register int iz2 = iz + iz - 1;
      apm[iiz] = -c[iz2];
      bpm[iiz] =  c[iz2+1];
    }

    if (ix_s == ix) continue;

    iix = ix_s * Nmp;
    for(ip = 0; ip < nphi_work; ip++)  c[ip] = f[iix+ip];

    // Second call 
    fftw_b(nphi_work, c, wsave);

    apm[ix_s] = -c[0];
    for(iz = 1, iiz = Nmx + ix_s; iz <= Mx; iz++, iiz += Nmx) {
      register int iz2 = iz + iz - 1;
      apm[iiz] = -c[iz2];
      bpm[iiz] =  c[iz2+1];
    }
  }  /* END loop over ix */

  return;
}
//
// EOF
//
