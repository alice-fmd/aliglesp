#ifndef  FLOATPIX
#define BYTE    8
#define SHORT   16
#define LONG    32
#define FLOAT (-32)
#define DOUBLE  64
#define BYTEPIX   BYTE
#define SHORTPIX  SHORT
#define LONGPIX   LONG
#define FLOATPIX  FLOAT
#define DOUBLEPIX DOUBLE
#endif

#define BOOLEAN         int


#ifdef __GNUC__

extern char   *FEVget(char *);
extern BOOLEAN FEVset(char *, char *);
extern BOOLEAN FEVexport (char *);
extern BOOLEAN FEVinit(void), FEVupdate(void);
extern void    FEVprint(void);
extern int     FHinit(FILE *), FHupdate(FILE *), FHlen(void);
extern void    iFEVset(char *, int), sFEVset(char *, char *),
	       cFEVset(char *, char *), lFEVset(char *, long),
	       fFEVset(char *, float), dFEVset(char *, double);
extern void    FHfree(void), FEVdel(char *), FEV_fatal(char *);
extern void    put_pix_fits(FILE *, double);
extern void    put_dvec_fits(FILE *, double *, int);
extern double  get_pix_fits(FILE *);

#else

extern char   *FEVget();
extern BOOLEAN FEVset();
extern BOOLEAN FEVexport();
extern BOOLEAN FEVinit(), FEVupdate(), FHlen();
extern void    FEVprint();
extern int     FHinit(), FHupdate();
extern void    iFEVset(), sFEVset(), lFEVset(), fFEVset(), dFEVset();
extern void    FHfree(), FEVdel(), FEV_fatal();
extern void    pix_put_fits();
extern void    put_dvec_fits();
extern double  get_pix_fits();

#endif

extern int bitpix;

#define FSkey_set(key_name, value)  lFEVset(key_name, (long)value); FEVexport(key_name)
#define FLkey_set(key_name, value)  lFEVset(key_name, value); FEVexport(key_name)
#define FLLkey_set(key_name, value) lFEVset(key_name, value); FEVexport(key_name)
#define FFkey_set(key_name, value)  dFEVset(key_name, (double)value); FEVexport(key_name)
#define FDkey_set(key_name, value)  dFEVset(key_name, value); FEVexport(key_name)
#define FCkey_set(key_name, value)  sFEVset(key_name, value); FEVexport(key_name)
#define Fckey_set(key_name, value)  cFEVset(key_name, value); FEVexport(key_name)

#define FSkey_get(key_name) (short)atol(FEVget(key_name))
#define FLkey_get(key_name) atol(FEVget(key_name))
#define FFkey_get(key_name) (float)atof(FEVget(key_name))
#define FDkey_get(key_name) atof(FEVget(key_name))
#define FCkey_get(key_name) FEVget(key_name)

#define Hupdate_fits(stream)    FHupdate(stream)
#define Hinit_fits(stream)      FHinit(stream)
