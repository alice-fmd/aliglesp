#include <math.h>
/** 
 * calculate C_l by a_lm 
 * 
 * @param dl   output D_l array                          
 * @param lmax input a_lm array - complex (nalm/2, nalm) 
 * @param alm  maximum value of l                        
 * @param nalm dimension of a_lm array (Nmx, nalm=2*Nmx) 
 */
void alm2dl(double* dl, int lmax, double* alm, int nalm)
{
  register int l, m;
  double cl, pi2 = 2 * M_PI;
  double T_CMB = 2.726;
  
  T_CMB *= T_CMB;
  for(l=0; l<=lmax; l++) {
    int ll = nalm * l;
    double s_alm = alm[ll] * alm[ll];
    for(m=1; m<=l; m++) {
      double v_r = alm[ll+m];
      double v_i = alm[ll+lmax+m+1];
      s_alm += 2. * (v_r * v_r + v_i * v_i);
    }
    cl = s_alm / (double)(2*l + 1);
    dl[l] = cl * (double)(l*(l+1)) / pi2 / T_CMB;
  }
}
/*
 * EOF
 */
