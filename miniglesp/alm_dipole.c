#include <math.h>

/**
 *
 *   loop over l - SUM_l[a_{lm}P_l^m(x_p)]
 *   input: x(Nx),Nph(Jmx),apm(0:Jmx,Nmx),bpm(0:Jmx,Nmx)
 *   ouput: f(Nmx,Nmp)
 */
void alm_dipole(int Nmx, int Nmp, int Lmx, int Nx,
		 double* x, double* apm, double* bpm, double* f)
{
  register int ix;
  double cnorm = 0;
  int    Kx    = (Nx+1)/2;

  /* for m=0,1,-1 */
  for(ix=0; ix < Kx; ix++) { /* for all x(ix) */
    double xc        =  -sqrt(1. - x[ix] * x[ix]);
    int    jx        =  Nx - 1 - ix;
    f[Nmp + 0]       += x[ix] * apm[ix];        /* a_10    */
    f[Nmp + 1]       += xc    * apm[Nmx+ix];    /* Re a_11 */
    f[Nmp + Lmx + 2] += xc    * bpm[Nmx+ix];    /* Im a_11 */
    if(ix < jx) {
      f[Nmp + 0]       += -x[ix] * apm[jx];       /* a_10    */
      f[Nmp + 1]       += +xc    * apm[Nmx + jx]; /* Re a_11 */
      f[Nmp + Lmx + 2] += +xc    * bpm[Nmx + jx]; /* Im a_11 */
    }
  }                 /* for all x(ix)       */

  /* normalization and record of alm */
  cnorm            =  -sqrt(1.5/M_PI);
  f[Nmp + 0]       *= cnorm / M_SQRT2;         /* a_10  */
  f[Nmp + 1]       *= cnorm / 2.;          /* Re a_11 */
  f[Nmp + Lmx + 2] *= -cnorm / 2.;          /* Im a_11 */

  return;
}
//
// EOF
//
