#include "miniglesp.h"
#include "fits.h"

void 
out_glesp_alm(double* alm, int lmax, int nbuf, int lmax_print, FILE* fp_out)
{
  register int l, m, bb;
  int  nlm     = (lmax_print+1)*(lmax_print+2) / 2;
  int block_data = 0;
  int nsize      = 12;

  /************/
  /* set head */
  /************/
  Fckey_set("SIMPLE",    "T");
  FSkey_set("BITPIX",    8);
  FSkey_set("NAXIS",     0);
  Fckey_set("EXTEND",    "T");
  FCkey_set("COMMENT",  "a_{lm} coefficients");

  FHupdate(fp_out);
  FHfree();

  FCkey_set("XTENSION",  "BINTABLE");
  FSkey_set("BITPIX",  8);
  FSkey_set("NAXIS",   2);
  FLkey_set("NAXIS1",  nsize);
  FLkey_set("NAXIS2",  nlm);
  FLkey_set("PCOUNT",  0);
  FLkey_set("GCOUNT",  1);

  FLkey_set("TFIELDS", 3);
  FCkey_set("EXTNAME", "A_lm DATA");

  FCkey_set("TFORM1",  "1J"); /* 1st field: index        */
  FCkey_set("TTYPE1",  "INDEX");
  FCkey_set("TUNIT1",  "UNITLESS");
  FCkey_set("TFORM2",  "1E"); /* 2nd field: real a_lm    */
  FCkey_set("TTYPE2",  "REAL");
  FCkey_set("TUNIT2",  "mK");
  FCkey_set("TFORM3",  "1E"); /* 3d field:  imagine a_lm */
  FCkey_set("TTYPE3",  "IMAG");
  FCkey_set("TUNIT3",  "mK");

  FCkey_set("OBJECT",  "A_LM");
  FLkey_set("LMAX",    lmax_print);
  FCkey_set("ORIGIN",  "GLESP");

  FHupdate(fp_out);
  FHfree();

  for (bb=l=0; l<=lmax_print; l++,bb+=nbuf) {
    register int ll = l * l + l + 1;
    for (m=0; m<=l; m++) {
      double v = ll+m;
      bitpix = LONGPIX;
      put_pix_fits(fp_out, v);

      bitpix = FLOATPIX;
      v = alm [bb+m];
      put_pix_fits(fp_out, v);

      bitpix = FLOATPIX;
      v = alm [bb+m+lmax+1];
      put_pix_fits(fp_out, v);
    }
  }

  block_data = nlm * nsize;
  if(block_data % BLOCK_SIZE != 0) {
    register int i;
    long len = BLOCK_SIZE * (block_data / BLOCK_SIZE + 1);
    len -= block_data;
    for(i=0; i<len; i++)   fputc('\0', fp_out);
  }

  return;
}
//
// EOF
// 
