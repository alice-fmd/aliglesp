/**
 * Given an array x[0,...,n-1], returns a value i such that x_in is
 * between x[i] and x[i+1]. x must be monotonic, either increasing or
 * decreasing. i=-1 or i=n-1 is returned to indicate that x is out of
 * range (Press et al., 2002)
 *
 * @param n    Size of X
 * @param x    Array to search 
 * @param x_in X value to find 
 *
 * @return Index i such that x[i] <= x < x[i+1] 
 */
int x_locate(int n, double* x, double x_in)
{
  int i     = -1;
  int il    = -1;        /*  initialize lower and  */
  int iu    = n;         /*  upper limits          */
  int n1    = n-1;
  int ascnd = (x[n1] >= x[0]); // Direction of array 
  
  /* if we are not yet done compute a midpoint */
  while (iu-il > 1) {
    int im = (iu+il) >> 1; // divide by 2 
    if ((x_in >= x[im]) == ascnd)
      il = im; /* and replace either the lower limit */
    else
      iu = im; /* or the upper limit, as appropriate */
  } /* repeat until the test condition is satified*/
  
  /* Then set the output */
  if      (x_in == x[0])  i = 0;
  else if (x_in == x[n1]) i = n1;
  else                    i = il;
  
  return i;
}
//
// EOF
//
