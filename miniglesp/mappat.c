#include "miniglesp.h"
#include <stdlib.h>
#include <string.h>

void
usage(const char* prog)
{
  printf("Usage: %s [OPTIONS]\n\n"
	 "Options:\n"
	 "\t-h         This help\n"
	 "\t-i FILE    Input file\n"
	 "\t-o FILE    Output file\n"
	 "\t-e NUMBER  Number of eta bins\n"
	 "\t-p NUMBER  Number of phi bins\n", prog);
}

#define NBUF 128

int main(int argc, char** argv)
{
  int i    = 0;
  int nEta = 200;
  int nPhi = 20;
  char input[NBUF];
  char output[NBUF];
  
  strncpy(input, "hits.dat", NBUF);
  strncpy(output, "map.dat", NBUF);
  
  for (i = 1; i < argc; i++) { 
    if (argv[i][0] == '-') { 
      switch (argv[i][1]) { 
      case 'h': usage(argv[0]); return 0; 
      case 'e': nEta = atoi(argv[++i]); break;
      case 'p': nPhi = atoi(argv[++i]); break;
      case 'i': strncpy(input, argv[++i], NBUF); break;
      case 'o': strncpy(output, argv[++i], NBUF); break;
      default: 
	fprintf(stderr, "Unknown option %s", argv[i]); return 1;
      }
    }
  }
  return mapit(input, output, nEta, nPhi, 1, -1, 0);
}

//
// EOF
//
