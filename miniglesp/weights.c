#include "miniglesp.h"

/** 
 * Calculate weights of Gauss-Legendre curvature.  The weights are
 * weighted by the bin size in phi
 * 
 * @param x1 
 * @param x2 
 * @param x 
 * @param pw 
 * @param Nx 
 * @param Nph 
 */
void weights(double x1, double x2, double* x, double* pw, int Nx, int* Nph)
{
  register int i, Imx;
  double dphi;
  
  gauleg(x1, x2, x, pw, Nx);
  
  Imx = (Nx+1)/2;
  dphi = TWOPI / (Nph[0]-0.0);
  pw[0]    = dphi * pw[0];
  pw[Nx-1] = dphi * pw[Nx-1];
  
  for(i=1; i<Imx; i++) {
    int jx;
    dphi  = TWOPI / (Nph[i]-0.0);
    pw[i] *= dphi;
    
    jx = Nx - i - 1;
    if(jx > i) {
      pw[jx]  *= dphi;
    }
  }
}
//
// EOF
// 
