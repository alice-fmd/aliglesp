#include <stdio.h>
#include "miniglesp.h"
#include "fits.h"
// #include "glesp.h"


#define  BLOCK_SIZE     2880
int bitpix = 0;
/*
 * created by Oleg Verkhodanov
 *    last correction: 14/05/2006
 */


/** 
 * Write out map to a FITS formatted file 
 * 
 * @param f           Map to write
 * @param x           X axis
 * @param nx          N bins along X
 * @param np          Number of phi bins
 * @param nph         Phi bins
 * @param fp_out      Output file pointer
 */
void outp_glesp_map(double* f, double* x, int nx, int np, int* nph, 
		    FILE* fp_out)
{
  register int i,j,ii;
  char tform1[30], tform2[30], tform3[30];
  long nphi  = 0;     /* total number of elements in nph-arrays */
  int  bitp1 = FLOATPIX;
  int  bitp2 = LONGPIX;
  int  bitp3 = FLOATPIX;
  int  block_data = 0;

  /* Number of entries in table in bytes */
  for (i=0; i<nx; i++)  nphi += nph[i];
  long long long_naxis1 = (long long)nphi * (ABS(bitp3) / 8) +
    nx * (ABS(bitp1) / 8) + nx * (ABS(bitp1) / 8);

  /************/
  /* set head */
  /************/
  Fckey_set("SIMPLE",    "T");
  FSkey_set("BITPIX",    8);
  FSkey_set("NAXIS",     0);
  Fckey_set("EXTEND",    "T");
  FCkey_set("COMMENT",  "GLESP MAP");

  /* 1st HEAD */
  Hupdate_fits(fp_out);
  FHfree();

  FCkey_set("XTENSION",  "BINTABLE");
  FSkey_set("BITPIX",  8);
  FSkey_set("NAXIS",   2);
  FLLkey_set("NAXIS1", long_naxis1);
  FLkey_set("NAXIS2",  1);
  FLkey_set("PCOUNT",  0);
  FLkey_set("GCOUNT",  1);

  FLkey_set("TFIELDS", 3);
  
  sprintf(tform1, "%dE", nx);
  FCkey_set("TFORM1",  tform1); /* 1st field: vector of X */
  FCkey_set("TTYPE1",  "COS(THETA)");
  FCkey_set("TUNIT1",  "COS(RAD)");
  
  sprintf(tform2, "%dJ", nx);
  FCkey_set("TFORM2",  tform2); /* 2nd field: vector nph(x) */
  FCkey_set("TTYPE2",  "DIMS");
  FCkey_set("TUNIT2",  "UNITLESS");

  sprintf(tform3, "%ldE", nphi);
  FCkey_set("TFORM3",  tform3); /* 3d field: vector of V(X,Y)  */
  FCkey_set("TTYPE3",  "TEMPERATURE");
  FCkey_set("TUNIT3",  "K");

  FCkey_set("OBJECT",  "CMB map");
  FCkey_set("ORIGIN",  "GLESP");
  FCkey_set("PIXTYPE", "GLESP");
  
  /* 2nd HEAD */
  Hupdate_fits(fp_out);

  /* recording 1-dim array of  X-argument vector */
  bitpix = bitp1;
  for(i=0; i<nx; i++) {
    double v = x[i];
    put_pix_fits(fp_out, v);
  }

  /* recording 1-dim array of dimensions of \phi-vector */
  bitpix = bitp2;
  for(i=0; i<nx; i++) {
    double v = nph[i];
    put_pix_fits(fp_out, v);
  }
  
  /* recording 2-dim array */
  bitpix = bitp3;
  for(ii=i=0; i<nx; i++,ii+=np) {
    for(j=0; j<nph[i]; j++) {
      double v = f[ii+j];
      put_pix_fits(fp_out, v);
    }
  }

  block_data = long_naxis1;
  if(block_data % BLOCK_SIZE != 0) {
    long len = BLOCK_SIZE * ( block_data / BLOCK_SIZE + 1);
    len -= block_data;
    for(i=0; i<len; i++)   fputc('\0', fp_out);
  }
  
  /* free memory from head */
  FHfree();
  
  return;
}


//
// EOF
//

