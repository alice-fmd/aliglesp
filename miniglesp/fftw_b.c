#include <fftw3.h>

/* coeffcients from map */
void fftw_b(int n, double* f_in, double* f_out)
{
  static   fftw_plan p;
  static   int       n_old = 0;
  register int       i;
  int                n2 = (n+1)/2;
  
  if (n_old != 0 && n_old != n)  fftw_destroy_plan(p);

  if (n_old != n) {
    p = fftw_plan_r2r_1d(n, f_in, f_out, FFTW_R2HC, FFTW_ESTIMATE);
    n_old = n;
    fftw_execute(p);
  } else {
    fftw_execute_r2r(p, f_in, f_out);
  }

  // Re-order output. 
  // f_out = { r_0, r_1, ..., r_m, i_o, i_{o-1}, ..., i_1 }
  // 
  // where m = n/2; o = (n+1)/2-1
  f_in[0] = f_out[0]; // Only real part for 0th moment
  for (i=1; i<n2; i++) {
    // fi points to f_in[2*i] 
    register double* fi = f_in + i + i; 
    *(fi-1) = f_out[i];   // Assign r_i to f_in[2*i-1] 
    *fi     = f_out[n-i]; // Assign i_i to f_in[2*i]
  }
  // On return, f_out = { r_0, i_1, r_1, ..., r_m, }
  return;
}
//
// EOF
//
