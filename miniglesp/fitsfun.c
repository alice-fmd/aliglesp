
/*
 * ENVIRONMENT  for FITS reading and writing
 *      by Oleg Verkhodanov
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <sys/types.h>
#include "fits.h"

#define BOOLEAN         int
#define FALSE           0
#define TRUE            1
#define BYTE            8

#define PR              fprintf
#define ER              stderr
#define ABS(x)          ((x) < 0 ? -(x) : (x))

#define BLOCK_SIZE      2880

#define lowbyte(w) ((w) & 0377)
#define highbyte(w) lowbyte(((w)&0xff00 >> 8)

#define in_head(stream) Hinit(stream)
#define out_head(stream) Hupdate(stream)
#define head_length Hlen()

#define FSkey_set(key_name, value)  lFEVset(key_name, (long)value); FEVexport(key_name)
#define FLkey_set(key_name, value)  lFEVset(key_name, value); FEVexport(key_name)
#define FFkey_set(key_name, value)  dFEVset(key_name, (double)value); FEVexport(key_name)
#define FDkey_set(key_name, value)  dFEVset(key_name, value); FEVexport(key_name)
#define FCkey_set(key_name, value)  sFEVset(key_name, value); FEVexport(key_name)
#define Fckey_set(key_name, value)  cFEVset(key_name, value); FEVexport(key_name)

#define FSkey_get(key_name) (short)atol(FEVget(key_name))
#define FLkey_get(key_name) atol(FEVget(key_name))
#define FFkey_get(key_name) (float)atof(FEVget(key_name))
#define FDkey_get(key_name) atof(FEVget(key_name))
#define FCkey_get(key_name) FEVget(key_name)

char **Fenviron;
extern void ffhead_correct(void);

#define MAXVAR 2048
#define HSRLEN 120

static int max_var = MAXVAR;
static int first_EVp_call = 1;

static struct F_varslot {
    char *name;             /* keyword */
    char *val;              /* value   */
    char *comm;             /* comment */
    BOOLEAN exported;       /*         */
} *F_sym; /* [MAXVAR]; */

#define NULLS  (struct F_varslot  *)0


static struct F_varslot *find(name) /*  */
char *name;
{
    int i;
    struct F_varslot *v;

    v = NULLS;
    for (i = 0; i < max_var; i++)
	if (F_sym[i].name == NULL){
	    if (v == NULLS)
		v = &F_sym[i];
	}
	else if (strcmp(F_sym[i].name,name) == 0) {
	    v = &F_sym[i];
	    break;
	}
    return(v);
}

static BOOLEAN assign(p, s) /* */
char **p, *s;
{
    int size;

    size = strlen(s)+1;
    if (*p == NULL) {
	if ((*p = (char *)malloc(size)) == NULL)
	   return(FALSE);
    }
    else if ((*p = (char *)realloc(*p, size)) == NULL)
	    return(FALSE);
    strcpy(*p,s);
    return(TRUE);
}

void FEVp_buf_size(n)
int n;
{
    if ( first_EVp_call )
	    max_var = n;

    return;
}

BOOLEAN FEVset(name, val) /*  */
char *name, *val;
{
     struct F_varslot *v;

     if ( first_EVp_call ) {
	    register int k;

	    if ( (F_sym = (struct F_varslot *)calloc(max_var,
		  sizeof (struct F_varslot))) == (struct F_varslot *)0 ) {
			PR(ER, "EVp: no memory for header's buffers\n");
			max_var = 0;
			return(FALSE);
	    }

	    for(k=0; k<max_var; k++) {
		    v = &F_sym[k];
		    v->name = NULL;
		    v->val  = NULL;
		    v->comm = NULL;
		    v->exported = 0;
	    }
	    first_EVp_call = 0;
     }

     if ((v = find(name)) == NULLS)
	 return(FALSE);
     v->comm = NULL;
     return(assign(&v->name,name) && assign(&v->val, val));
}


BOOLEAN FEVexport (name) /*  */
char *name;
{
    struct F_varslot *v;

    if ((v = find(name)) == NULLS)
	return(FALSE);
    if (v->name == NULL)
	if (!assign(&v->name, name) || !assign(&v->val,""))
	    return(FALSE);
    v->exported = TRUE;
    return(TRUE);
}

char *FEVget(name)
char *name;
{
     struct F_varslot *v;

     if ((v = find(name)) == NULLS || v->name == NULL)
	 return(NULL);
     return(v->val);
}

BOOLEAN FEVinit()
{
   int i, namelen;
   char name[20];

   for(i = 0; Fenviron[i] != NULL; i++) {
       namelen = strcspn(Fenviron[i], "=");
       strncpy(name, Fenviron[i], namelen);
       name[namelen] = '\0';
       if (!FEVset(name, &Fenviron[i][namelen+1]) || !FEVexport(name))
	   return(FALSE);
   }
   return(TRUE);
}

#define HSTRLEN 120  /*  */
static char hdrstr[HSTRLEN + 1];
static int FITS_head_len;

extern char *fgets80(char *, FILE *);
extern char *cvtofv(char *, char *);

int FHinit_FITS(stream, nrb)  /* reads common FITS-file head */
FILE *stream;
int nrb;       /* number_of_read_bytes; */
{
     char *pe, *pn;
     char comm[HSTRLEN], buff[60];
     struct F_varslot *v;

     comm[0] = '\0';

     if ( first_EVp_call ) {
	    register int k;
	    struct F_varslot *vv;

	    if ( (F_sym = (struct F_varslot *)calloc(max_var,
		  sizeof (struct F_varslot))) == (struct F_varslot *)0 ) {
			PR(ER, "FEVp: no memory for header's buffers\n");
			max_var = 0;
			return(FALSE);
	    }

	    for(k=0; k<max_var; k++) {
		    vv = &F_sym[k];
		    vv->name = NULL;
		    vv->val  = NULL;
		    vv->comm = NULL;
		    vv->exported = 0;
	    }
	    first_EVp_call = 0;
     }

     FITS_head_len = nrb;

     while (fgets80(hdrstr, stream) != NULL) {
	  short key;

	  hdrstr[81] = '\n';
	  hdrstr[82] = '\0';

	  /* FITS_head_len += strlen(hdrstr); */
	  FITS_head_len += 80;
//        if (strncmp(hdrstr, "END",  3) == 0) break;
	  if (strncmp(hdrstr, "END",  3) == 0 &&
		strncmp(hdrstr, "END-", 4) != 0) break;
	  if (strncmp(hdrstr, "COMMENT", 7) == 0)   continue;
	  if (strncmp(hdrstr, "HISTORY", 7) == 0)   continue;
	  if (strncmp(hdrstr, "    ", 4) == 0)   continue;

	  /* missing empty string */
	  pe = hdrstr; key = 0;
	  while(*pe) {
		 if(*pe != '\t' && *pe != ' ' && *pe != '\n')  key = 1;
		 pe++;
	  }
	  if(!key)  continue;

	  /* reading string till symbol '=' */
	  pe = strchr(hdrstr, '=');
	  *pe = '\0';

	  /* deleting '\t' and ' ' between keyword and '=' */
	  pn = hdrstr;
	  while(*pn && *pn != ' ' && *pn != '\t') pn++;
	  *pn = '\0';

	  /* missing '\t' and ' ' between '=' and value */
	  while(*++pe && (*pe == '\t' || *pe == ' ')) ;

	  /* reading value of keyword */
/*
	  pn = strchr(pe, '\n');
	  *pn = '\0'; *++pn = '\0';
*/
	  /* deleting always from ' \' or ' /' */
	  pn = pe; key = 0;
	  while(*pn) {
		 if(*pn == ' ')   key = 1;
		 pn++;
		 if(key && (*pn == '\\' || *pn == '/' || *pn == ' ') ) break;
		 else key = 0;
	  }
	  if(key) *--pn = '\0';  else *pn = '\0';

	  /* reading comment */
	  pn++;
	  while(*pn &&(*pn == '/' || *pn == '\\'|| *pn == ' '|| *pn == '\t'))
	       pn++;
	  strcpy(comm,pn);
	  if (!FEVset(hdrstr, cvtofv(pe, buff)) || !FEVexport(hdrstr))
			return(0);
	  if ((v = find(hdrstr)) == NULLS)     return(0);
	  if (!assign(&v->comm, comm) )        return(0);
     }

     if(FITS_head_len % BLOCK_SIZE != 0)  {
	    register int j;
	    int len = BLOCK_SIZE * ( FITS_head_len / BLOCK_SIZE + 1);
	    len -= FITS_head_len;
	    for(j=0; j<len; j++)   fgetc(stream);
	    FITS_head_len += len;
     }
     return(FITS_head_len);
}

int FHinit(stream)  /* reads common F-file head */
FILE *stream;
{
   return  FHinit_FITS(stream, 0);  /* reads common FITS-file head */
}

int FHupdate(stream) /* output head of image */
FILE *stream;
{
    extern int Fits_update(FILE *);
    return Fits_update(stream);
}

int FHlen()
{
    return FITS_head_len;
}

void sFEVset(name,value)
char *name, *value;
{
 int len = strlen(value);
 char cvalue[30];
 if (len >= 8)
	sprintf(cvalue, "'%s'", value);
 else {
	sprintf(cvalue, "'%s", value);
	len = 8-len;
	while (len--) strcat(cvalue, " ");
	strcat(cvalue, "'");
 }

 if(!FEVset(name, cvalue))
	FEV_fatal("sFEVset");
}

void cFEVset(name, value)
char *name, *value;
{
 if(!FEVset(name, value))
	FEV_fatal("cFEVset");
}

void iFEVset(name,i)
char *name;
int i;
{
 char value[20];

 sprintf(value,"%d", i);
 if(!FEVset(name, value))
	FEV_fatal("iFEVset");
}

void lFEVset(name, l)
char *name;
long l;
{
 char value[20];

 sprintf(value,"%ld", l);
 if(!FEVset(name, value))
	FEV_fatal("iFEVset");
}

void fFEVset(name, f)
char *name;
float f;
{
 char value[20];

 sprintf(value, "%f", f);
 if(!FEVset(name, value))
	FEV_fatal("fFEVset");
}

void dFEVset(name, d)
char *name;
double d;
{
 char value[20];

#ifdef __GNUC__
 sprintf(value, "%f", d);
#else
 sprintf(value, "%lf", d);
#endif
 if(!FEVset(name, value))
	FEV_fatal("fFEVset");
}

void FEV_fatal(msg) /* print error message and terminate */
char *msg;
{
    fprintf(stderr, "FEVp_error: %s\n", msg);
    exit(1);
}

void FHfree()
{
     register int i;

     for (i = 0; i < max_var; i++) {
	  if (F_sym[i].name != NULL) {
		 free(F_sym[i].name);
		 F_sym[i].name = NULL;
	  }
	  if (F_sym[i].val  != NULL) {
		 free(F_sym[i].val);
		 F_sym[i].val  = NULL;
	  }
     }
}

void FEVdel(name)
char *name;
{
     register int i,j;
     char bufval[HSTRLEN + 1];
     // BOOLEAN exported;

     for (i = 0; i < max_var; i++)
	if (F_sym[i].name != NULL)
	    if (strcmp(F_sym[i].name,name) == 0) {
		free(F_sym[i].name);
		free(F_sym[i].val);
		F_sym[i].name = NULL;  F_sym[i].val = NULL;
		for (j = i; j < max_var-1; j++) {
		       if(F_sym[j+1].val  != NULL) {
			       strcpy(bufval,F_sym[j+1].val);
			       free(F_sym[j+1].val);
			       F_sym[j+1].val = NULL;
		       }
		       if(F_sym[j+1].name != NULL) {
			       strcpy(hdrstr,F_sym[j+1].name);
			       free(F_sym[j+1].name);
			       F_sym[j+1].name = NULL;
			       F_sym[j].exported = F_sym[j+1].exported;
			       FEVset(hdrstr,bufval); FEVexport(hdrstr);
		       }
		}
		break;
	    }
}

/* character F-value 'st' to character FITS-value buf */
char *fvtocv(st,buf)
char *st;
char *buf;
{
	register char *cs,*s;
	register int len;

	if(strcmp(st,"UNITLESS") == 0)   strcpy(buf,"'                '");
	else {
		/* length of string value */
		len = strlen(st);
		cs = buf; s = st;
			/* writing first symbol " ' " */
		*cs++ = '\'';
			/* coping  st to cs " */
		while( (*cs++ = *s++)) ;
		cs--;
			/* cut till length 20 */
		if(len > 18) {  /* 18 because first symbol is ' */
			buf[19] = '\'';
			buf[20] = '\0';
		} else if (len < 8)  {
			len = 8 - len;
			while(len--) *cs++ = ' ';
			*cs++ = '\''; *cs = '\0';
		} else {
			*cs++ = '\''; *cs = '\0';
		}
	}
	return buf;
}

int Fits_update(stream)
FILE *stream;
{
  register int i,j;
  struct F_varslot *v;
  int len_hd, len_val, head_len, len;
  char hdrstr[HSRLEN+1];

     head_len = 0;

     for (i = 0; i < max_var; i++) {
	 v = &F_sym[i];
	 if (v->name == NULL || !v->exported)
	       continue;
	 len_hd  = strlen(v->name);
	 len_val = strlen(v->val);
	 sprintf(hdrstr, "%s", v->name);
	 for(j=len_hd; j<8; j++)   hdrstr[j] = ' ';
	 if (strncmp(v->name, "COMMENT",7) == 0) {
		char *cs = v->val;
		for(j=7; j<80; j++)   hdrstr[j] = ' ';
		if (strlen(cs) + 9 > 79) {
		    sprintf(hdrstr+9, "%.70s", cs);
		} else {
		    sprintf(hdrstr+9, "%s", cs);
		    hdrstr[9 + strlen(cs)] = ' ';
		}
	 } else {
		hdrstr[8] = '=';
		for(j=9; j<30; j++)   hdrstr[j] = ' ';
		if(*v->val == '\'')  {/* character */
		       sprintf(hdrstr+10, "%s", v->val);
		       hdrstr[10+len_val] = ' ';
		} else
		       sprintf(hdrstr+30-len_val, "%s", v->val);
		for(j=30; j<80; j++)   hdrstr[j] = ' ';
		hdrstr[31] = '/';
		if ( v->comm != NULL ) {
		       sprintf(hdrstr+33, "%s", v->comm);
		       hdrstr[33 + strlen(v->comm)] = ' ';
		}
	 }
	 for(j=0; j<80; j++)   fputc(hdrstr[j], stream);
	 hdrstr[80] = '\0';
	 head_len += strlen(hdrstr);
     }
     fputc('E', stream);
     fputc('N', stream);
     fputc('D', stream);
     for(j=0; j<77; j++)   fputc(' ', stream);
     head_len += 80;
     if(head_len % BLOCK_SIZE != 0)  {
	    len = BLOCK_SIZE * ( head_len / BLOCK_SIZE + 1);
	    len -= head_len;
	    for(j=0; j<len; j++)   fputc(' ', stream);
     }
     fflush(stream);
     FITS_head_len = head_len;
     return (head_len);
}

#if defined sun || defined IRIX
static int Sun_key = 1;
#else
static int Sun_key = 0;
#endif

int F_sun_data_key = 1;

static union _tt_ {
       char b[8];
       short s;
       long l;
       float f;
       double d;
} p;
static union _tt_ pp;

extern int bitpix;

void put_dvec_fits(stream, vec, n)
FILE *stream;
double *vec;
int n;
{
	register int i;
	long length = n * ABS(bitpix) / 8;

	for(i=0; i<n; i++)
		put_pix_fits(stream, vec[i]);

	if(length % BLOCK_SIZE != 0) {
		long len = BLOCK_SIZE * ( length / BLOCK_SIZE + 1);
		len -= length;
		for(i=0; i<len; i++)   fputc('\0', stream);
	}
	return;
}

void put_dvec_fits_pol(stream, vec, q, u, n, byte_block)
FILE *stream;
double *vec;
double *q, *u;
int n,
    byte_block;  /* number of bytes in recorded block */
{
	register int i, j;
	int byte_pix  = ABS(bitpix) / 8;
	int pix_block = byte_block / byte_pix;
	int num_block = n / pix_block,   /* number of full blocks per map */
					 /* to be recorded                */
	    rest_block = n % pix_block;

	/* total length in bytes of recorded data */
	long length = 3 * byte_block * (num_block + (rest_block ? 1 : 0));

	for(i=0; i<num_block; i++) {
	    int ii = i * pix_block;
	    for(j=0; j<pix_block; j++) put_pix_fits(stream, vec[ii+j]);
	    for(j=0; j<pix_block; j++) put_pix_fits(stream, q[ii+j]);
	    for(j=0; j<pix_block; j++) put_pix_fits(stream, u[ii+j]);
	}
	if ( rest_block ) {
	    int add_bytes  = byte_pix * (pix_block - rest_block);
	    int ii = num_block * pix_block;
	    for(j=0; j<rest_block; j++) put_pix_fits(stream, vec[ii+j]);
	    for(j=0; j<add_bytes;  j++) fputc('\0', stream);
	    for(j=0; j<rest_block; j++) put_pix_fits(stream, q[ii+j]);
	    for(j=0; j<add_bytes;  j++) fputc('\0', stream);
	    for(j=0; j<rest_block; j++) put_pix_fits(stream, u[ii+j]);
	    for(j=0; j<add_bytes;  j++) fputc('\0', stream);
	}

	if(length % BLOCK_SIZE != 0) {
		long len = BLOCK_SIZE * ( length / BLOCK_SIZE + 1);
		len -= length;
		for(i=0; i<len; i++)   fputc('\0', stream);
	}
	return;
}

void put_pix_fits(stream, val)
FILE *stream;
double val;
{
	int _key_ = Sun_key ^ F_sun_data_key ;
/*
static int qq=0;
if (!qq)
PR(ER, "key=%d, sun_key=%d, fsun_key=%d\n",
_key_, Sun_key, F_sun_data_key);
qq++;
*/
	switch (bitpix) {
	case SHORT:
		pp.s = p.s = (int)val;
		if ( _key_ )   {
		       p.b[0] = pp.b[1];
		       p.b[1] = pp.b[0];
		}
		putc(p.b[0], stream); putc(p.b[1], stream);
		break;
	case LONG:
		pp.l = p.l = (long)val;
		if ( _key_ )   {
		       p.b[0] = pp.b[3];
		       p.b[1] = pp.b[2];
		       p.b[2] = pp.b[1];
		       p.b[3] = pp.b[0];
		}
		putc(p.b[0], stream); putc(p.b[1], stream);
		putc(p.b[2], stream); putc(p.b[3], stream);
		break;
	case FLOAT:
		pp.f = p.f = val;
		if ( _key_ )   {
		       p.b[0] = pp.b[3];
		       p.b[1] = pp.b[2];
		       p.b[2] = pp.b[1];
		       p.b[3] = pp.b[0];
		}
		putc(p.b[0], stream); putc(p.b[1], stream);
		putc(p.b[2], stream); putc(p.b[3], stream);
		break;
	case DOUBLE:
		pp.d = p.d = val;
		if ( _key_ )   {
		       p.b[0] = pp.b[7];
		       p.b[1] = pp.b[6];
		       p.b[2] = pp.b[5];
		       p.b[3] = pp.b[4];
		       p.b[4] = pp.b[3];
		       p.b[5] = pp.b[2];
		       p.b[6] = pp.b[1];
		       p.b[7] = pp.b[0];
		}
		putc(p.b[0], stream); putc(p.b[1], stream);
		putc(p.b[2], stream); putc(p.b[3], stream);
		putc(p.b[4], stream); putc(p.b[5], stream);
		putc(p.b[6], stream); putc(p.b[7], stream);
		break;
	case BYTE:
	default:
		p.s = (int)val;
		putc(p.b[0], stream);
		break;
	}

	return;
}

double
get_pix_fits(stream)
FILE *stream;
{
	int _key_ = Sun_key ^ F_sun_data_key ;
/*
static int qq=0;
if (!qq)
PR(ER, "\nkey=%d, sun_key=%d, fsun_key=%d\n",
_key_, Sun_key, F_sun_data_key);
qq++;
*/
	pp.b[0] = p.b[0] = getc(stream);
	switch (bitpix) {
	case SHORT:
		pp.b[1] = p.b[1] = getc(stream);
		if ( _key_ )   {
		       pp.b[0] = p.b[1];
		       pp.b[1] = p.b[0];
		}
		return (pp.s);
	case LONG:
		pp.b[1] = p.b[1] = getc(stream);
		pp.b[2] = p.b[2] = getc(stream);
		pp.b[3] = p.b[3] = getc(stream);
		if ( _key_ )   {
		       pp.b[0] = p.b[3];
		       pp.b[1] = p.b[2];
		       pp.b[2] = p.b[1];
		       pp.b[3] = p.b[0];
		}
		return (pp.l);
	case FLOAT:
		pp.b[1] = p.b[1] = getc(stream);
		pp.b[2] = p.b[2] = getc(stream);
		pp.b[3] = p.b[3] = getc(stream);
		if ( _key_ )   {
		       pp.b[0] = p.b[3];
		       pp.b[1] = p.b[2];
		       pp.b[2] = p.b[1];
		       pp.b[3] = p.b[0];
		}
		return (pp.f);
	case DOUBLE:
		pp.b[1] = p.b[1] = getc(stream);
		pp.b[2] = p.b[2] = getc(stream);
		pp.b[3] = p.b[3] = getc(stream);
		pp.b[4] = p.b[4] = getc(stream);
		pp.b[5] = p.b[5] = getc(stream);
		pp.b[6] = p.b[6] = getc(stream);
		pp.b[7] = p.b[7] = getc(stream);
		if ( _key_ )   {
		       pp.b[0] = p.b[7];
		       pp.b[1] = p.b[6];
		       pp.b[2] = p.b[5];
		       pp.b[3] = p.b[4];
		       pp.b[4] = p.b[3];
		       pp.b[5] = p.b[2];
		       pp.b[6] = p.b[1];
		       pp.b[7] = p.b[0];
		}
		return (pp.d);
	case BYTE:
	default:
		return (p.b[0]);
	}
}

/* reading 80 bites from stream fp */
char *fgets80(st,fp)
char *st;
FILE *fp;
{
	register int i=0;
	register char *cs = st;
	int c;
	while(i<80 && (c = getc(fp)) != EOF) {
	       *cs++ = c;
	       i++;
	}
	*cs = '\0';
	return (i !=0 ? st : NULL);
}

/* character FITS value st to character F-value buf*/
char *cvtofv(st,buf)
char *st;
char *buf;
{
	register char *cs,*s;

	if(strcmp(st,"'                '") == 0)   strcpy(buf,"UNITLESS");
	else {
		cs = buf; s = st;
		/* missing first symbol " ' " */
		if (*s == '\'') s++;

		/* missing  last symbol " ' " */
		while(*s && (*cs++ = *s++) != '\'' ) ;
		/* if its '\0' or symbol after '\'', then go back */
		cs--;
		/* if it's '\'', go back */
		if (*cs == '\'') cs--;

		/* deleting symbols ' ' at the end */
		while(*cs == ' ') cs--;
		cs++; *cs = '\0';
	}
	return buf;
}

void ffhead_correct()
{
	char hdrstr[HSRLEN+1];
	int ffile_type = 0;

	if(FEVget("BUNIT")  != NULL)
	       { FCkey_set("BUNIT", fvtocv(FEVget("BUNIT"),hdrstr)); }
	else if (!ffile_type) { FCkey_set("BUNIT","'                '"); }

	if(FEVget("CTYPE1")  != NULL)
	       { FCkey_set("CTYPE1",fvtocv(FEVget("CTYPE1"),hdrstr)); }
	else if (!ffile_type)  { FCkey_set("CTYPE1","'                '"); }

	if(FEVget("CTYPE2")  != NULL)
		{ FCkey_set("CTYPE2",fvtocv(FEVget("CTYPE2"),hdrstr)); }

	if(FEVget("ORIGIN")  != NULL)
		{ FCkey_set("ORIGIN",fvtocv(FEVget("ORIGIN"),hdrstr)); }

	if(FEVget("OBJECT")  != NULL)
		{ FCkey_set("OBJECT",fvtocv(FEVget("OBJECT"),hdrstr)); }

	if(FEVget("DATA")  != NULL)
		{ FCkey_set("DATA",fvtocv(FEVget("DATA"),hdrstr)); }

	if(FEVget("DATA-OBS")  != NULL)
		{ FCkey_set("DATA-OBS",fvtocv(FEVget("DATA-OBS"),hdrstr)); }

	if(FEVget("INSTRUME")  != NULL)
		{ FCkey_set("INSTRUME",fvtocv(FEVget("INSTRUME"),hdrstr)); }

	if(FEVget("TELESCOP")  != NULL)
		{ FCkey_set("TELESCOP",fvtocv(FEVget("TELESCOP"),hdrstr)); }

	if(FEVget("OBSERVER")  != NULL)
		{ FCkey_set("OBSERVER",fvtocv(FEVget("OBSERVER"),hdrstr)); }


	if(FEVget("TFORM1")  != NULL)
		{ FCkey_set("TFORM1",fvtocv(FEVget("TFORM1"),hdrstr)); }
	if(FEVget("TFORM2")  != NULL)
		{ FCkey_set("TFORM2",fvtocv(FEVget("TFORM2"),hdrstr)); }
	if(FEVget("TFORM3")  != NULL)
		{ FCkey_set("TFORM3",fvtocv(FEVget("TFORM3"),hdrstr)); }

	if(FEVget("TTYPE1")  != NULL)
		{ FCkey_set("TTYPE1",fvtocv(FEVget("TTYPE1"),hdrstr)); }
	if(FEVget("TTYPE2")  != NULL)
		{ FCkey_set("TTYPE2",fvtocv(FEVget("TTYPE2"),hdrstr)); }
	if(FEVget("TTYPE3")  != NULL)
		{ FCkey_set("TTYPE3",fvtocv(FEVget("TTYPE3"),hdrstr)); }

	if(FEVget("TUNIT1")  != NULL)
		{ FCkey_set("TUNIT1",fvtocv(FEVget("TUNIT1"),hdrstr)); }
	if(FEVget("TUNIT2")  != NULL)
		{ FCkey_set("TUNIT2",fvtocv(FEVget("TUNIT2"),hdrstr)); }
	if(FEVget("TUNIT3")  != NULL)
		{ FCkey_set("TUNIT3",fvtocv(FEVget("TUNIT3"),hdrstr)); }

	if(FEVget("XTENSION")  != NULL)
		{ FCkey_set("XTENSION",fvtocv(FEVget("XTENSION"),hdrstr)); }
	if(FEVget("EXTNAME")  != NULL)
		{ FCkey_set("EXTNAME",fvtocv(FEVget("EXTNAME"),hdrstr)); }
	if(FEVget("CREATOR")  != NULL)
		{ FCkey_set("CREATOR",fvtocv(FEVget("CREATOR"),hdrstr)); }
	if(FEVget("VERSION")  != NULL)
		{ FCkey_set("VERSION",fvtocv(FEVget("VERSION"),hdrstr)); }
}
