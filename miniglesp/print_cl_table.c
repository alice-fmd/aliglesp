#include <stdio.h>

void print_cl_table (FILE* fp_out, double* cl, double* clp, int lmin, int lmax)
{
  register int l;
  
  for (l=lmin; l<=lmax; l++) {
    fprintf(fp_out, "%5d %17.10e\n", l, cl[l]);
  }
  
  return;
}
//
// EOF
//
