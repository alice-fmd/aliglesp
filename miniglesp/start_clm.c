#include "miniglesp.h"

/**
 *    dimension c(2*Lmx+1)
 *    preparation of coefficients for recurrent
 *    relation for P_l^m
 */
void start_clm(int Lmx, int m, double* cmm, double* c)
{
  register int k;
  register int k2;
  register int mm = m + m;
  register int m2 = m * m;

  *cmm = 0.5;
  if(m > 0) {
    for(k = 2; k <= mm; k += 2)  *cmm *= (double)(k+1)/k;
  }
  *cmm = (1 - 2 * (m % 2)) * sqrt(*cmm);

  k        = m+1;
  k2       = k*k;
  c[k]     = sqrt((4 * k2 - 1.) / (k2 - m2));
  c[Lmx+k] = 0.;

  if(m != Lmx) {
    register double *c2 = c + Lmx;
    for(k=m + 2; k <= Lmx; k++) {
      int k1   = k - 1; 
      int k2   = k * k; 
      int km2  = k2-m2;
      c[k]     = sqrt((4 * k2 - 1.) / km2);
      c2[k]    = sqrt((k + k + 1.) * (k1 * k1 - m2) / (k + k - 3.) / km2);
    }
  }
  return;
}
//
// EOF
//
