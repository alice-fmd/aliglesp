#include "miniglesp.h"

/** 
 * Calculate special grid for Legendre Polynomials 
 * 
 * @param x1  Lower bound
 * @param x2  Upper bound 
 * @param x   Array to be filled 
 * @param w   Array of weights (optional)
 * @param n   Number of bins 
 */
void gauleg(double x1, double x2, double* x, double* w, int n)
{
  int    i  = 0;
  int    m  = (n+1) / 2;        // Half number bins 
  double xm = 0.5 * (x2 + x1);  // Mean X 
  double xl = 0.5 * (x2 - x1);  // Half-distance in X 

  for (i=0; i<m; i++) {
    double z  = cos(M_PI * ((i+1)-.25) / (n+.50));
    double z1 = 0; 
    double pp = 0;
    do {
      double p1 = 1.0;
      double p2 = 0.0;
      double p3 = 0;				
      int    j  = 0;
      for(j=0; j<n; j++) {
	p3 = p2;
	p2 = p1;
	p1 = ((2*j+1)*z*p2-j*p3)/(j+1);
      }
      pp = n*(z*p1-p2)/(z*z-1.0);
      z1 = z;
      z  = z1-p1/pp;
    } while (ABS(z-z1) > EPS);

    // Set X coordinates (symmetric) 
    x[i]     = xm - xl * z;
    x[n-1-i] = xm + xl * z;
    
    // Fill in weights too (symmetric)
    if (w != 0) {
      w[i]     = 2.0*xl / ((1. - z*z) * pp*pp);
      w[n-1-i] = w[i];
    }
  }

  return;
}
//
// EOF
//
