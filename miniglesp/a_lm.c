#include "miniglesp.h"


/** 
 * Calculate fourier coefficents 
 * 
 * @param Lmn 
 * @param Lmx 
 * @param Mmn 
 * @param Mmx 
 * @param Nx 
 * @param Vnyq 
 * @param eps 
 * @param Nmx 
 * @param Nmp 
 * @param x 
 * @param pw 
 * @param f 
 * @param Nph 
 * @param wsave 
 * @param cf 
 * @param apm 
 * @param bpm 
 */
void a_lm(int     Lmn,
	  int     Lmx,
	  int     Mmn,
	  int     Mmx,
	  int     Nx,
	  double  eps,
	  int     Nmx,
	  int     Nmp, 
	  double* x,
	  double* pw,
	  double* f,
	  int*    Nph,
	  double* wsave,
	  double* cf,
	  double* apm,
	  double* bpm)
{
  register int m, mm, i, j, ii;

  for(mm=m=0; m<= Lmx; m++, mm += Nmx) {
    for(i=0; i< Nmx; i++) 
      apm[mm+i] = bpm[mm+i] = 0.;
  }

  /* also calculation of dipole */
  // Lmx  = -Lmx;

  /* fast-fourier */
  alm_fast3(Nmx,Nmp,Nx,Lmx,Nph,f,cf,apm,bpm,wsave);

  for(ii=i=0; i< Nmx; i++, ii+= Nmp) 
    for(j=0; j< Nmp; j++)  
      f[ii+j]=0.0;

  alm_dipole(Nmx, Nmp, Lmx, Nx, x, apm, bpm, f);
  Lmn = 2;

  alm_lgndr_m(Nmx, Nmp, Nph, Lmn, Lmx, Mmn, Mmx, eps, Nx,
	      x, apm, bpm, cf, wsave, f);

  return;
}
