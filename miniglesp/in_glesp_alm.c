#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "fits.h"

void in_glesp_alm(double* alm, int* lmax, int nbuf, char* filename)
{
  register int i, l, m;
  int tfields = 0;
  int bitp1, bitp2, bitp3;
  char tform1[30],
    tform2[30],
    tform3[30];
  char str[161];
  int  lmx = -1, lmax_input = *lmax;
  FILE *fp;
  int naxis=0, naxis1=0, naxis2=0;

  /* open file */
  if ((fp = fopen(filename, "r")) == (FILE *)0) {
    fprintf(stderr, "in_glesp_map: can not open file '%s'\n", filename);
    exit(1);
  }
  /* read 1st head */
  FHinit(fp);

  if (FEVget("EXTEND") != NULL)
    strcpy(str, FEVget("EXTEND"));

  if (strcmp(str, "T") != 0) {
    fprintf(stderr, "in_glesp_alm: bad extension in file '%s'\n", filename);
    exit(1);
  }

  /* Free memory from 1st head */
  FHfree();

  /****************************************/
  /* read basic keywods from the 2nd head */
  /****************************************/
  FHinit(fp);

  if (FEVget("XTENSION") != NULL)
    strcpy(str, FEVget("XTENSION"));
  else {
    fprintf(stderr, "in_glesp_alm: no extension in file '%s'\n", filename);
    exit(1);
  }
  if (strcmp(str, "BINTABLE") != 0) {
    fprintf(stderr, "in_glesp_alm: can work only with BINTABLE extension\n");
    exit(1);
  }
  if (FEVget("NAXIS") != NULL)
    naxis  = FSkey_get("NAXIS");
  if (FEVget("NAXIS1") != NULL)
    naxis1  = FLkey_get("NAXIS1");
  if (FEVget("NAXIS2") != NULL)
    naxis2  = FLkey_get("NAXIS2");
  fprintf(stdout, "naxis=%d naxis2=%d, naxis3=%d\n", naxis, naxis1, naxis2);

  if (FEVget("TFIELDS") != NULL)
    tfields = FLkey_get("TFIELDS");
  if (tfields != 3) {
    fprintf(stderr, "in_glesp_alm: TFILEDS value must be equal to "
	    "3 in a_lm file '%s'\n",  filename);
    exit(1);
  }

  if (FEVget("TFORM1") != NULL) {
    strcpy(tform1, FEVget("TFORM1"));
    if (strcmp(tform1, "1J") == 0) {
      bitp1 = LONGPIX;
    } else if (strcmp(tform1, "1I") == 0) {
      bitp1 = SHORTPIX;
    } else {
      fprintf(stderr, "in_glesp_alm: bad value for TFORM1. Sorry...\n");
      exit(1);
    }
  } else {
    fprintf(stderr, "in_glesp_alm: no parameter TFORM1. Sorry...\n");
    exit(1);
  }
  if (FEVget("TFORM2") != NULL) {
    strcpy(tform2, FEVget("TFORM2"));
    if (strcmp(tform2, "1E") == 0) {
      bitp2 = FLOATPIX;
    } else if (strcmp(tform2, "1D") == 0) {
      bitp2 = DOUBLEPIX;
    } else {
      fprintf(stderr, "in_glesp_alm: bad value for TFORM2. Sorry...\n");
      exit(1);
    }
  } else {
    fprintf(stderr, "in_glesp_alm: no parameter TFORM2. Sorry...\n");
    exit(1);
  }
  if (FEVget("TFORM3") != NULL) {
    strcpy(tform3, FEVget("TFORM3"));
    if (strcmp(tform2, "1E") == 0) {
      bitp3 = FLOATPIX;
    } else if (strcmp(tform3, "1D") == 0) {
      bitp3 = DOUBLEPIX;
    } else {
      fprintf(stderr, "in_glesp_alm: bad value for TFORM3. Sorry...\n");
      exit(1);
    }
  } else {
    fprintf(stderr, "in_glesp_alm: no parameter TFORM3. Sorry...\n");
    exit(1);
  }
  if (FEVget("LMAX") != NULL) {
    *lmax = atoi(FEVget("LMAX"));
    printf("read lmax=%d\n", *lmax);
  }

  if (lmax_input > 0 && lmax_input <= *lmax) {
    *lmax = lmax_input;
  } else {
    if (nbuf < (*lmax + 1)*2) {
      if ( (lmax_input + 1) * 2 > nbuf)
	lmax_input = nbuf / 2 - 1;
      *lmax = lmax_input;
    } else {
      if (*lmax > lmax_input) {
	fprintf(stderr, "in_glesp_alm: check set of lmax: lmax=%d, nbuf=%d\n",
		*lmax, nbuf);
	exit(1);
      }
    }
  }
  printf("lmax=%d lmax_input=%d\n", *lmax, lmax_input);

  /* Free memory from 1nd head */
  FHfree();

  /* get values from file */
  for (i=0; i<naxis2; i++) {
    static int flag = 1;
    register int bb, nlm;
    double ddd, val;

    bitpix = bitp1;
    //nlm = get_pix_fits(fp)+0.5;
    ddd = get_pix_fits(fp);
    nlm = ddd + .5;
    l = (int)sqrt((double)(nlm-1));

    if (l > *lmax)
      break;

    if (l*2+1 > nbuf) {
      if (flag) {
	fprintf(stderr, "in_glesp_alm: attention ! l(=%d) is larger ", l);
	fprintf(stderr, "than array dimension (2*l+1>nbuf(%d)).\n", nbuf);
      }
      flag = 0;
      break;
    }
    m = nlm - 1 - l*l - l;
    bb = l * nbuf;

    /* Real part */
    bitpix = bitp2;
    val = get_pix_fits(fp);
    printf("Assigning real part l=%d, m=%d -> alm[%2d+%2d     ]=alm[%4d]=%f\n", 
	   l, m, bb, m, bb+m, val);
    alm [bb+m]        = val;
    if (feof(fp)) {
      fprintf(stderr, "in_glesp_alm: unexpected end of file at l=%d and m=%d\n",
	      l, m);
      break;
    }
    /* Imagine part */
    bitpix = bitp3;
    val = get_pix_fits(fp);
    printf("Assigning imag part l=%d, m=%d -> alm[%2d+%2d+%2d+1]=alm[%4d]%f\n", 
	   l, m, bb, m, *lmax, bb+m+*lmax+1, val);
    alm [bb+m+ *lmax+1] = val;
    if (feof(fp)) {
      fprintf(stderr, "in_glesp_alm: unexpected end of file at l=%d and m=%d\n",
	      l, m);
      break;
    }
    if (l > lmx)   lmx = l;
  }
  if (*lmax <= 0)  *lmax = lmx;

  fclose(fp);
  return;
}
//
// EOF
//
  

