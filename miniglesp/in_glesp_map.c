#include "miniglesp.h"
#include "fits.h"
#include <string.h>
#include <stdlib.h>
extern int FHinit_FITS(FILE *, int);

/** 
 * 
 * 
 * @param f        2-dim map                          
 * @param x 	   argument, x = cos theta            
 * @param nx 	   number of elements in x-array      
 * @param np 	   max number of elements by phi-axis 
 * @param nph 	   number of elements in phi-array    
 * @param filename File to read 
 */
int in_glesp_map(double** f, double** x, int* nx, int* np, 
		  int** nph, const char* filename)
{
  register int i,j,ii;
  register FILE *fp_in;
  double fmax = -1.e+30, fmin = 1.e+30;
  long int nphi=0, nph_max=0;
  char extend[10];
  char xtens[20];
  char ttype1[30];  
  char ttype2[30];
  char ttype3[30];
  char tform1[30];  
  char tform2[30];
  char tform3[30];
  char tunit1[30];  
  char tunit2[30];
  char tunit3[30];
  char tdim1[30];  
  char tdim2[30];
  char tdim3[30];
  int tfields;
  char origin[30];
  char object[30];

  if ((fp_in = fopen(filename, "r")) == 0) {
    fprintf(stderr, "Cannot open file %s\n", filename);
    return 1;
  }

  /****************************************/
  /* read basic keywords from the 1st head */
  /****************************************/
  /* Hinit_fits(fp_in); */
  FHinit_FITS(fp_in, 0);

  if (FEVget("EXTEND") != NULL)
    strcpy(extend, FEVget("EXTEND"));

  if (strcmp(extend, "T") != 0) {
    fprintf(stderr, "bad extension in file '%s'\n", filename);
    return 1;
  }
  FHfree();

  /****************************************/
  /* read basic keywords from the 2nd head */
  /****************************************/
  /* Hinit(fp_in); */
  FHinit_FITS(fp_in, 0);
  
  if (FEVget("XTENSION") != NULL)
    strcpy(xtens, FEVget("XTENSION"));

  if (strcmp(xtens, "BINTABLE") != 0) {
    fprintf(stderr, "Cannot do BINTABLE\n");
    return 1;
  }
  if (FEVget("TFIELDS") != NULL)
    tfields = FLkey_get("TFIELDS");
  
  if(tfields != 3) {
    fprintf(stderr, "Bad number of fields %d\n", tfields);
    return 1;
  }

  if (FEVget("TFORM1") != NULL)   strcpy(tform1, FEVget("TFORM1"));
  if (FEVget("TDIM1") != NULL)    strcpy(tdim1, FEVget("TDIM1"));
  if (FEVget("TTYPE1") != NULL)   strcpy(ttype1, FEVget("TTYPE1"));
  if (FEVget("TUNIT1") != NULL)   strcpy(tunit1, FEVget("TUNIT1"));
  
  if (FEVget("TFORM2") != NULL)   strcpy(tform2, FEVget("TFORM2"));
  if (FEVget("TDIM2") != NULL)    strcpy(tdim2, FEVget("TDIM2"));
  if (FEVget("TTYPE2") != NULL)   strcpy(ttype2, FEVget("TTYPE2"));
  if (FEVget("TUNIT2") != NULL)   strcpy(tunit2, FEVget("TUNIT2"));
  
  if (FEVget("TFORM3") != NULL)   strcpy(tform3, FEVget("TFORM3"));
  if (FEVget("TDIM3") != NULL)    strcpy(tdim3,  FEVget("TDIM3"));
  if (FEVget("TTYPE3") != NULL)   strcpy(ttype3, FEVget("TTYPE3"));
  if (FEVget("TUNIT3") != NULL)   strcpy(tunit3, FEVget("TUNIT3"));
  
  if (FEVget("OBJECT") != NULL)   strcpy(object, FEVget("OBJECT"));
  if (FEVget("ORIGIN") != NULL)   strcpy(origin, FEVget("ORIGIN"));

  /* set grid flag */
  
  if(*(tform1)=='\0'|| *(tform2)=='\0'|| *(tform3)=='\0') {
    fprintf(stderr, "bad value of dimensions. Check TFORM# keywords.\n");
    return 1;
  }

  /* extract dimension of the 1st array */
  i = atoi(tform1);

  /* extract dimensions of the 2nd array */
  *nx = atoi(tform2);
  if (*nx != i) {
    fprintf(stderr, "bad dimension by X-axis...\n");
    return 1;
  }

  /**********************************/
  /* take memory and read 1st field */
  /**********************************/
  if ((*x = (double *)calloc((unsigned)*nx, sizeof **x)) ==
      (double *)0)
    goto nomem;

  /* read 1-dim array of  X-argument vector */
  /* bitpix = ph->bitpix1 = atoi(ph->tform1) / *nx * 8; */
  i = tform1[strlen(tform1)-1];
  if (i == 'D') bitpix = DOUBLEPIX;
  else          bitpix = FLOATPIX;
  for(i=0; i < *nx; i++) {
    (*x)[i] = get_pix_fits(fp_in);
  }

  /**********************************/
  /* take memory and read 2nd field */
  /**********************************/
  if ((*nph = (int*)calloc((unsigned)*nx, sizeof **nph))==(int *)0)
    goto nomem;

  /* read 1-dim array of Nph-vector */
  /* bitpix = ph->bitpix2 = atoi(ph->tform2) / *nx * 8; */
  i = tform2[strlen(tform2)-1];
  switch (i) {
  case 'I':  bitpix = SHORTPIX; break; 
  case 'J':  bitpix = LONGPIX;  break;
  default:   
  case 'E':  bitpix = FLOATPIX; break;
  }
  for(j=0; j < *nx; j++) {
    (*nph)[j] = get_pix_fits(fp_in) + 0.5;
    if ( (*nph)[j] > nph_max )  nph_max = (*nph)[j];
    nphi += (*nph)[j];
  }

  /**********************************/
  /* take memory and read 3d field */
  /**********************************/
  /* find memory for arrays */
  if ((*f =(double*)calloc((unsigned)(*nx *nph_max), sizeof **f)) ==
      (double *)0) {
    nomem:
    fprintf(stderr, "Failed to allocate memory for array\n");
    return 1;
  }

  /* in memory we keep transposed image:  */
  /*      f(nphi,nx)                      */
  /*      by X (in strings):  cos theta,  */
  /*      by Y (in columns):  phi.        */
  /* It gives a possibility to change     */
  /* step by X-axis with interpolation    */

  /* read 2-dim array */
  /* bitpix = ph->bitpix3 = atoll(ph->tform3) / nphi * 8; */
  i = tform3[strlen(tform3)-1];
  switch (i) {
  case 'D':  bitpix = DOUBLEPIX; break;
  default:
  case 'E':  bitpix = FLOATPIX;  break;
  }

  for(ii=i=0; i < *nx; i++,ii+=nph_max) {
    for(j=0; j< (*nph)[i]; j++) {
      double v;
      if (feof(fp_in)) {
	fprintf(stderr, "unexpected end of file '%s'\n", filename);
	return 1;
      }
      v = get_pix_fits(fp_in);
      if (v > fmax)  fmax = v;
      if (v < fmin)  fmin = v;
      (*f)[ii+j] = v;
    }
  }

  if (fp_in != stdin && fp_in != (FILE *)0)  fclose(fp_in);
  FHfree();

  *np = nph_max;

  return 0;
}
//
// EOF
// 
