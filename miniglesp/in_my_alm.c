#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "fits.h"

int in_my_alm(double** ralm, double** ialm, int* lmax, const char* filename)
{
  FILE* fp;
  char  str[161];
  int   naxis;
  char  tform[30];
  int   bitp1, bitp2, bitp3;
  int   tfields;
  int   i;
  int   len;

  /* open file */
  if ((fp = fopen(filename, "r")) == (FILE *)0) {
    fprintf(stderr, "in_glesp_map: can not open file '%s'\n", filename);
    return 1;
  }
  /* read 1st head */
  FHinit(fp);

  if (FEVget("EXTEND") != NULL)
    strcpy(str, FEVget("EXTEND"));

  if (strcmp(str, "T") != 0) {
    fprintf(stderr, "in_glesp_alm: bad extension in file '%s'\n", filename);
    return 1;
  }

  /* Free memory from 1st head */
  FHfree();
  
  /****************************************/
  /* read basic keywods from the 2nd head */
  /****************************************/
  FHinit(fp);

  if (FEVget("XTENSION") != NULL)
    strcpy(str, FEVget("XTENSION"));
  else {
    fprintf(stderr, "in_glesp_alm: no extension in file '%s'\n", filename);
    return 1;
  }
  if (strcmp(str, "BINTABLE") != 0) {
    fprintf(stderr, "in_glesp_alm: can work only with BINTABLE extension\n");
    return 1;
  }
  // if (FEVget("NAXIS") != NULL)   naxis   = FSkey_get("NAXIS");
  // if (FEVget("NAXIS1") != NULL)  naxis1  = FLkey_get("NAXIS1");
  if (FEVget("NAXIS2") != NULL)  naxis  = FLkey_get("NAXIS2");
  // fprintf(stdout, "naxis=%d naxis2=%d, naxis3=%d\n", naxis, naxis1, naxis2);

  if (FEVget("TFIELDS") != NULL)
    tfields = FLkey_get("TFIELDS");
  if (tfields != 3) {
    fprintf(stderr, "in_glesp_alm: TFILEDS value must be equal to "
	    "3 in a_lm file '%s'\n",  filename);
    return 1;
  }

  if (FEVget("TFORM1") != NULL) {
    strcpy(tform, FEVget("TFORM1"));
    if      (strcmp(tform, "1J") == 0)     bitp1 = LONGPIX;
    else if (strcmp(tform, "1I") == 0)     bitp1 = SHORTPIX;
    else {
      fprintf(stderr, "in_glesp_alm: bad value for TFORM1. Sorry...\n");
      return 1;
    }
  } else {
    fprintf(stderr, "in_glesp_alm: no parameter TFORM1. Sorry...\n");
    return 1;
  }

  if (FEVget("TFORM2") != NULL) {
    strcpy(tform, FEVget("TFORM2")); 
    if      (strcmp(tform, "1E") == 0)       bitp2 = FLOATPIX;
    else if (strcmp(tform, "1D") == 0)       bitp2 = DOUBLEPIX;
    else {
      fprintf(stderr, "in_glesp_alm: bad value for TFORM2. Sorry...\n");
      return 1;
    }
  } else {
    fprintf(stderr, "in_glesp_alm: no parameter TFORM2. Sorry...\n");
    return 1;
  }

  if (FEVget("TFORM3") != NULL) {
    strcpy(tform, FEVget("TFORM3"));
    if      (strcmp(tform, "1E") == 0) bitp3 = FLOATPIX;
    else if (strcmp(tform, "1D") == 0) bitp3 = DOUBLEPIX;
    else {
      fprintf(stderr, "in_glesp_alm: bad value for TFORM3. Sorry...\n");
      return 1;
    }
  } else {
    fprintf(stderr, "in_glesp_alm: no parameter TFORM3. Sorry...\n");
    return 1;
  }

  if (FEVget("LMAX") != NULL) {
    *lmax = atoi(FEVget("LMAX"));
    printf("read lmax=%d\n", *lmax);
  }
  
  len = *lmax+1;
  printf("Allocating 2 %dx%d (%d) arrays\n", len, len, len*len);
  *ralm = (double*)calloc(len*len, sizeof(double));
  *ialm = (double*)calloc(len*len, sizeof(double));
  
  /* Free memory from 1nd head */
  FHfree();

  /* get values from file */
  for (i=0; i<naxis; i++) {
    double ddd;
    double val;
    int    nlm;
    int    l;
    int    m;
    
    bitpix = bitp1;
    //nlm = get_pix_fits(fp)+0.5;
    ddd = get_pix_fits(fp);
    nlm = ddd + .5;
    l = (int)sqrt((double)(nlm-1));
  
    if (l > *lmax) break;

    m = nlm - 1 - l * l - l;
    
    /* Real part */
    bitpix = bitp2;
    val = get_pix_fits(fp);
    printf("Assigning real part l=%d, m=%d -> alm[%2d*%2d+%2d]=a[%3d]=%f\n", 
	   l, m, l, len, m, l*len+m, val);
    (*ralm)[l * len + m] = val;
    if (feof(fp)) {
      fprintf(stderr, "in_glesp_alm: unexpected end of file at l=%d and m=%d\n",
	      l, m);
      break;
    }
    /* Imagine part */
    bitpix = bitp3;
    val = get_pix_fits(fp);
    printf("Assigning imag part l=%d, m=%d -> alm[%2d*%2d+%2d]=a[%3d]=%f\n", 
	   l, m, l, len, m, l*len+m, val);
    (*ialm)[l * len + m] = val;
    if (feof(fp)) {
      fprintf(stderr, "in_glesp_alm: unexpected end of file at l=%d and m=%d\n",
	      l, m);
      break;
    }
  }

  fclose(fp);

  return 0;
}

