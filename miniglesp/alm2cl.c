
/** 
 * calculate C_l by a_lm 
 * 
 * @param cl   output C_l array                          
 * @param lmax input a_lm array - complex (nalm/2, nalm) 
 * @param alm  maximum value of l                        
 * @param nalm dimension of a_lm array (Nmx, nalm=2*Nmx) 
 */
void alm2cl (double* cl, int lmax, double* alm, int nalm)
{
  register int l, m;
  
  for(l=0; l<=lmax; l++) {
    int ll = nalm * l;
    double s_alm = alm[ll] * alm[ll];
    for(m=1; m<=l; m++) {
      double v_r = alm[ll+m];
      double v_i = alm[ll+lmax+m+1];
      s_alm += 2. * (v_r * v_r + v_i * v_i);
    }
    cl[l] = s_alm / (double)(2*l + 1);
  }
  return;
}
//
// EOF
//
