#include "miniglesp.h"

/** 
 * Calculate the index corresponding to (theta,phi) into the 2D array
 * of size (nx x nphi). 
 * 
 * @param theta Theta value (radians) 
 * @param phi   Phi value   (radians)
 * @param x     X axis 
 * @param nx    Number of bins along X 
 * @param nphi  Number of bins along phi 
 * @param nph   Phi axis
 * 
 * @return Index or negative value on error 
 *
 * @note This would be replaced by the TH2::FindBin(cos(theta), phi)
 */
int ang2n(double theta, double phi, double* x, int nx, int nphi, int* nph)
{
  int n = 0, i_x=0, j_phi=0;
  
  angle2pixel(x, nx, nph, &i_x, &j_phi, theta, phi);
  if (i_x < 0 || j_phi < 0)
    n = -10;
  else
    n = i_x * nphi + j_phi;
  
  return n;
}
//
// EOF
//
