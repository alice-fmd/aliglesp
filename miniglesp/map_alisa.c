#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "miniglesp.h"

/** 
 * Read in data from a file 
 * 
 * @param f             Array to fill
 * @param x             X axis 
 * @param nx            Number of X bins 
 * @param np            Number of phi bins
 * @param nph           Phi bins
 * @param inp_file      Input file 
 * @param event_num     Which event to read 
 * @param event_type    0: all, 1 primary 
 * @param event_inf     Assume infinite resolution 
 */
int map_alisa(double*     f, 
	      double*     x, 
	       int         nx, 
	      int         np, 
	      int*        nph, 
	       const char* inp_file, 
	      int         event_num, 
	      int         event_type, 
	      int         resolution) 
{
  FILE* fp;
  char  str[161];
  int   take_event = 0;

  /* open input file with Alisa data */
  if ((fp = fopen(inp_file, "r")) == 0) {
    fprintf(stderr, "Failed to open input file %s\n", inp_file);
    return 1;
  }

  while(fgets(str,160,fp)) {
    // Check for comment line 
    if (*str == '#' || *str == '\n')	continue;
    // Check for beginning of event 
    if (strncmp("BEGIN", str, 5) == 0) {
      char ss[161];
      if (event_num > 0) {
	int nn = 0;
	sscanf(str, "%s%s%d",ss,ss,&nn);
	// Set flag_n to true if this is the selected event 
	if (event_num == nn)  take_event = 1;
      }
      else 
	take_event = 1;
      continue;
    }

    // Check for end of event 
    if (strncmp("END", str, 3) == 0) {
      take_event = 0;
      continue;
    }

    if (take_event) {
      int n_pix;
      double eta    = 0; 
      double theta  = 0;
      double phi    = 0;
      int    flag   = 0;
      double deta   = 0;
      double dphi   = 0;
      // double degThe = 0;
      // double degPhi = 0;
      // double dtheta = 0;
      // Read a line 
      sscanf(str, "%lf%lf%lf%d%lf%lf",
	     &eta, &theta, &phi, &flag, &deta, &dphi);
      // degThe = theta;
      // degPhi = phi;
      theta /= RAD2DEG;
      phi   /= RAD2DEG;

      if (event_type >=0 && event_type != flag) continue;
      if (resolution && deta < 0.)         continue;

      // dtheta =  fabs(-2 * deta / (exp (-eta) + exp (eta)));
      // dphi   /= RAD2DEG;

      n_pix = ang2n(theta, phi, x, nx, np, nph);
      if (n_pix < 0) { 
	fprintf(stderr, "(theta,phi)=(%f,%f) [eta=%f] out of bounds\n",
		RAD2DEG * theta, RAD2DEG * phi, eta);
	continue;
      }
#if 0
      printf("theta=%12f phi=%12f ix=%6d iphi=%6d\n",
	     degThe, degPhi, ind_x(nx, x, cos(theta)), 
	     (int)(phi / 2 / M_PI * np+.5));
#endif
      f[n_pix] += 1; /* summer of events */
    }
  }

  fclose (fp);
  return 0;
}
//
// EOF
//

