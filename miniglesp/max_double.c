#include <math.h>
#include <float.h>

/** 
 * Return 
 * @f[ 
 *   \log_{10}(X)-8
 * @f]
 *
 * where @f$ X@f$ is the maximum value of a double 
 * 
 * @return 
 */
int max_double()
{
#ifndef MAXDOUBLE 
# ifdef DBL_MAX
#  define MAXDOUBLE DBL_MAX
# else
#  define MAXDOUBLE MAXFLOAT
# endif
#endif
  return (int)(log10(MAXDOUBLE))-8;
}
//
// EOF
//
