#include "miniglesp.h"

/** 
 * Legandre functions F^m_k(x), k=m,m+1,...,Kmx
 * 
 * @param Lmx 
 * @param m 
 * @param cmm 
 * @param c 
 * @param x 
 * @param glm 
 */
void lgndr_m(int Lmx, int m, double cmm, double* c, double x, double* glm)
{
  register int i, k, Imax = 0;
  register int    m1    = m+1;
  register int    m2    = m+2;
  double          vl    = 1.0 - x * x + 1.e-50;
  register double dfm   = 0.50 * m * log10(vl);
  int             Lmin  = LMIN_GL;
  register double Enorm = ENORM_GL;    /* 10^Lmin */
  register double* gm1  = &(glm[m1]);
  register double* c2   = &(c[Lmx]);

  if(fabs(dfm) > Lmin) Imax = -dfm/Lmin;
  dfm += Imax * Lmin;
  
  *gm1 = cmm * pow(10., dfm);    /* P_m^m    */

  if(m == Lmx || m1 == Lmx) {
    if(m == Lmx) {
      while(Imax > 0) { *gm1 /= Enorm; Imax--; }
    } else  if(m1 == Lmx) {
      glm[m2] = x * c[m1] * *gm1;    /* P_m+1^m  */
      while(Imax>0) {*gm1 /=Enorm; glm[m2]/=Enorm; Imax--;}
    }
    return;
  }
  glm[m2] = x * c[m1] * *gm1;    /* P_m+1^m  */

  for(k=m2; k <= Lmx; k++)  { /* P_m+l^m */
    register int kk;
    register int k1=k+1;
    register double *ck  = c+k;
    register double *c2k = c2 + k;
    register double *g   = glm + k;

    /* recursional relation */
    // glm[k1] = x * c[k]*glm[k] - c[Lmx+k]*glm[k-1];
    *(g+1) = x *  *ck  *  *g - *c2k  *  *(g-1);

    if(fabs(glm[k1]) > Enorm) {
      for(kk=m1; kk <= k1; kk++) {
	glm[kk] /= Enorm;
	if(Imax >= 2) glm[kk] /= Enorm;
      }
      if(Imax >= 2) Imax -= 2; else Imax--;
    }
  }
  if(Imax >= 1) {
    register int l1 = Lmx + 1;
    for(i=1; i<=Imax; i++) {
      for(k=m+1; k <= l1; k++)
	glm[k] /= Enorm;
    }
  }

  return;
}
//
// EOF
// 
