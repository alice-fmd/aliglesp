#include "miniglesp.h"

/** 
 * Calculate pixel (i_x,i_phi) from (theta,phi) for 2D array. 
 * 
 * @param x 
 * @param nx 
 * @param nph 
 * @param i_x 
 * @param j_phi 
 * @param theta 
 * @param phi 
 *
 * @note This is redundant if using a histogram 
 */
void angle2pixel(double* x, int nx, int* nph, int* i_x, int* j_phi, 
		 double theta, double phi)
{
  *i_x   = ind_x(nx, x, cos(theta));
  if (*i_x == nx)  *i_x   = -nx;  // Bad return
  if (*i_x  < 0)   *j_phi = -1;   // Bad return 
  else {
    double dp = TWOPI / nph[*i_x];  // step in phi
    *j_phi = phi / dp + 0.5;      // Just divide 
    if (*j_phi >= nph[*i_x] )  *j_phi=0; // Wrap around 
  }
  return;
}
//
// EOF
//
 
