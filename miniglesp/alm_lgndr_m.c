#include "miniglesp.h"
#include <stdlib.h>

#define NORM (M_2_SQRTPI / 2. / M_SQRT2)

/**
 *   loop over l - SUM_l[a_{lm}P_l^m(x_p)]
 *   input: x(Nx),Nph(Jmx),apm(0:Jmx,Nmx),bpm(0:Jmx,Nmx)
 *   ouput: f(Nmx,Nmp)
 *
 *     DIMENSION f(Nmx*Nmp),x(Nx),Nph(Nmx)
 *     DIMENSION apm((Jmx+1)*Nmx),bpm((Jmx+1)*Nmx)
 *     DIMENSION c(2*Lmx+2),glm(Lmx+1)
 *
 */
int alm_lgndr_m(int Nmx, int Nmp, int* Nph, 
		int Lmn, int Lmx, int Mmn, int Mmx, 
		double eps,
		int Nx, 
		double* x, double* apm, double* bpm, double* glm,
		double* c, double* f)
{
  register int l,m, mb, ix;
  double        cmm   = 0;
  double        cnorm = -NORM;
  int           Kx    = (Nx+1)/2;
  int           mmin  = (Mmn < 0 ? 0 : Mmn);     /*               */
  int           mmax  = (Mmx < 0 ? Lmx : Mmx);   /* use in future */
  register int* mx    = 0;
  register int  mm    = Nmx * mmin;


  if ((mx = (int *)calloc((unsigned)Kx, sizeof(int))) == 0) {
    fprintf(stderr, "Failed to allocate array mx\n");
    return 1;
  }
  for(m=0; m<Kx; m++) {
    mx[m] = VNYQ * Nph[m];
    if (mx[m] > Lmx) mx[m] = Lmx;
  }


  printf("Looping over m=[%d,%d]\n", mmin, mmax);
  for(m=mmin; m <= mmax; m++, mm+= Nmx) { /* for all m */
    int L0 = m;
    register int llp;
    mb = Lmx + 1 + m;

    start_clm(Lmx, m, &cmm, c);

    printf(" Looping over x=[0,%d]\n", Kx);
    for (ix=0; ix<Kx; ix++) { /* for all x(ix) */
      double          xc  = x[ix];
      register double api = apm[mm+ix];
      register double bpi = bpm[mm+ix];
      register double apj = 0.0;
      register double bpj = 0.0;
      register int    jx  = Nx - 1 - ix;

      if (m > mx[ix])  continue;

      apj = apm[mm+jx];
      bpj = bpm[mm+jx];

      lgndr_m(Lmx, m, cmm, c, xc, glm);

      if(L0 < Lmn)  L0 = Lmn;

      llp = L0 * Nmp;
      printf("  Looping over l=[%d,%d]\n", L0, Lmx);
      for(l=L0; l<= Lmx; l++,llp += Nmp) {  /* for all m<L<Lmx */
	register double gl = glm[l+1];
	if(fabs(gl) > eps) {
	  printf("   Updating at %d+%d with %f\n", llp, m, gl);
	  f[llp+m]   += gl*api;
	  f[llp+mb]  += gl*bpi;

	  if(ix < jx) {
	    int ax = 1 - 2 * ((l+m) % 2);
	    if (ax < 0) {
	      f[llp+m]  -= gl*apj;
	      f[llp+mb] -= gl*bpj;
	    } else {
	      f[llp+m]  += gl*apj;
	      f[llp+mb] += gl*bpj;
	    }
	  }
	}             /* for glm< eps       */
      }               /* for all m<L< Lmx   */
    }                /* for all x(ix)      */

    /* normalization and record of alm */
    llp = Lmn * Nmp;
    for (l= Lmn; l<= Lmx; l++, llp += Nmp) {
      f[llp+m]  *=  cnorm;
      f[llp+mb] *= -cnorm;
    }
  }     /*  over all m */

  if (mx)  free(mx);

  return 0;
}
//
// EOF
//

