#
#
#
include Makefile.def

PACKAGE		:= AliGlesp
VERSION		:= 0.4
TARGETS		:= lib$(PACKAGE).so 
DICTIONARY	:= $(PACKAGE)_Dict
FROM_HITS	:= 1
EPS		:= 1e-6
NX		:= 200
NY		:= 20
SIM_NX		:= 200
SIM_NY		:= 20
SIM_VFILES	:= $(wildcard data/sim-v*.hits)
LMAX		:= $(shell awk 'BEGIN {x=($(NX)-1)/2;y=($(NY)-1)/4;print int(x<y?x:y)}')

all:	.miniglesp .v2 


.miniglesp:
	$(MAKE) -C miniglesp

.v2:
	$(MAKE) -C src 
	touch $@ 

data/test.hits data/test.root:	scripts/MakeData.C
	@echo "Making $@ using $<"
	@$(ROOT) $(ROOT_BATCH) scripts/MakeData.C\(\"$(basename $@)\"\) 

data/jjg.root: scripts/RunJJGModel.C 
	@$(ROOT) $(ROOT_BATCH) $<\(\"$@\",0,0,10000,10,0\)

data/%.map:   data/%.hits .miniglesp 
	@echo "Generating $@ from $< using mappat -e $(NX) -p $(NY)"
	@./miniglesp/mappat -i $< -o $@ -e $(NX) -p $(NY)

data/%.alm data/%.cl:data/%.map .miniglesp 
	@echo "Generating $@ from $< using cl2map"
	@./miniglesp/cl2map -i $< -o data/$*.alm -c data/$*.cl -l $(LMAX)

data/%-glesp.root: data/%.map .miniglesp
	@echo "Generating $@ from $< using RunGlesp.C"
	@$(ROOT) $(ROOT_BATCH) miniglesp/RunGlesp.C\(\"$<\",\"$@\"\)


ifdef FROM_HITS
data/%-map.root: data/%.hits .v2 
	@echo "Making $@ from $< using scripts/Run.C NX=$(NX) NY=$(NY)"
	@$(ROOT) $(ROOT_BATCH) src/MapIt.C\(\"$<\",\"$@\",$(NX),$(NY)\)

else
data/%-map.root: data/%.map .v2
	@echo "Making $@ from $< using scripts/Run.C NX=$(NX) NY=$(NY)"
	@$(ROOT) $(ROOT_BATCH) src/MapIt.C\(\"$<\",\"$@\",$(NX),$(NY)\)

endif
data/%-map.root: data/%.root .v2 
	@echo "Making $@ from $< using scripts/Run.C NX=$(NX) NY=$(NY)"
	@$(ROOT) $(ROOT_BATCH) src/MapIt.C\(\"$<\",\"$@\",$(NX),$(NY)\)

data/%-sph.root: data/%-map.root .v2
	$(ROOT) $(ROOT_BATCH) src/DesolveIt.C\(\"$<\",\"$@\"\)

data/%-vns.root: data/%-sph.root .v2 
	$(ROOT) $(ROOT_BATCH) src/FlowIt.C\(\"$<\",\"$@\"\)

%.test:	data/%-root.root
	@$(MAKE)  -s --no-print-directory -C miniglesp $@ 
	@echo "Drawing results from $<"
	@$(ROOT) $(ROOT_FLAGS) scripts/DrawResults.C\(\"$<\"\) 

%.diff:	data/%-glesp.root data/%-root.root tests/CompareResults.C 
	@echo "Testing diff between $(word 1,$^) and $(word 2,$^)"
	@$(ROOT) $(ROOT_FLAGS) \
	  tests/CompareResults.C\(\"$(word 1,$^)\",\"$(word 2,$^)\",$(EPS)\) \
	  2>&1 | tee $@ 

#%.draw:data/%-glesp.root data/%-root.root tests/CompareResults.C 
#	@echo "Drawing from $(word 1,$^) and $(word 2,$^)"
#	@$(ROOT) $(ROOT_FLAGS) \
#	  scripts/DrawBoth.C\(\"$(word 1,$^)\",\"$(word 2,$^)\"\) 

data/sim.map: 		NX=$(SIM_NX)
data/sim.map: 		NY=$(SIM_NY)
data/sim-root.root: 	NX=$(SIM_NX)
data/sim-root.root: 	NY=$(SIM_NY)

data:	
	mkdir -p data

draw-%:src/Draw.C data/%-map.root data/%-sph.root 
	$(MAKE) data/$*-vns.root  
	$(ROOT) $(ROOT_FLAGS) $<\(\"$(word 2,$^)\",\"$(word 3,$^)\"\)

$(DICTIONARY).cc:$(HEADERS)
	@echo "Making dictionary $@"
	@$(ROOT_CINT) -f $@ -c $(CPPFLAGS) $^

clean:
	@echo "Cleaning"
	@rm -f 			\
	      *~ 		\
	      *.log		\
	      core		\
	      data/*.root 	\
	      data/*.dat 	\
	      data/*.map	\
	      data/test.hits	\
	      .v2

distclean:	clean
	@echo "Distribution cleaning"
	@rm -f *.o 		\
	      $(DICTIONARY).cc 	\
	      $(DICTIONARY).h 	\
	      $(TARGETS)  	\
	      .miniglesp	
	@$(MAKE) -s --no-print-directory -C miniglesp $@
	@$(MAKE) -s --no-print-directory -C scripts   $@
	@$(MAKE) -s --no-print-directory -C tests     $@

distdir		:= $(PACKAGE)-$(VERSION)
dist:
	@mkdir -p $(distdir) 
	cp -a $(DISTS) $(distdir)
	$(MAKE) -C miniglesp distdir=../$(distdir) $@
	$(MAKE) -C scripts   distdir=../$(distdir) $@ 
	$(MAKE) -C tests     distdir=../$(distdir) $@ 
	tar -czf $(distdir).tar.gz $(distdir)
	rm -rf $(distdir)

distcheck:dist
	tar -xzvf $(distdir).tar.gz 
	(cd $(distdir) && $(MAKE)) 
	(cd $(distdir) && $(MAKE) test BATCH=1) 
	(cd $(distdir) && $(MAKE) clean)
	(cd $(distdir) && $(MAKE) dist)	
	rm -rf $(distdir)
	@echo "******************************************"
	@echo "$(distdir).tar.gz ready for distribution"
	@echo "******************************************"
# Some tests
test-coords:	tests/TestCoords.C all
	@$(ROOT) $(ROOT_FLAGS) $<\($(SIM_NX),$(SIM_NY)\)
test-mapping:	tests/TestMapping.C all
	@$(ROOT) $(ROOT_FLAGS) scripts/Load.C \
		$<+\(\"data/sim.hits\",$(SIM_NX),$(SIM_NY)\)

v2-root:$(SIM_VFILES:%.hits=%-root.root)
	@$(ROOT) $(ROOT_FLAGS) scripts/DrawV2.C\(\"data\",\"sim-v.*-root\"\)

v2-glesp:$(SIM_VFILES:%.hits=%-glesp.root)
	@$(ROOT) $(ROOT_FLAGS) scripts/DrawV2.C\(\"data\",\"sim-v.*-glesp\"\)

v2-both:$(SIM_VFILES:%.hits=%-glesp.root) $(SIM_VFILES:%.hits=%-root.root)
	@$(ROOT) $(ROOT_FLAGS) scripts/DrawBothV2.C\(\"data\"\)

.PRECIOUS:$(SIM_VFILES:%.hits=%-glesp.root) $(SIM_VFILES:%.hits=%-root.root) \
	data/jjg-map.root data/jjg-sph.root data/jjg.root 

VPATH	= . lib 
#
# EOF
#

